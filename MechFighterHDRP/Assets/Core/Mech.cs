﻿using System;
using System.IO;
using Core.MechComponents;
using Core.Utility;
using JetBrains.Annotations;
using UnityEngine;

namespace Core {
    [Serializable]
    public class Mech {

        const int DEFAULT_ANIM_ID = 10000;
        
        #region NonGameState

        [UsedImplicitly] public byte movesetType;
        public int score;
        public byte handle;
        public readonly MechParameters parameters;
        
        [UsedImplicitly] public int currentAnimStateStartFrame;
        [UsedImplicitly] public int currentAnimStateId;
        
        #endregion

        #region GameState

        public Vector3Int position;
        public Vector3Int velocity;
        public Structs.AimData aimData;
        public short health;
        public short dashMeter;
        public short specialMeter;

        public bool lockedOn;
        public byte lockOnTarget;
        
        public bool grounded;
        public bool inHitStun;
        public bool counterHitState; //unserialized
        public bool hasDoubleJump; //unserialized
        public bool hasAirDash; //unserialized and everything below too
        public int superGainLockoutStart;
        public int dashGainLockoutStart;
        public int selectedWeaponId = -1;
        public bool isBlocking;
        public bool inBlockStun;
        
        public Structs.MechCharacterState characterState;

        readonly MechComponent[] mechComponents;

        #endregion

        #region Serialization

        public void Serialize(BinaryWriter bw) {
            bw.Write(position.x);
            bw.Write(position.y);
            bw.Write(velocity.x);
            bw.Write(velocity.y);
            bw.Write(aimData.Heading);
            bw.Write(aimData.pitch);
            bw.Write(health);
            bw.Write(dashMeter);
            bw.Write(specialMeter);
            bw.Write(score);
            bw.Write(currentAnimStateId);
            bw.Write((int)characterState);
            for (int i = 0; i < mechComponents.Length; i++)
                mechComponents[i].Serialize(bw);
        }
        
        public void Deserialize(BinaryReader br) {
            position.x = br.ReadInt32();
            position.y = br.ReadInt32();
            velocity.x = br.ReadInt32();
            velocity.y = br.ReadInt32();
            aimData.Heading = br.ReadUInt16();
            aimData.pitch = br.ReadInt16();
            health = br.ReadInt16();
            dashMeter = br.ReadInt16();
            specialMeter = br.ReadInt16();
            score = br.ReadInt32();
            currentAnimStateId = br.ReadInt32();
            characterState = (Structs.MechCharacterState)br.ReadInt32();
            for (int i = 0; i < mechComponents.Length; i++)
                mechComponents[i].Deserialize(br);
        }

        public override int GetHashCode() {
            return HashCode
                .Of(position)
                .And(velocity)
                .And(aimData)
                .And(health)
                .And(dashMeter)
                .And(specialMeter)
                .And(score)
                .AndEach(mechComponents);
        }

        #endregion

        #region Properties

        //public bool IsActionable => !inHitStun && currentAction == null;
        public bool IsActionable => !inHitStun && !actionHandler.IsActionActive && !inBlockStun;
        public uint CurrentWeaponRange { get; set; }

        public readonly MechPhysicsComponent physicsComponent;
        public readonly MechController controllerComponent;
        public readonly MechActionHandler actionHandler;
        public readonly MechHitController hitController;
        public readonly HitboxManager hitboxManager;

        #endregion

        public Mech(byte handle, MechComponent[] components, MechParameters parameters, byte movesetType, HitboxManager hitboxManager) {
            this.handle = handle;
            mechComponents = new MechComponent[components.Length];
            for (int i = 0; i < components.Length; i++) {
                mechComponents[i] = components[i];
                mechComponents[i].SetInstance(this);
                switch (mechComponents[i]) {
                    case MechPhysicsComponent physics:
                        physicsComponent = physics;
                        break;
                    case MechController controller:
                        controllerComponent = controller;
                        break;
                    case MechActionHandler handler:
                        actionHandler = handler;
                        break;
                    case MechHitController hit:
                        hitController = hit;
                        break;
                }
            }
            this.hitboxManager = hitboxManager;
            this.parameters = parameters;
            this.movesetType = movesetType;
        }

        public void ReturnToIdle(int frameNumber) {
            //currentAnimStateId = GetIdleState();
            //currentAnimStateStartFrame = frameNumber;
        }

        int GetIdleState() {
            if (grounded)
                return 10000; //idle //todo: croucing idle 10001
            return 20000;
        }
        
        public void ResetState() {
            //not resetting position and aim data. these are done in Reposition Mechs
            health = (short)parameters.health;
            dashMeter = (short)parameters.dash;
            specialMeter = (short)parameters.super;
            currentAnimStateId = DEFAULT_ANIM_ID;
            hasDoubleJump = true;
            characterState = 0;
            velocity = Vector3Int.zero;
            lockedOn = false;
            grounded = true;
            characterState = Structs.MechCharacterState.Standing;
            currentAnimStateId = 0;
            inHitStun = false;
            currentAnimStateStartFrame = 0;
            for (int i = 0; i < mechComponents.Length; i++) {
                mechComponents[i].ResetState();
            }
        }

        #region Events

        public event Action<int> OnLanding;
        public void TriggerOnLanding(int frameNumber) => OnLanding?.Invoke(frameNumber);
        public event Action<int> OnAirborne;
        public void TriggerOnAirborne(int frameNumber) => OnAirborne?.Invoke(frameNumber);

        #endregion
    }
}