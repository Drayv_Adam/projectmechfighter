using System;
using System.IO;
using Core.MechActions;
using Core.Utility;
using MechFighter;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.MechComponents {
    using static MechGameConstants;
    using static Structs;
    public class MechHitController : MechComponent {

        public static event Action<int> OnBlock;
        public static event Action<int> OnGuardBreak; 
        public static event Action<int> OnHit; 

        const byte DEATH_HIT_STOP = 30;
        const byte KNOCKDOWN_MIN_DURATION = 30;
        const byte GUARD_BREAK_DASH_LOCKOUT_PENALTY = 255;
        const byte GUARD_MAX_ANGLE = 80;
        
        const ushort COMBO_RATE = 6000;
        const ushort BAD_STARTER_P1 = 7500;
        const uint CH_MULTIPLIER = 11000;
        
        static readonly AttackLevelValues[] AttackLevels = {
            new AttackLevelValues(8, 0, 0, 0, 0, 9, 7500, 100),
            new AttackLevelValues(9, 0, 12*2, 12*2, 4, 11, 8000, 120),
            new AttackLevelValues(10, 1, 14*2, 14*2, 4, 13, 8500, 200),
            new AttackLevelValues(11, 2, 17*2, 17*2, 5, 16, 8900, 500),
            new AttackLevelValues(12, 5, 19*2, 19*2, 5, 18, 9200, 1000),
            new AttackLevelValues(13, 8, 21*2, 21*2, 6, 20, 9400, 2000)
        };

        HitLagManager hitLagManager;
        
        byte hitCombo;
        uint currentDamageScaling = SCALE;
        int lastHitFrame;
        ushort currentHitStunDuration;
        bool deathTriggered;
        bool previouslyGrounded;
        bool blocked;
        
        public MechHitController(HitLagManager hitLagManager) {
            this.hitLagManager = hitLagManager;
        }

        public void Update(int frameNumber) {
            if (!mech.inHitStun && !mech.inBlockStun)
                return;
            
            if (!previouslyGrounded && mech.grounded) {
                mech.actionHandler.StartAction(mech.controllerComponent.SystemActions.landingOnBack, frameNumber);
                lastHitFrame = frameNumber;
                currentHitStunDuration = KNOCKDOWN_MIN_DURATION;
                previouslyGrounded = mech.grounded;
                return;
            }
            
            if (frameNumber > lastHitFrame + currentHitStunDuration && mech.grounded) {
                mech.actionHandler.ResetState();
                mech.inBlockStun = false;
                mech.inHitStun = false;
                mech.ReturnToIdle(frameNumber);
                ResetState();
            }
        }
            
        public void GetFucked(HitProperties hitProperties, HitboxAttributes attributes, ushort aimDataHeading, Vector3Int attackerPosition, int frameNumber) {
            if (mech.characterState == MechCharacterState.Knockdown)
                return;
            if (deathTriggered)
                return;
            blocked = IsBlocked(attributes, attackerPosition);
            if (!blocked)
                mech.isBlocking = false;
            if (!HandleDamage(hitProperties, frameNumber))
                deathTriggered = true;
            HandleHitLag(hitProperties.attackLevel, frameNumber);
            HandleLauncher(hitProperties, frameNumber);
            HandleKnockback(hitProperties.localKnockback, aimDataHeading);
            HandleHitStun(hitProperties);
            HandleActions(hitProperties, frameNumber, attributes);
            UpdateProration(hitProperties.attackLevel, hitProperties.badStarter);
            lastHitFrame = frameNumber;
            if (!blocked)
                hitCombo++;
            previouslyGrounded = mech.grounded;
        }

        void HandleActions(HitProperties hitProperties, int frameNumber, HitboxAttributes attributes) {
            if (blocked)
                return;
            var systemActions = mech.controllerComponent.SystemActions;
            MechAction action = null;
            if (deathTriggered && mech.grounded)
                action = systemActions.deathStanding;
            else if (hitProperties.launcher && attributes.isLow)
                action = systemActions.knockDown;
            else if (!mech.grounded || hitProperties.launcher)
                action = systemActions.getHitAir;
            else if (hitProperties.attackLevel < 3)
                action = systemActions.getHitLight[Random.Range(0, systemActions.getHitLight.Length)];
            else if (attributes.isOverhead)
                action = systemActions.getHitOverhead;
            else
                action = systemActions.getHitStanding;
            
            if (action != null)
                mech.actionHandler.StartAction(action, frameNumber);
        }

        bool IsBlocked(HitboxAttributes attributes, Vector3Int attackerPosition) {
            if (!mech.isBlocking || attributes.isLow && mech.characterState == MechCharacterState.Standing ||
                attributes.isOverhead && mech.characterState == MechCharacterState.Crouching)
                return false;
            
            var offset = attackerPosition - mech.position;
            var angle = MechUtils.GetVectorHeading(new Vector2Int(offset.x, offset.z));

            return AreHeadingsSimilar(mech.aimData.Heading, angle);
        }

        bool AreHeadingsSimilar(int heading1, int heading2) {
            return (Mathf.Abs(heading1 - heading2) < GUARD_MAX_ANGLE * ANGLE_SCALE ||
                    Mathf.Abs(heading1 - heading2) > (360- GUARD_MAX_ANGLE) * ANGLE_SCALE);
        }
        
        void HandleHitLag(byte levelId, int frameNumber) {
            if (deathTriggered) {
                hitLagManager.AddHitlag(DEATH_HIT_STOP, frameNumber);
                return;
            }
            var hitlagDuration = AttackLevels[levelId].hitStop;
            if (mech.counterHitState)
                hitlagDuration += AttackLevels[levelId].chHitStopBonus;
            hitLagManager.AddHitlag(hitlagDuration, frameNumber);
        }

        void HandleHitStun(HitProperties hitProperties) {
            var attackLevel = AttackLevels[hitProperties.attackLevel];
            if (blocked) {
                currentHitStunDuration = attackLevel.blockStun;
                mech.inBlockStun = true;
                return;
            }
            currentHitStunDuration = (ushort)(currentDamageScaling * (mech.grounded? attackLevel.hitStun : attackLevel.airHitStun) / SCALE);
            if (mech.counterHitState)
                currentHitStunDuration += attackLevel.chHitStunBonus;
            mech.inHitStun = currentHitStunDuration > 0;
        }

        void HandleLauncher(HitProperties hitProperties, int frameNumber) {
            if (hitProperties.launcher && !blocked) {
                mech.grounded = false;
                mech.TriggerOnAirborne(frameNumber);
            }
        }

        void UpdateProration(byte attackLevel, bool badStarter) {
            if (blocked)
                return;
            if (hitCombo == 0)
                currentDamageScaling = badStarter? BAD_STARTER_P1 : SCALE;
            else
                currentDamageScaling = currentDamageScaling * AttackLevels[attackLevel].proration / SCALE;
        }
        
        bool HandleDamage(HitProperties properties, int frameNumber) {
            if (blocked) {
                mech.dashMeter -= (short)AttackLevels[properties.attackLevel].shieldDamage;
                if (mech.dashMeter < 0) {
                    mech.counterHitState = true;
                    mech.dashGainLockoutStart = frameNumber + GUARD_BREAK_DASH_LOCKOUT_PENALTY;
                    OnGuardBreak?.Invoke(mech.handle);
                }
                else {
                    OnBlock?.Invoke(mech.handle);
                }
                return true;
            }

            OnHit?.Invoke(mech.handle);
            var takenHealth = mech.counterHitState
                ? (short) (CH_MULTIPLIER * GetScaledDamage(properties.damage) / SCALE)
                : GetScaledDamage(properties.damage);
            mech.health -= takenHealth;
            return mech.health > 0;
        }
        
        short GetScaledDamage(ushort damage) {
            if (hitCombo > 0) {
                return (short)(currentDamageScaling * damage / SCALE * COMBO_RATE / SCALE);
            }
            return (short)damage;
        }
        
        void HandleKnockback(Vector3Int knockback, ushort heading) {
            if (mech.grounded)
                knockback.y = 0;
            knockback = MechUtils.RotateVector3AroundY(knockback, heading);
            mech.physicsComponent.HardResetVelocity();
            mech.physicsComponent.AddForce(knockback);
        }
        
        public override void ResetState() {
            hitCombo = 0;
            lastHitFrame = 0;
            currentDamageScaling = SCALE;
            currentHitStunDuration = 0;
            deathTriggered = false;
        }

        public override void Serialize(BinaryWriter bw) {
            //
        }

        public override void Deserialize(BinaryReader br) {
            //
        }

        public override int GetHashCode() {
            return 0;//
        }
    }
}