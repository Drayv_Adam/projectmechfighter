using System;
using System.IO;

namespace Core.MechComponents {
    [Serializable]
    public abstract class MechComponent {
        protected Mech mech { get; private set; }
        
        public virtual void SetInstance(Mech mechInstance) {
            mech = mechInstance;
        }

        public abstract void ResetState();
        
        public abstract void Serialize(BinaryWriter bw);
        public abstract void Deserialize(BinaryReader br);
        public abstract override int GetHashCode();
    }
}