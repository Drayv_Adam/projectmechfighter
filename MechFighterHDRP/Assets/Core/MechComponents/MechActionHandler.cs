using System.IO;
using Core.MechActions;
using Core.MechMovesets;
using Core.MechMovesets.MechActions;
using Core.Utility;
using MechFighter;
using UnityEngine;

namespace Core.MechComponents {
    
    using static Structs;
    
    public class MechActionHandler : MechComponent {

        byte nextEventId;
        uint currentActionFrame;

        MechAction currentAction;
        
        bool isMovementDataSet;
        bool lockOnCache;
        
        MovementData currentMovementData;
        public HitResult lastHitResult;
        public ActionCancels currentActionCancels;
        
        public Mech Mech => mech;
        
        public MechInputs CurrentMechInputs { get; private set; }

        public bool ButtonPressedOrBuffered(Buttons button) => Mech.controllerComponent.ButtonPressedOrBuffered(button);

        public bool IsActionActive => currentAction != null;


        public MechAction[] SpecialActions => Mech.controllerComponent.SpecialActions;
        public SystemActions SystemActions => Mech.controllerComponent.SystemActions;

        public override void SetInstance(Mech mechInstance) {
            base.SetInstance(mechInstance);
            mech.OnLanding += HandleLanding;
        }

        ~MechActionHandler() {
            mech.OnLanding -= HandleLanding;
        }

        public bool TryStartAction(MechAction action, MechInputs mechInputs, int frameNumber) {
            CurrentMechInputs = mechInputs;
            if (action?.ConditionsSatisfied(mech) != true)
                return false;
            ResetState();
            mech.aimData.Heading = mechInputs.aimData.Heading;
            currentActionFrame = 0;
            currentAction = action;
            if (currentAction.LockOnOnStart) {
                lockOnCache = mech.lockedOn;
                mech.lockedOn = true;
                mech.lockOnTarget = mech.handle == 0 ? (byte) 1 : (byte) 0;
            }
            currentAction.Start(mechInputs, frameNumber, this);
            //Debug.Log($"[ mech {mech.handle} ]: {currentAction.name} started");
            return true;
        }
        
        public void StartAction(MechAction action, int frameNumber) {
            ResetState();
            currentActionFrame = 0;
            currentAction = action;
            currentAction.Start(CurrentMechInputs, frameNumber, this);
        }
        
        public bool TryUpdateOrFinish(MechInputs mechInputs, int frameNumber) {
            if (currentAction == null)
                return true;
            CurrentMechInputs = mechInputs;
            TryTriggerNextEvent(frameNumber);

            if (lastHitResult >= currentActionCancels.cancelOn) {
                if (mech.controllerComponent.TryStartNewAction(mechInputs, frameNumber, true, currentActionCancels)) {
                    Debug.Log($"Action cancelled into {currentAction}");
                    return false;
                }
            }
            
            currentAction.OnUpdate((byte)(nextEventId - 1), frameNumber, this, out bool isHolding);

            if (currentActionFrame >= currentAction.endFrame && currentAction.endFrame != 0) {
                FinishAction(frameNumber);
                return true;
            }
            
            if (!isHolding)
                currentActionFrame++;

            if (isMovementDataSet)
                ApplyMovementData(currentMovementData);
            return false;
        }

        public void FinishAction(int frameNumber) {
            //Debug.Log($"[ mech {mech.handle} ]: {currentAction.name} finished");
            mech.ReturnToIdle(frameNumber);
            ResetState();
        }

        public void NotifyHitResult(HitResult hit) {
            if (lastHitResult < hit)
                lastHitResult = hit;
        }
        
        public void JumpToEvent(byte eventId, int frameNumber)  {
            if (eventId >= currentAction.actionEvents.Length) {
                Debug.LogError($"Invalid Event ID on {eventId}. Event count: {currentAction.actionEvents.Length}");
                return;
            }
            var actionEvent = currentAction.actionEvents[eventId];
            currentActionFrame = actionEvent.onFrame;
            nextEventId = eventId;
            currentAction.TriggerEvent(nextEventId, frameNumber, this);
            nextEventId++;
        }
        
        public void UpdateCancels(ActionCancels newActionCancels) {
            currentActionCancels = newActionCancels;
        }
        
        public void UpdateAnimation(int animationId, int frameNumber) {
            mech.currentAnimStateId = animationId;
            mech.currentAnimStateStartFrame = frameNumber;
        }
        
        public void UpdateHitboxes(HitboxData data) {
            mech.hitboxManager.UpdateHitboxData(mech.handle, data);
        }
        
        public void UpdateMovement(MovementData m) {
            if (m.influencedByDInput) {
                var forwardMomentum = m.localAcceleration.z;
                m.localAcceleration.z = 0;
                var v2 = CurrentMechInputs.movement;
                if (m.normalizeDInput)
                    v2 = v2 == Vector2Int.zero? new Vector2Int(0, MechGameConstants.SCALE) : MechUtils.Normalize(v2);
                v2 = v2 * forwardMomentum / MechGameConstants.SCALE;
                m.localAcceleration += new Vector3Int(v2.x, 0, v2.y);
            }
            if (m.resetVelocity)
                mech.physicsComponent.HardResetVelocity();
            if (m.impactForce)
                ApplyMovementData(m);
            else {
                isMovementDataSet = m.localAcceleration != Vector3Int.zero;
                currentMovementData = m;
            }
        }
        
        void TryTriggerNextEvent(int frameNumber) {
            if (nextEventId >= currentAction.actionEvents.Length) 
                return;
            var actionEvent = currentAction.actionEvents[nextEventId];
            if (currentActionFrame < actionEvent.onFrame || actionEvent.ignoreNormally)
                return;

            if(currentAction.TriggerEvent(nextEventId, frameNumber, this))
                nextEventId++;
        }

        void HandleLanding(int frameNumber) {
            mech.hasDoubleJump = true;
            mech.hasAirDash = true;
            if (currentAction != null && !currentAction.OnLandingCancel())
                return;
            if (currentAction != null)
                Debug.Log($"[ mech {mech.handle} ]: {currentAction?.name} cancelled by landing");
            if (mech.characterState == MechCharacterState.Tumble || mech.characterState == MechCharacterState.Knockdown) {
                mech.characterState = MechCharacterState.Knockdown;
                TryStartAction(SystemActions.landingOnBack, CurrentMechInputs, frameNumber);
                return;
            }
            mech.characterState = MechCharacterState.Standing;
            TryStartAction(SystemActions.normalLanding, CurrentMechInputs, frameNumber);
        }

        void ApplyMovementData(MovementData m) {
            var force = m.localAcceleration;
            if (force == Vector3Int.zero)
                return;
            if (force.x != 0 || force.z != 0)
                force = MechUtils.RotateVector3AroundY(force, mech.aimData.Heading); //this is wrong
            mech.physicsComponent.AddForce(force, m.ignoreWeight, m.targetSpeed);
        }
        
        public override void ResetState() {
            if (currentAction != null && currentAction.LockOnOnStart)
                mech.lockedOn = lockOnCache;
            mech.selectedWeaponId = -1;
            mech.CurrentWeaponRange = 0;
            mech.hitboxManager.ClearHitFlags(mech.handle);
            nextEventId = 0;
            currentAction = null; //this is important!
            currentActionFrame = 0;
            isMovementDataSet = false;
            mech.isBlocking = false;
            lastHitResult = HitResult.Whiff;
            currentActionCancels = default;
        }

        public override void Serialize(BinaryWriter bw) {
            bw.Write(nextEventId);
            bw.Write(currentActionFrame);
            //bw.Write(currentAction); todo
            bw.Write(isMovementDataSet);
            //bw.Write(currentMovementData); todo
        }

        public override void Deserialize(BinaryReader br) {
            nextEventId = br.ReadByte();
            currentActionFrame = br.ReadUInt32();
            //currentAction; todo
            isMovementDataSet = br.ReadBoolean();
            //currentMovementData = todo
        }

        public override int GetHashCode() {
            return HashCode.Of(nextEventId).And(currentActionFrame).And(isMovementDataSet);
            //todo: add current action and current movementData
        }
    }
}