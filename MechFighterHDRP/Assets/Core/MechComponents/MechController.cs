using System.IO;
using Core.MechActions;
using Core.MechMovesets;
using Core.MechMovesets.MechActions;
using Core.Utility;
using MechFighter;
using UnityEngine;

namespace Core.MechComponents {
    
    using static Structs;
    using static MechGameConstants;
    public class MechController : MechComponent {

        const int MAX_DOWN_TIME = 60;
        const int SHIELD_COST_TICK = 4;
        
        readonly MechMoveset moveset;
        
        #region GameState
        readonly bool[] bufferedButtons = new bool[(int) Buttons.Count];
        public DirectionalInput CurrentDirectionalInput { get; private set; }
        int framesSinceButtonChange;
        public AimData CurrentAimData { get; private set; }

        #endregion

        public SystemActions SystemActions => moveset.systemActions;
        public MechAction[] SpecialActions => moveset.specialActions;

        public MechController(MechMoveset mechMoveset) {
            moveset = mechMoveset;
        }

        bool wasLockOnPressed;
        int downTime;
        
        public void UpdateInputs(MechInputs mechInputs) {
            CurrentDirectionalInput = MechUtils.Vector2ToDirectionalInput(mechInputs.movement);
            CurrentAimData = mechInputs.aimData;
            LockOnToggle(mechInputs.LockOn);
            HandleBuffer(mechInputs.buttons);
        }

        void LockOnToggle(bool lockOnPressed) {
            if (lockOnPressed && !wasLockOnPressed) {
                mech.lockedOn = !mech.lockedOn;
                mech.lockOnTarget = mech.handle == 0 ? (byte) 1 : (byte) 0;
                mech.CurrentWeaponRange = 0;
            }
            wasLockOnPressed = lockOnPressed;

        }

        public bool ButtonPressedOrBuffered(Buttons button) {
            return framesSinceButtonChange < INPUT_REPEAT_BUFFER_WINDOW && bufferedButtons[(int) button];
        }

        #region Private Methods

        public void Update(MechInputs mechInputs, int frameNumber) {
            if (mech.health < 0)
                return;
            if (!mech.IsActionable && !mech.inBlockStun) {
                if (!mech.actionHandler.TryUpdateOrFinish(mechInputs, frameNumber))
                    return;
            }

            if (HandleKnockdown(mechInputs, frameNumber))
                return;
            
            HandleCrouching(mechInputs);
            if (mech.inBlockStun)
                return;
            if (TryStartNewAction(mechInputs, frameNumber, false)) {
                mech.actionHandler.TryUpdateOrFinish(mechInputs, frameNumber);
                //this is here so that action event on frame 0 actually happens on frame 0, and so every other
                return;
            }
            HandleBlocking(mechInputs.Guard, frameNumber);
            HandleFreeRotation(mechInputs.aimData.Heading, mechInputs.movement);
            if (HandleMovement(mechInputs.movement)) 
                return;
            if (mech.characterState == MechCharacterState.Running)
                mech.characterState = MechCharacterState.Standing;
        }

        void HandleBlocking(bool blocking, int frameNumber) {
            mech.isBlocking = blocking && mech.grounded && mech.dashMeter > 0;
            if (mech.isBlocking) {
                mech.dashMeter -= SHIELD_COST_TICK;
                mech.dashGainLockoutStart = frameNumber;
            }
        }

        public bool TryStartNewAction(MechInputs mechInputs, int frameNumber, bool isCancel, ActionCancels cancels = default) {
            if (ButtonPressedOrBuffered(Buttons.Jump) && (cancels.jump || !isCancel))
                if (TrySelectAndStartJump(mechInputs, frameNumber)) {
                    mech.characterState = MechCharacterState.Jumping;
                    return true;
                }
            if (ButtonPressedOrBuffered(Buttons.Dash) && (cancels.dash || !isCancel))
                if (TrySelectAndStartDash(mechInputs, frameNumber)) {
                    mech.characterState = MechCharacterState.Dashing;
                    return true;
                }
            if (ButtonPressedOrBuffered(Buttons.Melee) && ButtonPressedOrBuffered(Buttons.Guard) && (cancels.grab || !isCancel))
                if (TrySelectAndStartAttack(AttackType.Grab, mechInputs, frameNumber))
                    return true;
            if (ButtonPressedOrBuffered(Buttons.Primary) && (cancels.primary || !isCancel))
                if (TrySelectAndStartAttack(AttackType.Primary, mechInputs, frameNumber))
                    return true;
            if (ButtonPressedOrBuffered(Buttons.Secondary) && (cancels.secondary || !isCancel))
                if (TrySelectAndStartAttack(AttackType.Secondary, mechInputs, frameNumber))
                    return true;
            if (ButtonPressedOrBuffered(Buttons.Melee) && (cancels.melee || !isCancel))
                if (TrySelectAndStartAttack(AttackType.Melee, mechInputs, frameNumber))
                    return true;
            if (ButtonPressedOrBuffered(Buttons.Guard) && cancels.block) {
                //start blocking
                Debug.Log("cancel by block");
                return true;
            }
            return false;
        }
        
        bool TrySelectAndStartJump(MechInputs mechInputs, int frameNumber) {
            if (!mech.grounded)
                return mech.actionHandler.TryStartAction(moveset.movementActions.doubleJumpAction, mechInputs, frameNumber);
            return mech.actionHandler.TryStartAction(moveset.movementActions.jumpAction, mechInputs, frameNumber);
        }
        
        bool TrySelectAndStartDash(MechInputs mechInputs, int frameNumber) {
            if (!mech.grounded)
                return mech.actionHandler.TryStartAction(moveset.movementActions.airDashAction, mechInputs, frameNumber);
            if (mechInputs.movement.y < 0)
                return mech.actionHandler.TryStartAction(moveset.movementActions.backDashAction, mechInputs, frameNumber);
            return mech.actionHandler.TryStartAction(moveset.movementActions.dashAction, mechInputs, frameNumber);
        }
        
        bool TrySelectAndStartAttack(AttackType attackType, MechInputs mechInputs, int frameNumber) {
            int type = (int) attackType;
            int state = GetCharacterStateId();
            int direction = (int) CurrentDirectionalInput;
            
            if (state > (int) MechCharacterState.Running)
                return false;
            
            if (mech.actionHandler.TryStartAction(moveset.attacks[state, type, direction], mechInputs, frameNumber))
                return true;
            
            //defaulting to other moves with the same buttons
            if (direction != 0 && mech.actionHandler.TryStartAction(moveset.attacks[state, type, 0], mechInputs, frameNumber))
                return true;
            if (state == 0)
                return false;
            
            if (!mech.grounded) {
                if (TryStartAerialAction(mechInputs, frameNumber, type, direction)) {
                    mech.characterState = MechCharacterState.Jumping;
                    return true;
                }
                return false;
            }

            if (TryStartStandingAction(mechInputs, frameNumber, type, direction)) {
                mech.characterState = MechCharacterState.Standing;
                return true;
            }
            return false;
        }

        bool TryStartStandingAction(MechInputs mechInputs, int frameNumber, int type, int direction) {
            return direction != 0 && mech.actionHandler.TryStartAction(moveset.attacks[0, type, direction], mechInputs, frameNumber) 
                   || mech.actionHandler.TryStartAction(moveset.attacks[0, type, 0], mechInputs, frameNumber);
        }

        bool TryStartAerialAction(MechInputs mechInputs, int frameNumber, int type, int direction) {
            if (mech.characterState == MechCharacterState.Jumping)
                return false;
            return direction != 0 && mech.actionHandler.TryStartAction(moveset.attacks[(int) MechCharacterState.Jumping, type, direction], mechInputs, frameNumber) 
                   || mech.actionHandler.TryStartAction(moveset.attacks[(int) MechCharacterState.Jumping, type, 0], mechInputs, frameNumber);
        }

        bool HandleKnockdown(MechInputs mechInputs, int frameNumber) {
            if (mech.characterState == MechCharacterState.Knockdown) {
                if (MechUtils.AnyButtonsPressed(mechInputs.buttons) || mechInputs.movement != Vector2Int.zero || downTime > MAX_DOWN_TIME) {
                    downTime = 0;
                    mech.actionHandler.TryStartAction(SystemActions.getUp, mechInputs, frameNumber);
                }
                downTime++;
                return true;
            }
            return false;
        }

        int GetCharacterStateId() {
            if (mech.characterState == MechCharacterState.Dashing)
                return mech.grounded ? (int) MechCharacterState.Running : (int) MechCharacterState.Jumping;
            return (int) mech.characterState;
        }
        
        void HandleCrouching(MechInputs mechInputs) {
            if (!mech.grounded)
                return;
            switch (mech.characterState) {
                case MechCharacterState.Crouching:
                case MechCharacterState.Standing:
                    mech.characterState = mechInputs.Crouch ? MechCharacterState.Crouching : MechCharacterState.Standing;
                    break;
                case MechCharacterState.Running:
                    if (mechInputs.Crouch)
                        mech.characterState = MechCharacterState.Crouching;
                    break;
            }
        }
        
        bool HandleMovement(Vector2Int movementInput) {
            if (movementInput.sqrMagnitude < DIRECTIONAL_INPUT_THRESHOLD * DIRECTIONAL_INPUT_THRESHOLD)
                return false;

            if (mech.characterState == MechCharacterState.Crouching || mech.isBlocking)
                return false;
            
            if (!mech.grounded) 
                mech.physicsComponent.HandleHorizontalMovement(movementInput, mech.parameters.airDriftSpeed, mech.parameters.airDriftAcceleration);
            else if (mech.characterState == MechCharacterState.Running)
                mech.physicsComponent.HandleHorizontalMovement(new Vector2Int(0, SCALE), mech.parameters.runSpeed, mech.parameters.walkAcceleration);
            else
                mech.physicsComponent.HandleHorizontalMovement(movementInput, mech.parameters.walkSpeed, mech.parameters.walkAcceleration);
            return true;
        }

        void RotateTowardsInputDirection(ushort cameraHeading, Vector2Int movementInput) {
            var targetAngle = cameraHeading + MechUtils.GetVectorHeading(movementInput);
            mech.aimData.Heading = MechUtils.RotateTowards(mech.aimData.Heading, targetAngle, mech.parameters.turnSpeed);
        }
        
        void HandleFreeRotation(ushort aimDataHeading, Vector2Int movementInput) {
            if (mech.characterState == MechCharacterState.Crouching)
                return;
            if (mech.characterState == MechCharacterState.Running) {
                RotateTowardsInputDirection(aimDataHeading, movementInput);
                return;
            }
            mech.aimData.Heading = MechUtils.RotateTowards(mech.aimData.Heading, aimDataHeading, mech.parameters.turnSpeed);
        }

        void HandleBuffer(bool[] buttons) {
            if (!AreButtonsTheSame(bufferedButtons, buttons)) {
                buttons.CopyTo(bufferedButtons, 0);
                framesSinceButtonChange = 0;
            }

            framesSinceButtonChange++;
        }
        
        bool AreButtonsTheSame(bool[] buttons1, bool[] buttons2) {
            for (int i = 0; i < buttons1.Length; i++)
                if (buttons1[i] != buttons2[i])
                    return false;
            return true;
        }

        #endregion
        
        #region Serialization
        public override void ResetState() {
            downTime = 0;
            //no need to reset previous buttons
        }

        public override void Serialize(BinaryWriter bw) {
            for (int i = 0; i < bufferedButtons.Length; i++)
                bw.Write(bufferedButtons[i]);
            bw.Write(framesSinceButtonChange);
        }
        
        public override void Deserialize(BinaryReader br) {
            for (int i = 0; i < bufferedButtons.Length; i++)
                bufferedButtons[i] = br.ReadBoolean();
            framesSinceButtonChange = br.ReadInt32();
        }

        
        // @LOOK Not hashing bufferedButtons, would a game desync... it would...
        public override int GetHashCode() {
            return HashCode.Of(framesSinceButtonChange).AndEach(bufferedButtons);
        }

        #endregion
    }
}