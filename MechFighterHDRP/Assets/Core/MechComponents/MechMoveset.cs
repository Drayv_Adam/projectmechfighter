using System;
using Core.MechActions;

namespace Core.MechMovesets {
    
    public struct MechMoveset {
        public readonly MovementActions movementActions;
        public readonly MechAction[,,] attacks;
        public readonly SystemActions systemActions;
        public readonly MechAction[] specialActions;
        
        public MechMoveset(MovementActions movementActions, MechAction[,,] attacks, SystemActions systemActions, MechAction[] specialActions) {
            this.movementActions = movementActions;
            this.attacks = attacks;
            this.systemActions = systemActions;
            this.specialActions = specialActions;
        }
    }

    [Serializable]
    public struct MovementActions {
        public MechAction jumpAction;
        public MechAction doubleJumpAction;
        public MechAction dashAction;
        public MechAction backDashAction;
        public MechAction airDashAction;
    }

    [Serializable]
    public struct SystemActions {
        public MechAction normalLanding;
        public MechAction landingOnBack;
        public MechAction getUp;
        public MechAction[] getHitLight;
        public MechAction getHitOverhead;
        public MechAction getHitStanding;
        public MechAction getHitAir;
        public MechAction knockDown;
        public MechAction deathStanding;
    }
    
    public enum AttackType {
        Melee,
        Primary,
        Secondary,
        Grab,
        Count
    }
}