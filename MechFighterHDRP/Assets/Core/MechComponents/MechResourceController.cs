using System.IO;
using Core.Utility;
using MechFighter;
using UnityEngine;

namespace Core.MechComponents {
    
    using static MechGameConstants;
    public class MechResourceController : MechComponent {

        const long MIN_DIST = (long)4 * 4 * SCALE * SCALE;
        const long MAX_DIST = (long)18 * 18 * SCALE * SCALE;

        const short MAX_SUPER_REGEN = 15;
        const short MIN_SUPER_REGEN = 1;
        
        const int SUPER_REGEN_LOCKOUT_DUR = 60;
        const int DASH_REGEN_LOCKOUT_DUR = 30;
        
        
        public void Update(int frameNumber) {
            HandleDashRegen(frameNumber);
            HandleSuperRegen(frameNumber);
        }

        void HandleDashRegen(int frameNumber) {
            if (mech.dashGainLockoutStart + DASH_REGEN_LOCKOUT_DUR > frameNumber)
                return;
            if (mech.dashMeter < mech.parameters.dash && mech.grounded) {
                mech.dashMeter += 100;
            }
        }

        void HandleSuperRegen(int frameNumber) {
            if (mech.superGainLockoutStart + SUPER_REGEN_LOCKOUT_DUR > frameNumber)
                return;
            if (mech.specialMeter < mech.parameters.super && mech.grounded) {
                var superIncrement = GetSuperIncrement();
                mech.specialMeter += superIncrement;
                mech.specialMeter = (short)Mathf.Clamp(mech.specialMeter, 0, mech.parameters.super);
            }
        }

        short GetSuperIncrement() {
            var sqr = DistanceFromCenterSqr();
            if (sqr < MIN_DIST)
                return MAX_SUPER_REGEN;
            if (sqr > MAX_DIST)
                return MIN_SUPER_REGEN;
            var ratio = (float)(sqr - MIN_DIST) / (MAX_DIST - MIN_DIST);
            ratio = 1 - ratio;
            return (short)(ratio * (MAX_SUPER_REGEN - MIN_SUPER_REGEN) + MIN_SUPER_REGEN); //todo: make this not use float
        }
        
        long DistanceFromCenterSqr() {
            var pos = mech.position;
            pos.y = 0;
            return MechUtils.SqrMagnitude(pos);
        }
        
        public override void ResetState() {
            //
        }

        public override void Serialize(BinaryWriter bw) {
            //
        }

        public override void Deserialize(BinaryReader br) {
            //
        }

        public override int GetHashCode() {
            return 0;
        }
    }
}