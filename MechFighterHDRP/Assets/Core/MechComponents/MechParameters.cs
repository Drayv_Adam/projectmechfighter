using System;

namespace Core.MechComponents {
    [Serializable]
    public struct MechParameters {
        public ushort health;
        public ushort dash;
        public ushort super;
        public ushort walkSpeed;
        public ushort walkAcceleration;
        public ushort runSpeed;
        public ushort runAcceleration;
        public ushort airDriftAcceleration;
        public ushort airDriftSpeed;
        public ushort turnSpeed;
        public ushort groundFriction;
        public ushort fallAcceleration;
        public ushort airDrag;
        public ushort weight;
        
        public static MechParameters Default => new MechParameters() {
            health = 10000,
            dash = 10000,
            super = 10000,
            walkSpeed = 500,
            walkAcceleration = 100,
            runSpeed = 1800,
            runAcceleration = 400,
            airDriftAcceleration = 50,
            airDriftSpeed = 800,
            turnSpeed = 16000,
            groundFriction = 50,
            fallAcceleration = 160,
            airDrag = 0,
            weight = 10000,
        };
    }
}