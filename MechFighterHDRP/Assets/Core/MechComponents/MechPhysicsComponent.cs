using System.IO;
using Core.Utility;
using MechFighter;
using UnityEngine;

namespace Core.MechComponents {
    
    using static MechGameConstants;
    using static MechUtils;
    
    public class MechPhysicsComponent : MechComponent {

        #region nonGameState

        readonly int boundsRadius;
        readonly MechParameters parameters;

        #endregion
        
        #region GameState
        
        public bool Disabled { get; set; }

        public bool GravityDisabled { get; set; }
        
        public bool DisableGroundCheck { get; set; }

        #endregion

        #region Serialization

        public override void ResetState() {
            Disabled = false;
            GravityDisabled = false;
        }

        public override void Serialize(BinaryWriter bw) {
            bw.Write(Disabled);
            bw.Write(GravityDisabled);
        }

        public override void Deserialize(BinaryReader br) {
            Disabled = br.ReadBoolean();
            GravityDisabled = br.ReadBoolean();
        }

        public override int GetHashCode() {
            return HashCode
                .Of(Disabled)
                .And(GravityDisabled);
        }

        #endregion
        
        public MechPhysicsComponent(int boundsRadius, MechParameters mechParameters) {
            this.boundsRadius = boundsRadius;
            parameters = mechParameters;
            Disabled = true;
        }

        #region Public Methods

        public void Update(int frameNumber) {
            if (Disabled)
                return;
            CheckCollisions(frameNumber);
            ApplyGravity();
            FinalMovement();
            ApplyDrag();
        }

        public void HardResetVelocity() {
            mech.velocity = Vector3Int.zero;
        }
        
        /// <summary>
        /// For local space velocity changes
        /// </summary>
        /// <param name="movementInput"></param>
        /// <param name="targetSpeed"></param>
        /// <param name="acceleration"></param>
        /// <param name="onlyMoveForward">I don't know lol</param>
        public void HandleHorizontalMovement(Vector2Int movementInput, ushort targetSpeed, ushort acceleration) {
            var hVelocity = new Vector2Int(mech.velocity.x, mech.velocity.z);

            movementInput = ClampMagnitude(movementInput, SCALE);
            movementInput = RotateVector2(movementInput, mech.aimData.Heading);
            
            //how fast we want to go minus how fast we're going
            //if we're going faster than we want to go, delta will be opposite 
            var horizontalForce = (movementInput * targetSpeed) / SCALE - hVelocity;
            horizontalForce = ClampMagnitude(horizontalForce, acceleration);

            hVelocity += horizontalForce;

            mech.velocity.x = hVelocity.x;
            mech.velocity.z = hVelocity.y;
        }
        
        /// <summary>
        /// For global velocity changes
        /// </summary>
        /// <param name="force"></param>
        /// <param name="targetMaxSpeed"></param>
        /// <param name="ignoreWeight"></param>
        public void AddForce(Vector3Int force, bool ignoreWeight = false, int targetMaxSpeed = MAX_SPEED) {
            if (!ignoreWeight)
                force *= SCALE / parameters.weight;
            
            if (targetMaxSpeed < MAX_SPEED) { //Naturally slowdown to desirable velocity if going faster
                if (SqrMagnitude(mech.velocity + force) > targetMaxSpeed * targetMaxSpeed) {
                    var deltaForce = Normalize(force) * targetMaxSpeed / SCALE - mech.velocity;
                    deltaForce = ClampMagnitude(deltaForce, Magnitude(force));
                    mech.velocity += deltaForce;
                    return;
                }
            }

            mech.velocity += force;
        }

        #endregion

        #region Checks

        void CheckCollisions(int frameNumber) {
            CheckGround(frameNumber);
        }

        void CheckGround(int frameNumber) {
            if (mech.position.y < 1) {
                mech.position.y = 0;
                if (mech.velocity.y < 0) {
                    mech.velocity.y = 0;
                }
                if (mech.grounded || DisableGroundCheck) return;
                mech.grounded = true;
                mech.TriggerOnLanding(frameNumber);
            }
            else if (mech.grounded && !DisableGroundCheck) {
                mech.grounded = false;
                mech.TriggerOnLanding(frameNumber);
            }
        }

        #endregion
        
        #region Private Methods

        void ApplyGravity() {
            if (GravityDisabled)
                return;
            if (!mech.grounded && parameters.fallAcceleration > 0)
                AddForce(Vector3Int.down * parameters.fallAcceleration, true);
        }

        void ApplyDrag() {
            var dragForce = mech.grounded ? parameters.groundFriction : parameters.airDrag;
            if (SqrMagnitude(mech.velocity) < dragForce * dragForce) //dragForce^2 shouldn't be over maxIntValue
                mech.velocity = Vector3Int.zero;
            else if (dragForce > 0)
                mech.velocity -= Normalize(mech.velocity) * dragForce / SCALE;
        }
        
        void FinalMovement() {
            mech.velocity = ClampMagnitude(mech.velocity, MAX_SPEED);
            mech.position = TryMovement(mech.position, ref mech.velocity);
        }
        
        Vector3Int TryMovement(Vector3Int pos, ref Vector3Int velocity) {
            var target = pos + velocity;
            if (SqrMagnitude(target) > (long)boundsRadius * (long)boundsRadius) {
                target = NormalizeLarge(target) * (boundsRadius / SCALE); //warning: boundsRadius has to be multiple of SCALE
                velocity = target - pos;
            }

            return target;
        }

        #endregion
    }
}