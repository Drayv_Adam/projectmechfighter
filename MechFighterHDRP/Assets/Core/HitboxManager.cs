using System.Collections.Generic;
using Core.MechMovesets.MechActions;
using Core.Utility;
using GameViewAssets;
using MechFighter;
using UnityEngine;

namespace Core {
    using static MechGameConstants;
    using static MechUtils;

    public class HitboxManager {
        const long MIN_DIST_SQR = 16 * SCALE * SCALE;
        //there shouldn't be any hitbox farther than half of 2 * SCALE from the mech's pivot,
        //because hits above this distance are ignored

        //List of hitscans
        
        static readonly Dictionary<Collider, byte> ColliderToMechDict = new Dictionary<Collider, byte>();

        public readonly HitboxData[] mechHitboxData;
        public readonly Structs.HitScanData[] hitScanData;
        readonly bool[] hitflags;
        readonly bool[] hitScanFlags;
        
        readonly Collider[] hits = new Collider[20];
        readonly RaycastHit[] raycastHits = new RaycastHit[10];
        public HitboxManager(int playerNum) {
            ColliderToMechDict.Clear();
            mechHitboxData = new HitboxData[playerNum];
            hitflags = new bool[playerNum];
            hitScanData = new Structs.HitScanData[playerNum];
            hitScanFlags = new bool[playerNum];
        }
        
        public void CheckHits(Mech[] mechs, int frameNumber) {
            CheckHitboxes(mechs, frameNumber);
            CheckHitScans(mechs, frameNumber);
        }

        void CheckHitboxes(Mech[] mechs, int frameNumber) {
            //fine as long as only 2 player game. Don't bother checking hitboxes, if players too far
            if (!ArePositionsCloseEnough(mechs[0].position, mechs[1].position))
                return;

            for (int i = 0; i < mechHitboxData.Length; i++) {
                //to make hits hit more than 1 targets, hitflags need to be set per potential target, and only filter potential targets, not stop searching alltogather
                if (mechHitboxData[i].hitboxes == null || hitflags[i]) 
                    continue;
                CheckHitboxDataHits(mechs, i, frameNumber);
            }
        }

        void CheckHitScans(Mech[] mechs, int frameNumber) {

            for (int i = 0; i < hitScanData.Length; i++) {
                if (hitScanFlags[i] || hitScanData[i] == null)
                    continue;
                CheckHitScanDataHits(mechs, i, frameNumber);
            }
            
            HandleHitScanDecay();
        }

        public void UpdateHitboxData(byte mechHandle, HitboxData data) {
            if (data.resetHitFlags)
                hitflags[mechHandle] = false;
            mechHitboxData[mechHandle] = data;
        }

        public void AddHitScan(int handle, Structs.HitScanData data) {
            if (data.hitScanProperties.resetHitFlags)
                hitScanFlags[handle] = false;
            hitScanData[handle] = data;
        }

        public void ClearHitFlags(byte mechHandle) {
            hitflags[mechHandle] = false;
            mechHitboxData[mechHandle].hitboxes = null;
        }

        void CheckHitboxDataHits(Mech[] mechs, int attackerId, int frameNumber) {
            for (int i = 0; i < mechHitboxData[attackerId].hitboxes.Length; i++) {
                //todo check for clashes
                if (CheckHitboxHits(attackerId, mechHitboxData[attackerId].hitboxes[i], mechs, out int targetHit)) {
                    HandleMechHit(attackerId, mechHitboxData[attackerId].hitboxes[i].properties, mechHitboxData[attackerId].hitboxes[i].attributes, targetHit, mechs, frameNumber);
                    hitflags[attackerId] = true;
                    return; //we wouldn't return here, if we want to handle multiple target hits, just add target to ignore list
                }
            }
        }

        bool CheckHitboxHits(int attackerId, Structs.Hitbox hitbox, Mech[] mechs, out int targetHit) {
            targetHit = -1;
            var attacker = mechs[attackerId];
            for (int i = 0; i < hitbox.spheres.Length; i++) {
                if (CheckSphere(attacker.position, attacker.aimData.Heading, hitbox.spheres[i], attackerId, out targetHit)) {
                    return true; //we wouldn't return here, if we want to handle multiple target hits
                }
            }
            return false;
        }

        bool CheckSphere(Vector3Int position, ushort heading, Structs.Sphere sphere, int attackerId, out int targetHit) {
            targetHit = -1;
            if (sphere.center != Vector3Int.zero) {
                sphere.center = RotateVector3AroundY(sphere.center, heading);
            }
            var pos = IntToVector3(position + sphere.center);
            var hitCount = Physics.OverlapSphereNonAlloc(pos, IntToFloat(sphere.radius), hits, MechGameRunner.LayerMask);
            if (hitCount == 0)
                return false;
            if (hitCount > hits.Length)
                hitCount = hits.Length;
            for (int i = 0; i < hitCount; i++) {
                targetHit = GetHitMechId(hits[i]);
                if (targetHit != attackerId)
                    return true; //we wouldn't return here, if we want to handle multiple target hits
            }

            return false;
        }

        void CheckHitScanDataHits(Mech[] mechs, int attackerId, int frameNumber) {
            var data = hitScanData[attackerId];
            if (CheckRay(data.origin, data.aimData, data.hitScanProperties.hitScanRange, attackerId, out int targetHit)) {
                hitScanFlags[attackerId] = true;
                HandleMechHit(attackerId, hitScanData[attackerId].hitScanProperties.hitProperties, hitScanData[attackerId].hitScanProperties.hitboxAttributes, targetHit, mechs, frameNumber);
            }
        }

        bool CheckRay(Vector3Int origin, Structs.AimData aimData, uint maxDistance, int attackerId, out int targetHit) {
            targetHit = -1;

            var direction = Quaternion.Euler(
                PitchToFloat(aimData.pitch), 
                HeadingToFloat(aimData.Heading), 
                0) * Vector3.forward;
            
            var ray = new Ray(IntToVector3(origin), direction);

            var hitCount = Physics.RaycastNonAlloc(ray, raycastHits, IntToFloat((int)maxDistance), MechGameRunner.LayerMask);
            if (hitCount == 0)
                return false;
            if (hitCount > raycastHits.Length)
                hitCount = raycastHits.Length;
            for (int i = 0; i < hitCount; i++) {
                targetHit = GetHitMechId(raycastHits[i].collider);
                if (targetHit != attackerId)
                    return true;
            }
            
            return false;
        }
        
        void HandleHitScanDecay() {
            for (int i = 0; i < hitScanData.Length; i++) {
                if (hitScanData[i] == null)
                    return;
                hitScanData[i].hitScanProperties.lifeTime--;
                if (hitScanData[i].hitScanProperties.lifeTime == 0) {
                    hitScanData[i] = null;
                    hitScanFlags[i] = false;
                }
            }
        }

        void HandleMechHit(int attacker, Structs.HitProperties hitProperties, Structs.HitboxAttributes attributes, int target, Mech[] mechs, int frameNumber) {
            mechs[attacker].actionHandler.NotifyHitResult(Structs.HitResult.Hit);
            mechs[target].hitController.GetFucked(hitProperties, attributes, mechs[attacker].aimData.Heading, mechs[attacker].position, frameNumber);
        }
        
        bool ArePositionsCloseEnough(Vector3Int pos1, Vector3Int pos2) {
            return SqrMagnitude(pos1 - pos2) < MIN_DIST_SQR;
        }

        int GetHitMechId(Collider hit) {
            if (ColliderToMechDict.ContainsKey(hit))
                return ColliderToMechDict[hit];
            var mechView = hit.GetComponentInParent<MechView>();
            ColliderToMechDict.Add(hit, mechView.CurrentState.handle);
            return mechView.CurrentState.handle;
        }
        
        //todo: Serialize
    }
}