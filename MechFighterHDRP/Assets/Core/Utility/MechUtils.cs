﻿using System;
using MechFighter;
using UnityEngine;

namespace Core.Utility {
    
    using static MechGameConstants;
    
    public static class MechUtils {
        public static int FloatToScaledInt(float number) {
            return Mathf.RoundToInt(number * SCALE);
        }

        public static ushort FloatToHeading(float angle) {
            return WrapHeading(Mathf.RoundToInt(angle * ANGLE_SCALE));
        }

        public static float IntToFloat(int number) {
            return (float)number / SCALE;
        }

        public static float HeadingToFloat(ushort angle) {
            return (float) angle / ANGLE_SCALE;
        }

        public static float PitchToFloat(short pitch) {
            return (float) pitch / ANGLE_SCALE;
        }
        
        public static Vector3 IntToVector3(Vector3Int vector) {
            return new Vector3(IntToFloat(vector.x), IntToFloat(vector.y), IntToFloat(vector.z));
        }
        
        public static Vector3Int Vector3ToInt(Vector3 vector) {
            return new Vector3Int(FloatToScaledInt(vector.x), FloatToScaledInt(vector.y), FloatToScaledInt(vector.z));
        }

        public static Vector2Int Vector2ToInt(Vector2 vector) {
            return new Vector2Int(FloatToScaledInt(vector.x), FloatToScaledInt(vector.y));
        }

        public static long SqrMagnitude(Vector2Int v) {
            return (long)v.x * v.x + (long)v.y * v.y;
        }
        
        public static long SqrMagnitude(Vector3Int v) {
            return (long)v.x * v.x + (long)v.y * v.y + (long)v.z * v.z;
        }

        public static int Magnitude(Vector3Int v) {
            return (int) Math.Sqrt(SqrMagnitude(v));
        }

        public static long LargeVectorMagnitude(Vector3Int largeVector) {
            return (long) Math.Sqrt(SqrMagnitude(largeVector));
        }
        
        public static Vector3Int NormalizeLarge(Vector3Int largeVector) {
            long length = LargeVectorMagnitude(largeVector);

            largeVector.x = (int)(largeVector.x * SCALE / length);
            largeVector.y = (int)(largeVector.y * SCALE / length);
            largeVector.z = (int)(largeVector.z * SCALE / length);

            return largeVector;
        }
        
        public static Vector3Int Normalize(Vector3Int vector) {
            int length = (int)vector.magnitude;
            vector.x = vector.x * SCALE / length;
            vector.y = vector.y * SCALE / length;
            vector.z = vector.z * SCALE / length;
            return vector;
        }
        
        public static Vector2Int Normalize(Vector2Int vector) {
            int length = (int)vector.magnitude;
            vector.x = vector.x * SCALE / length;
            vector.y = vector.y * SCALE / length;
            return vector;
        }
        
        public static Vector3Int ClampMagnitude(Vector3Int vector, int maxLength) {
            if (SqrMagnitude(vector) < (long)maxLength * (long)maxLength)
                return vector;
            return Normalize(vector) * maxLength / SCALE;
        }

        public static Vector2Int ClampMagnitude(Vector2Int vector, int maxLength) {
            if (SqrMagnitude(vector) <= maxLength * maxLength)
                return vector;
            return Normalize(vector) * maxLength / SCALE;
        }
        
        public static int GetMovement(byte value) {
            return (value - MOVEMENT_VALUES) * SCALE / MOVEMENT_VALUES;
        }

        public static byte SerializeMovementInput(int value) {
            return (byte) (value * MOVEMENT_VALUES / SCALE + MOVEMENT_VALUES);
        }

        public static Vector3Int RotateVector3AroundY(Vector3Int v, int angle) {
            var cs = TrigonometryCache.Cos(-angle);
            var sn = TrigonometryCache.Sin(-angle);

            return new Vector3Int(
                (v.x * cs - v.z * sn) / SCALE,
                v.y,
                (v.x * sn + v.z * cs) / SCALE);
        }
        
        public static Vector2Int RotateVector2(Vector2Int v, int angle) {
            var cs = TrigonometryCache.Cos(-angle);
            var sn = TrigonometryCache.Sin(-angle);

            return new Vector2Int(
                (v.x * cs - v.y * sn) / SCALE,
                (v.x * sn + v.y * cs) / SCALE);
        }

        public static ushort WrapHeading(int angle) {
            if (angle >= 360 * ANGLE_SCALE)
                angle %= 360 * ANGLE_SCALE;
            if (angle < 0) {
                angle += 360 * ANGLE_SCALE;
            }
            return (ushort)angle;
        }

        public static ushort GetVectorHeading(Vector2Int v) {
            return (ushort)(Math.Atan2(v.x, v.y) * Mathf.Rad2Deg * ANGLE_SCALE); //floating point!!!
        }
        
        public static ushort RotateTowards(int startAngle, int targetAngle, ushort maxRotation) {
            var delta = targetAngle - startAngle;
            if (delta > 180 * ANGLE_SCALE)
                delta -= 360 * ANGLE_SCALE;
            else if (delta < -180 * ANGLE_SCALE)
                delta += 360 * ANGLE_SCALE;
            if (Mathf.Abs(delta) <= maxRotation)
                return WrapHeading(startAngle + delta);
            if (delta < 0)
                return WrapHeading(startAngle - maxRotation);
            return WrapHeading(startAngle + maxRotation);
        }
        
        public static Structs.DirectionalInput Vector2ToDirectionalInput(Vector2Int v) {
            if (v.x < -DIRECTIONAL_INPUT_THRESHOLD)
                return Structs.DirectionalInput.Left;
            if (v.x > DIRECTIONAL_INPUT_THRESHOLD)
                return Structs.DirectionalInput.Right;
            if (v.y > DIRECTIONAL_INPUT_THRESHOLD)
                return Structs.DirectionalInput.Forward;
            if (v.y < -DIRECTIONAL_INPUT_THRESHOLD)
                return Structs.DirectionalInput.Back;
            return Structs.DirectionalInput.Neutral;
        }
        
        public static bool AnyButtonsPressed(bool[] buttons) {
            foreach (var button in buttons) {
                if (button)
                    return true;
            }
            return false;
        }
    }
}