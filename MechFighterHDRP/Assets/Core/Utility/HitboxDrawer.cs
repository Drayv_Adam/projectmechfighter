using Core.MechMovesets.MechActions;
using SharedGame;
using UnityEngine;

namespace Core.Utility {
    public class HitboxDrawer : MonoBehaviour {
#if UNITY_EDITOR
        [SerializeField] Color HitboxIdColor = Color.white;
        
        public HitboxData[] hitboxData;
        public Structs.HitScanData[] hitScanData;

        Mech[] mechs;

        GameManager GameManager => GameManager.Instance;

        void OnValidate() {
            if (HitboxDrawerHelper.style != null)
                HitboxDrawerHelper.style.normal.textColor = HitboxIdColor;
        }


        void OnDrawGizmos() {
            if (GameManager == null || !GameManager.IsRunning || GameManager.Runner == null)
                return;
            GetMechData();

            DrawHitboxes();
            DrawHitScans();
        }

        void DrawHitboxes() {
            if (hitboxData == null || hitboxData.Length == 0)
                return;

            for (int i = 0; i < hitboxData.Length; i++) {
                HitboxDrawerHelper.DrawHitboxData(hitboxData[i], mechs[i].position, mechs[i].aimData.Heading);
            }
        }


        void DrawHitScans() {
            if (hitScanData == null || hitScanData.Length == 0)
                return;
            for (int i = 0; i < hitScanData.Length; i++) {
                HitboxDrawerHelper.DrawHitScanData(hitScanData[i]);
            }
        }
        
        void GetMechData() {
            var game = (MechGame)GameManager.Runner.Game;

            mechs = game.mechs;

            hitboxData = game.hitboxManager.mechHitboxData;

            hitScanData = game.hitboxManager.hitScanData;
        }
#endif
    }
}
