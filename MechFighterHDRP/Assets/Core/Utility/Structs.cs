﻿using System;
using Core.Connection;
using Core.MechComponents;
using Core.MechMovesets;
using UnityEngine;

namespace Core.Utility {
    public static class Structs {
        [Serializable]
        public struct PosRotY {
            public Vector3Int position;
            public int heading;

            public PosRotY(Vector3Int position, int heading) {
                this.position = position;
                this.heading = heading;
            }
        }
        
        [Serializable]
        public struct MechInputs {
            public Vector2Int movement;
            public AimData aimData;
            public bool[] buttons;
            public MechInputs(int buttonCount = (int) Buttons.Count) {
                movement = new Vector2Int();
                aimData = new AimData();
                buttons = new bool[buttonCount];
            }
            #region aliases
            public bool LockOn {
                get => buttons[(int) Buttons.LockOn];
                set => buttons[(int) Buttons.LockOn] = value;
            }
            public bool Jump {
                get => buttons[(int) Buttons.Jump];
                set => buttons[(int) Buttons.Jump] = value;
            }
            public bool Dash {
                get => buttons[(int) Buttons.Dash];
                set => buttons[(int) Buttons.Dash] = value;
            }
            public bool Crouch {
                get => buttons[(int) Buttons.Crouch];
                set => buttons[(int) Buttons.Crouch] = value;
            }
            public bool Primary {
                get => buttons[(int) Buttons.Primary];
                set => buttons[(int) Buttons.Primary] = value;
            }
            public bool Secondary {
                get => buttons[(int) Buttons.Secondary];
                set => buttons[(int) Buttons.Secondary] = value;
            }
            public bool Melee {
                get => buttons[(int) Buttons.Melee];
                set => buttons[(int) Buttons.Melee] = value;
            }
            public bool Guard {
                get => buttons[(int) Buttons.Guard];
                set => buttons[(int) Buttons.Guard] = value;
            }
            public bool Utility1 {
                get => buttons[(int) Buttons.Utility1];
                set => buttons[(int) Buttons.Utility1] = value;
            }
            public bool Utility2 {
                get => buttons[(int) Buttons.Utility2];
                set => buttons[(int) Buttons.Utility2] = value;
            }
            public bool Utility3 {
                get => buttons[(int) Buttons.Utility3];
                set => buttons[(int) Buttons.Utility3] = value;
            }
            public bool Utility4 {
                get => buttons[(int) Buttons.Utility4];
                set => buttons[(int) Buttons.Utility4] = value;
            }
            #endregion
        }

        [Serializable]
        public struct AimData {
            [SerializeField] ushort heading;
            public short pitch;

            public ushort Heading {
                get => heading;
                set => heading = MechUtils.WrapHeading(value);
            }
            
            public AimData(short pitch, ushort heading) {
                this.heading = heading;
                this.pitch = pitch;
            }
        }

        [Serializable]
        public struct MechGameParams {
            public int playerNum;
            public StageInfo.StageInfo stageInfo;
            public PlayerInfo[] playerInfos;
            public MechParameters[] mechParams;
            public MechMoveset[] mechMovesets;
            public bool isGameLocal;
            
            
            public MechGameParams(PlayerInfo[] playerInfos, StageInfo.StageInfo stageInfo, MechParameters[] mechParams,
                MechMoveset[] mechMovesets, bool isGameLocal) {
                playerNum = playerInfos.Length;
                this.playerInfos = playerInfos;
                this.stageInfo = stageInfo;
                this.mechParams = mechParams;
                this.mechMovesets = mechMovesets;
                this.isGameLocal = isGameLocal;
            }
        }
        

        public enum Buttons {
            //Confirm,
            //Back,
            //Pause,
            //Context,
            LockOn,
            Jump,
            Dash,
            Crouch,
            Primary,
            Secondary,
            Melee,
            Guard,
            Utility1,
            Utility2,
            Utility3,
            Utility4,
            Count
        }

        [Serializable]
        public struct MenuButtons {
            public bool confirm;
            public bool back;
            public bool context;
            public bool pause;
        }
        
        [Serializable]
        public struct GameButtons {
            public bool lockOn;
            public bool jump;
            public bool dash;
            public bool crouch;
            public bool primary;
            public bool secondary;
            public bool melee;
            public bool guard;
            public bool utility1;
            public bool utility2;
            public bool utility3;
            public bool utility4;

            public bool[] ToArray() {
                return new bool[] {
                    lockOn,
                    jump,
                    dash,
                    crouch,
                    primary,
                    secondary,
                    melee,
                    guard,
                    utility1,
                    utility2,
                    utility3,
                    utility4
                };
            }
        }

        [Serializable]
        public struct LookSettings {
            public LookSensitivity mouse;
            public LookSensitivity joystick;

            public LookSettings(float defaultSensitivity = 1) {
                mouse = new LookSensitivity() {x = defaultSensitivity, y = defaultSensitivity};
                joystick = new LookSensitivity() {x = defaultSensitivity, y = defaultSensitivity};
            }
        }
        
        [Serializable]
        public struct LookSensitivity {
            [Range (0,1)] public float x;
            public bool xInverted;
            [Range (0,1)] public float y;
            public bool yInverted;
            public float X => xInverted ? -x : x;
            public float Y => yInverted ? -y : y;
        }

        
        [Serializable]
        public struct Hitbox {
            [Header("Set true if it is a new hitbox (false if only 'moving' the hitboxes")]
            public bool resetHitFlags;
            public Sphere[] spheres;
            public HitboxAttributes attributes;
            public HitProperties properties;
            
            [HideInInspector] public bool[] hitFlags;
            //other properties
        }
    
        [Serializable]
        public struct Sphere {
            public Vector3Int center;
            public ushort radius;

            public bool CheckCollisionWith(Sphere sphere) {
                return MechUtils.SqrMagnitude(sphere.center - center) <= (sphere.radius + radius) * (sphere.radius + radius);
            }
        }
        
        [Serializable]
        public struct HitboxAttributes {
            [Header("Don't select multiple except overhead and high")]
            [Tooltip("Standing overheads shouldn't be lowProfiled, jumps often should")]
            public bool isOverhead;
            public bool isLow;
            public bool isThrow;
            [Tooltip("Move can be low profiled")]
            public bool isHigh;
        }

        [Serializable]
        public struct HitProperties {
            [Header("Jab - 300, Medium - 600, Heavy - 800. (+/- 200)")]
            [Tooltip("Subtract 50-200 for positive attributes (o, add 100-200 for negative (slow, short)")]
            public ushort damage;
            [Header("1 - jabs, 2 - pokes&weak lows, 3 - medium, 4 - strong hits, 5 super strong")]
            public byte attackLevel;
            [Header("affects only grounded opponents. Makes them no longer grounded. (sweeps incl)")]
            public bool launcher;
            [Header("Crumple on counter hit. Allows for ch only combos")]
            public bool chCrumple;
            [Header("very bad combo scaling, if it is the first move of the combo")]
            public bool badStarter;
            [Header("Y ignored on grounded opponents. Jump is 3k up, Forward Step ~1000-2000")]
            public Vector3Int localKnockback;
        }

        [Serializable]
        public struct AttackLevelValues {
            public byte hitStop;
            public byte chHitStopBonus;
            public byte hitStun;
            public byte airHitStun;
            public byte chHitStunBonus;
            public byte blockStun;
            public ushort proration;
            public ushort shieldDamage;
            public AttackLevelValues(byte hitStop, byte chHitStopBonus, byte hitStun, byte airHitStun, byte chHitStunBonus, byte blockStun, ushort proration, ushort shieldDamage) {
                this.hitStop = hitStop;
                this.chHitStopBonus = chHitStopBonus;
                this.hitStun = hitStun;
                this.airHitStun = airHitStun;
                this.chHitStunBonus = chHitStunBonus;
                this.blockStun = blockStun;
                this.proration = proration;
                this.shieldDamage = shieldDamage;
            }
        }
        
        [Serializable]
        public struct StanceParams {
            public int weaponId;
            public byte readyEventId;
            public byte attackStartEventId;
            public byte[] triggersA;
            public byte triggerB;
            public byte eventBId;
            public byte triggerC;
            public byte recoveryStartEventId;
        }

        [Serializable]
        public struct HitScanProperties {
            public uint hitScanRange;
            public byte lifeTime;
            public bool resetHitFlags;
            public ushort originHeightOffset;
            public HitProperties hitProperties;
            public HitboxAttributes hitboxAttributes;
        }
        
        [Serializable]
        public class HitScanData {
            public Vector3Int origin;
            public AimData aimData;
            public HitScanProperties hitScanProperties;

            public HitScanData(Vector3Int origin, AimData aimData, HitScanProperties hitScanProperties) {
                this.origin = origin;
                this.aimData = aimData;
                this.hitScanProperties = hitScanProperties;
            }
        }
        
        public enum CameraMode {
            UserFree,
            UserLockOn,
            Cinematic
        }
        
        public enum LagType {
            None,
            Hitstun,
            Hitlag,
            Blockstun,
            LandingLag,
            Knockdown
        }

        public enum MechCharacterState {
            Standing,
            Crouching,
            Jumping,
            Running,
            Dashing,
            Tumble,
            Knockdown,
            Count,
        }

        public enum DirectionalInput {
            Neutral,
            Forward,
            Back,
            Left,
            Right,
            Count,
        }

        /// <summary>
        /// Used in cancel action into action system. Can cancel into any action equal to higher on the list as the type set in cancellable data.
        /// ie. if action is cancellable into Block, player can cancel into Block, Attack, Primary and Secondary
        /// </summary>
        public enum ActionTypes {
            Block,
            Jump,
            Dash,
            Melee,
            Primary,
            Secondary,
            Other
        }

        public enum HitResult {
            Whiff,
            Blocked,
            Hit
        }

        public enum GamePhase {
            Awaiting,
            CountDown,
            Game,
            GameEnd,
            SetFinish
        }
        
        public enum RoundEndReason {
            None,
            KnockOut,
            Time,
        }
    }
}