namespace MechFighter {
    public struct MechGameConstants {
        //short values = -32 768 ; 32 767 (ushort max val = 65 535)
        //int values = -2 147 483 648 ; 2 147 483 647 (uint max val = 4 294 967 295)
        //long values =  -9 223 372 036 854 775 808 ; 9 223 372 036 854 775 807 (ulong max val = 18 446 744 073 709 551 615)

        //public const int MAX_BULLETS = 30;

        public const byte MOVEMENT_BITS = 4;
        public const byte MOVEMENT_VALUES = 7;
        public const byte MOVEMENT_X_MASK = 0b0000_1111;
        public const byte MOVEMENT_Y_MASK = 0b1111_0000;
    
        public const byte INPUT_YAW = 0;
        public const byte INPUT_PITCH = 2;
        public const byte INPUT_MOVEMENT = 4;

        public const byte INPUT_FIRST_BUTTON = 40;
        public const byte INPUT_MAX = 63;

        public const ushort SCALE = 10000; //this equals 1 from now on
        public const byte ANGLE_SCALE = 182;
        public const float DELTA_TIME = 1f / 60f;  //WARNING!!! DON'T USE IN GAME STATE!!!
        public const short DIRECTIONAL_INPUT_THRESHOLD = SCALE / 10;
        
        public static readonly byte[] V_CAM_LAYER = {10, 11};
        
        public const int MAX_SPEED = SCALE * 4;

        public const byte INPUT_REPEAT_BUFFER_WINDOW = 6;

        public const ushort MECH_HALF_HEIGHT = 10000;
        
        public const ushort MAX_BLOCK_ANGLE_RANGE_EFFECT = 90 * ANGLE_SCALE;

        public const short RELOAD_METER_GAIN = 1000;

        public const ushort HIGH_PING_THRESHOLD = 200;
    }
}
