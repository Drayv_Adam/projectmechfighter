using System;
using Core.MechActions;
using Core.MechData;
using Core.MechMovesets.MechActions;
using GameViewAssets;
using GameViewAssets.Code;
using JetBrains.Annotations;
using UnityEngine;

namespace Core.Utility {
    public class ActionHitboxEditor : MonoBehaviour {
#if UNITY_EDITOR
        [SerializeField] MechView mechView;

        MechView previousMech;
        
        [SerializeField] MechActionObject mechActionObject;

        [Header("Timeline Navigation:")]
        [SerializeField] uint animationFrame;
        [SerializeField] bool jumpToPreviousEvent;
        [SerializeField] bool jumpToNextEvent;
        [UsedImplicitly, SerializeField] bool refresh;

        [Header("Readonly")] 
        [UsedImplicitly, SerializeField] int currentAnimationId;
        [UsedImplicitly, SerializeField] int currentEventIndex;
        [UsedImplicitly, SerializeField] int eventStartFrame;
        [UsedImplicitly, SerializeField] int eventCount;
        
        MechAction mechAction;
        MechActionEvent[] events;

        uint animationStartTime;

        #region properties

        Animator _animator;
        Animator MechAnimator {
            get {
                if (_animator == null && mechView != null)
                    _animator = mechView.GetComponent<Animator>();
                return _animator;
            }
        }
        MechAnimationManager _animationManager;
        MechAnimationManager AnimationManager {
            get {
                if (_animationManager == null && mechView != null)
                    _animationManager = mechView.GetComponent<MechAnimationManager>();
                return _animationManager;
            }
        }
        
        #endregion

        void Awake() {
            if (mechView != null)
                mechView.gameObject.SetActive(false);
        }

        void OnValidate() {
            if (mechView == null)
                return;
            SwapMechs();
            if (mechActionObject == null) {
                currentAnimationId = 10000;
                AnimationManager.PlayAnimation(currentAnimationId, 0);
                return;
            }
            
            InitializeFields();

            refresh = true;
            if (!jumpToNextEvent) 
                TryJumpNextEvent();
            if (!jumpToPreviousEvent)
                TryJumpToPreviousEvent();

            HandleMechAction();
        }
        
        void HandleMechAction() {
            MechAnimator.Rebind();
            SetCurrentAnimationId();
            AnimationManager.PlayAnimation(currentAnimationId, animationFrame - animationStartTime);
            MechAnimator.Update(0);
        }

        void SwapMechs() {
            if (previousMech == mechView)
                return;
            if (previousMech != null)
                previousMech.gameObject.SetActive(false);
            mechView.gameObject.SetActive(true);
            _animator = null;
            _animationManager = null;
            previousMech = mechView;
        }
        
        #region Navigation

        void InitializeFields() {
            mechAction = mechActionObject.GetMechAction();
            events = mechAction.actionEvents;
            eventCount = events.Length;
            currentEventIndex = GetLastEventIndex();
            animationStartTime = 0;
            if (currentEventIndex >= 0 && currentEventIndex < events.Length)
                eventStartFrame = (int) events[currentEventIndex].onFrame;
            else
                eventStartFrame = -1;
        }
        
        void TryJumpToPreviousEvent() {
            jumpToPreviousEvent = true;
            int lastEventIndex = GetLastEventIndex();
            if (lastEventIndex < 0) {
                animationFrame = 0;
                return;
            }
            if (events[lastEventIndex].onFrame == animationFrame)
                lastEventIndex--;
            animationFrame = lastEventIndex < 0 ? 0 : events[lastEventIndex].onFrame;
            InitializeFields();
        }

        void TryJumpNextEvent() {
            jumpToNextEvent = true;
            int nextEventIndex = GetLastEventIndex() + 1;
            animationFrame = nextEventIndex >= events.Length ? mechAction.endFrame : events[nextEventIndex].onFrame;
            InitializeFields();
        }

        void SetCurrentAnimationId() {
            currentAnimationId = mechAction.startAnimationId;
            animationStartTime = 0;
            for (int i = 0; i < events.Length; i++) {
                if (events[i].ignoreNormally)
                    continue;
                if (animationFrame >= events[i].onFrame && events[i].changeAnimation) {
                    currentAnimationId = events[i].animationId;
                    animationStartTime = events[i].onFrame;
                }
            }
        }
        
        int GetLastEventIndex() {
            int lastEventIndex = -1;
            for (int i = 0; i < events.Length; i++) {
                if (events[i].ignoreNormally)
                    continue;
                if (events[i].onFrame > animationFrame)
                    return lastEventIndex;
                lastEventIndex = i;
            }

            return lastEventIndex;
        }

        int GetLastEventWithHitboxChange() {
            int lastEventIndex = -1;
            for (int i = 0; i < events.Length; i++) {
                if (events[i].ignoreNormally)
                    continue;
                if (events[i].onFrame <= animationFrame && events[i].changeHitbox)
                    lastEventIndex = i;
            }

            return lastEventIndex;
        }
        
        #endregion
        
        void OnDrawGizmos() {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(Vector3.zero, 0.1f);
            if (events == null || events.Length == 0)
                return;
            // GetLastEventWithHitboxChanges
            var index = GetLastEventWithHitboxChange();
            if (index < 0 || index >= events.Length)
                return;

            if (events[index] == null)
                return;
            HitboxDrawerHelper.DrawHitboxData(events[index].hitboxData);
        }
#endif
    }
}
