using Core.MechMovesets.MechActions;
using UnityEditor;
using UnityEngine;

namespace Core.Utility {
#if UNITY_EDITOR
    public static class HitboxDrawerHelper {
        const int MAX_DAMAGE_COLOR = 1000;

        static readonly Color LowDamageColor = Color.yellow;
        static readonly Color HighDamageColor = Color.red;

        public static GUIStyle style = new GUIStyle {
            normal = {
                textColor = Color.white,
            },
            fontSize = 24
        };


        public static void DrawHitboxData(HitboxData data, Vector3Int? position = null, ushort heading = 0) {
            if (data.hitboxes == null)
                return;
            for (int i = 0; i < data.hitboxes.Length; i++) {
                Gizmos.color = GetColorBasedOnDamage(data.hitboxes[i].properties.damage);
                DrawSpheres(data.hitboxes[i].spheres, position, heading);
            }
        }

        static void DrawSpheres(Structs.Sphere[] spheres, Vector3Int? position = null, ushort heading = 0) {
            for (int i = 0; i < spheres.Length; i++) {
                DrawSphere(spheres[i], position, heading, i);
            }
        }

        public static void DrawSphere(Structs.Sphere sphere, Vector3Int? position = null, ushort heading = 0,
            int index = 0) {
            var centerOffset = sphere.center;
            position ??= Vector3Int.zero;
            if (sphere.center != Vector3Int.zero) {
                centerOffset = MechUtils.RotateVector3AroundY(centerOffset, heading);
            }

            var pos = MechUtils.IntToVector3(position.Value + centerOffset);
            Gizmos.DrawWireSphere(pos, MechUtils.IntToFloat(sphere.radius));
            Handles.Label(pos, index.ToString(), style);
        }

        static Color GetColorBasedOnDamage(int damage) {
            return Color.Lerp(LowDamageColor, HighDamageColor, (float) damage / MAX_DAMAGE_COLOR);
        }

        public static void DrawHitScanData(Structs.HitScanData hitScanData) {
            if (hitScanData == null)
                return;
            var pos1 = MechUtils.IntToVector3(hitScanData.origin);

            var direction = Quaternion.Euler(
                MechUtils.PitchToFloat(hitScanData.aimData.pitch),
                MechUtils.HeadingToFloat(hitScanData.aimData.Heading),
                0) * Vector3.forward * MechUtils.IntToFloat((int)hitScanData.hitScanProperties.hitScanRange);
            var pos2 = pos1 + direction;

            Gizmos.color = GetColorBasedOnDamage(hitScanData.hitScanProperties.hitProperties.damage);
            
            Gizmos.DrawLine(pos1, pos2);
        }
    }
#endif
}