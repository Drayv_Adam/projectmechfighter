using System.Collections;
using Core.Managers;
using Core.StageInfo;
using UnityEngine;

namespace Core.Utility {
    public class GameRunnerLoader : MonoBehaviour {
        [SerializeField] MechGameRunner runnerPrefab;
        [SerializeField] StageInfoObject stageInfo;

        Coroutine startGameCor;

        void Start() {
            if (MechGameRunner.Instance == null) {
                var instance = Instantiate(runnerPrefab);
                if (stageInfo != null) { //running from scene, not menu
                    TryStartGame(instance);
                    return; //dont destroy now, destroy when coroutine ends
                }
            }
            Destroy(gameObject);
        }

        void TryStartGame(MechGameRunner runnerInstance) {
            if (startGameCor != null)
                Debug.LogError("StartGameCor Is Already Running!");
            else
                startGameCor = StartCoroutine(StartGameWithoutSceneLoad(runnerInstance));
        }

        IEnumerator StartGameWithoutSceneLoad(MechGameRunner runnerInstance) {
            yield return null;
            runnerInstance.StartLocalGameWithoutLoadingScene(new[] {false, false}, stageInfo.stageInfo);
            HUDManager.Instance.HUDToggle(true);
            Destroy(gameObject);
        }
    }
}