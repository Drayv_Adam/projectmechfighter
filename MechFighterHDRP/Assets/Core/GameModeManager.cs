﻿using System;
using Core.Utility;
using UI;
using UnityEngine;

namespace Core {
    
    using static Structs;
    public class GameModeManager {
        const int PRE_COUNTDOWN = 90;
        public const int COUNTDOWN_DURATION = 60; //in frames
        public const int MATCH_DURATION = 99 * 60;
        public const int CLEANUP_DURATION = 180;
        public const int MAX_ROUNDS = 2; //don't change, seriously
        
        public int NextPhaseEndFrame { get; private set;}
        public static event Action OnCountDownStart;
        public static event Action OnRoundStart;
        public static event Action<RoundEndReason, int> OnRoundEnd;

        public static event Action OnAwaitStart;
        
        public GameModeManager() {
            HandleAwaitStart(0);
        }

        public GamePhase HandleGamePhase(Mech[] mechs, int frameNumber, GamePhase gamePhase) {
            switch (gamePhase) {
                case GamePhase.Game: {
                    var endReason = CheckFinishConditions(mechs, frameNumber, out int winnerIndex);
                    if (endReason == RoundEndReason.None) 
                        return GamePhase.Game;
                    OnRoundEnd?.Invoke(endReason, winnerIndex);
                    HandleEndGameStart(frameNumber);
                    return GamePhase.GameEnd;
                }
                case GamePhase.Awaiting:
                    if (!IsTimeElapsed(frameNumber)) break;
                    HandleCountDownStart(frameNumber);
                    return GamePhase.CountDown;
                case GamePhase.CountDown:
                    if (!IsTimeElapsed(frameNumber)) break;
                    HandleGameStart(frameNumber);
                    return GamePhase.Game;
                case GamePhase.GameEnd:
                    if (!IsTimeElapsed(frameNumber)) break;
                    if (CheckFinishSet(mechs))
                        return GamePhase.SetFinish;
                    HandleAwaitStart(frameNumber);
                    return GamePhase.Awaiting;
                case GamePhase.SetFinish:
                    MenuManager.OpenMenu(MenuType.Rematch);
                    NextPhaseEndFrame = 0; //wait for prompt
                    return GamePhase.SetFinish;
                default:
                    Debug.LogError($"Unhandled game phase {gamePhase}");
                    break;
            }
            return gamePhase;
        }
        
        bool CheckFinishSet(Mech[] mechs) {
            for (var i = 0; i < mechs.Length; i++) {
                if (mechs[i].score >= MAX_ROUNDS) {
                    Debug.Log("Score is above max, should finish");
                    return true;
                }
            }
            return false;
        }
        
        public RoundEndReason CheckFinishConditions(Mech[] mechs, int frameNumber, out int winnerIndex) {
            bool oneStanding = IsOneOrNoneStanding(mechs, out winnerIndex);
            bool time = IsTimeElapsed(frameNumber);
            
            if (oneStanding) {
                AnnouncerManager.Instance.DisplayAnnouncerText(AnnouncerManager.Announcement.Empty);
                return RoundEndReason.KnockOut;
            }

            if (time) {
                winnerIndex = FindMechIdWithMostHealth(mechs);
                AnnouncerManager.Instance.DisplayAnnouncerText(AnnouncerManager.Announcement.Timeout);
                return RoundEndReason.Time;
            }
            

            return RoundEndReason.None;
        }
        
        
        void HandleCountDownStart(int frameNumber) {
            NextPhaseEndFrame = frameNumber + COUNTDOWN_DURATION;
            
            OnCountDownStart?.Invoke();
        }

        void HandleEndGameStart(int frameNumber) {
                NextPhaseEndFrame = frameNumber + CLEANUP_DURATION;
        }

        void HandleGameStart(int frameNumber) {
            NextPhaseEndFrame = frameNumber + MATCH_DURATION;
            if (MenuManager.isPracticeModeSelected)
                NextPhaseEndFrame = Int32.MaxValue;
            OnRoundStart?.Invoke();
        }

        public void HandleAwaitStart(int frameNumber) {
            NextPhaseEndFrame = frameNumber + PRE_COUNTDOWN;
            OnAwaitStart?.Invoke();
        }

        static int FindMechIdWithMostHealth(Mech[] mechs) {
            var max = 0;
            var maxID = -1;
            for (int i = 0; i < mechs.Length; i++) {
                if (max < mechs[i].health) {
                    max = mechs[i].health;
                    maxID = i;
                }
            }
            if (mechs[0] != null && mechs[1] != null)
                if (mechs[0].health == mechs[1].health) {
                    maxID = 2;
                    return maxID;
                }

            mechs[maxID].score += 1;
            return maxID;
        }
        
        bool IsTimeElapsed(int frameNumber) {
            return NextPhaseEndFrame != -1 && frameNumber >= NextPhaseEndFrame;
        }
        
         bool IsOneOrNoneStanding(Mech[] mechs, out int winnerIndex) {
             winnerIndex = -1;
            var standing = 0;
            for (var i = 0; i < mechs.Length; i++) {
                if (mechs[i].health > 0) {
                    standing++;
                    winnerIndex = i;
                }
            }
            switch (standing) {
                case 0: winnerIndex = mechs.Length + 1; //DOUBLE / ALL K.O.
                    return true; //well it's false, but we still want to end round
                case 1:
                    for (var i = 0; i < mechs.Length; i++) {
                        if (mechs[i].health > 0) {
                            mechs[i].score += 1;
                        }
                    }
                    return true;
            }
            return false; //more than 1 alive
        }
    }
}