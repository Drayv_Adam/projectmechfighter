using System;
using System.Collections.Generic;
using Core.MechActions;
using Core.MechMovesets.MechActions;
using Core.Utility;
using JetBrains.Annotations;
using UnityEngine;

namespace Core.MechData {
    
    [CreateAssetMenu(fileName = "MechAction", menuName = "MechActions/Create MechAction Object", order = 1)]
    public class MechActionObject : ScriptableObject {
        [Tooltip("Use this only if this is an extended action, which doesn't require additional fields")]
        [SerializeField] MechActionExtensionType actionExtensionType;

        [SerializeField] protected string actionName;
        [SerializeField] protected int startAnimationId;
        [Tooltip("Set 0 for infinite")]
        [SerializeField] protected ushort endActionFrame = 50;
        [SerializeField] protected bool counterHitStateOnStart = true;
        [SerializeField] List<MechActionEventObject> events = new List<MechActionEventObject>();

        void OnValidate() { 
            actionName = name;
            for (var i = 0; i < events.Count; i++) {
                var actionEvent = events[i];
                actionEvent.note = events[i].GetAutoName(i);
                events[i] = actionEvent;
            }
        }

        public virtual MechAction GetMechAction() {
            var actionEvents = GetMechActionEvents();
            switch (actionExtensionType) {
                case MechActionExtensionType.DontCancelOnLand:
                    return new DontCancelOnLandAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
                case MechActionExtensionType.SwordSpecial:
                    return new SwordSecondaryAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
                case MechActionExtensionType.AirDash:
                    return new AirDashAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
                case MechActionExtensionType.DoubleJump:
                    return new DoubleJumpAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
                case MechActionExtensionType.Dash:
                    return new DashAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
                case MechActionExtensionType.Jump:
                    return new JumpAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
                default:
                    return new MechAction(actionName, startAnimationId, endActionFrame, counterHitStateOnStart, actionEvents);
            }
        }

        protected MechActionEvent[] GetMechActionEvents() {
            var mechActionEvents = new MechActionEvent[events.Count];
            for (int i = 0; i < events.Count; i++) {
                mechActionEvents[i] = events[i].GetMechActionEvent();
            }

            return mechActionEvents;
        }
        
        public enum MechActionExtensionType {
            None,
            Jump,
            Dash,
            DoubleJump,
            AirDash,
            SwordSpecial,
            DontCancelOnLand
        }
        
        [Serializable]
        struct MechActionEventObject {
            [Tooltip("The note is not saved to MechAction"), UsedImplicitly]
            public string note;
            [SerializeField] bool ignoreNormally;
            [SerializeField] uint onFrame;
            [SerializeField] MechAnimationEvent animation;
            [SerializeField] MovementEvent movement;
            [SerializeField] HitboxEvent hitboxes;
            [SerializeField] CharacterStateEvent characterState;
            [SerializeField] ActionCancelsEvent cancels;

            public MechActionEvent GetMechActionEvent() {
                if (hitboxes.hitboxData.hitboxes != null && hitboxes.hitboxData.hitboxes.Length == 0)
                    hitboxes.hitboxData.hitboxes = null;
                return new MechActionEvent() {
                    ignoreNormally = ignoreNormally,
                    onFrame = onFrame,
                    changeAnimation = animation.changeAnimation,
                    animationId = animation.changeAnimation? animation.animationId: 0,
                    changeMovement = movement.changeMovement,
                    movementData = movement.changeMovement? movement.movementData : default,
                    changeHitbox = hitboxes.changeHitboxes,
                    hitboxData = hitboxes.changeHitboxes? hitboxes.hitboxData : default,
                    changeCharacterState = characterState.changeCharacterState,
                    characterState = characterState.characterState,
                    disableGroundCheck = characterState.disableGroundCheck,
                    changeChState = characterState.changeCounterHitState,
                    counterHitState = characterState.counterHitState,
                    updateCancels = cancels.changeActionCancels,
                    actionCancels = cancels.changeActionCancels? cancels.actionCancels : default
                };
            }

            [Serializable]
            struct MechAnimationEvent {
                public bool changeAnimation;
                public int animationId;
            }

            [Serializable]
            struct MovementEvent {
                public bool changeMovement;
                public MovementData movementData;
            }

            [Serializable]
            struct HitboxEvent {
                public bool changeHitboxes;
                public HitboxData hitboxData;
            }

            [Serializable]
            struct CharacterStateEvent {
                public bool changeCharacterState;
                public Structs.MechCharacterState characterState;
                public bool changeCounterHitState;
                public bool counterHitState;
                [Tooltip("Disable ground check if you want to make sure the character remains considered grounded / airborne until you enable it again or action gets cancelled")]
                public bool disableGroundCheck;
            }

            public string GetAutoName(int index) {
                var original = note;
                if (!string.IsNullOrEmpty(note)) {
                    var parts = note.Split(' ');
                    original = parts.Length > 2? parts[2] : parts[0];
                }
                var flags = "";
                if (animation.changeAnimation)
                    flags += "A";
                if (movement.changeMovement)
                    flags += movement.movementData.localAcceleration != Vector3Int.zero ? "+M" : "-M";
                if (hitboxes.changeHitboxes)
                    flags += hitboxes.hitboxData.hitboxes == null || hitboxes.hitboxData.hitboxes.Length == 0? "-H" : "+H";
                if (characterState.changeCharacterState)
                    flags += "+S";
                if (characterState.disableGroundCheck)
                    flags += characterState.disableGroundCheck? "+G" : "-G";
                if (characterState.changeCounterHitState)
                    flags += characterState.counterHitState? "+Ch" : "-Ch";
                if (cancels.changeActionCancels)
                    flags += cancels.actionCancels.IsAny? "+Ca" : "-Ca";
                var frame = (onFrame < 10) ? $"0{onFrame}" : onFrame.ToString();
                note = $"{index}: {frame} {original} ({flags})";
                return note;
            }

            [Serializable]
            struct ActionCancelsEvent {
                public bool changeActionCancels;
                public ActionCancels actionCancels;
            }
        }
    }
}