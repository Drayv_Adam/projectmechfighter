using Core.MechComponents;
using Core.MechMovesets;
using UnityEngine;

namespace Core.MechData {
    [CreateAssetMenu(fileName = "MechDataMapper", menuName = "ScriptableObjects/Mappers/Create MechDataMapper", order = 1)]
    public class MechDataMapper : ScriptableObject {
        [SerializeField] MechDataObject[] mechDataObjects;
        [SerializeField] MechMovesetObject[] mechMovesets;

        MechParameters[] mechParameters;

        public MechParameters[] MechParameters {
            get {
                mechParameters = new MechParameters[mechDataObjects.Length];
                for (int i = 0; i < mechParameters.Length; i++)
                    mechParameters[i] = mechDataObjects[i].MechParameters;
                return mechParameters;
            }
        }
        
        //Accessed only once on game Start (get start game data)
        public MechMoveset[] MechMovesets {
            get {
                var movesets = new MechMoveset[mechMovesets.Length];
                for (int i = 0; i < movesets.Length; i++) {
                    movesets[i] = mechMovesets[i].BuildMoveset();
                }

                return movesets;
            }
        }
    }
}