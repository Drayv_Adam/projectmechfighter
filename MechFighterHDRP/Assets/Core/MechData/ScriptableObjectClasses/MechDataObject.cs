using Core.MechComponents;
using UnityEngine;

namespace Core.MechData {
    [CreateAssetMenu(fileName = "MechData", menuName = "ScriptableObjects/MechData/Create MechData Object", order = 1)]

    public class MechDataObject : ScriptableObject {
        [SerializeField] MechParameters mechParameters;

        public MechParameters MechParameters => mechParameters;
    }
}
