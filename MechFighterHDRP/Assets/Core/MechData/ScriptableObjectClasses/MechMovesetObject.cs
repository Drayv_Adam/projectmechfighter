using System;
using Core.MechActions;
using Core.MechMovesets;
using Core.Utility;
using UnityEngine;

namespace Core.MechData {

    using static Structs;
    
    [CreateAssetMenu(fileName = "MechMoveset", menuName = "ScriptableObjects/MechData/Create MechMoveset Object", order = 1)]
    public class MechMovesetObject : ScriptableObject {
        [SerializeField] MovementActionObjects movementActions;
        [SerializeField] AttackActionObjects standingAttacks;
        [SerializeField] AttackActionObjects crouchingAttacks;
        [SerializeField] AttackActionObjects jumpingAttacks;
        [SerializeField] AttackActionObjects runningAttacks;
        [SerializeField] SystemActions systemActions;
        [SerializeField] MechActionObject[] specialActions;
        
        static string movesetName;

        MechMoveset moveset;
        
        void Awake() {
            AssignDebugStrings();
        }

        void AssignDebugStrings() {
            movesetName = name;
        }
        
        //this runs only once when we load 
        public MechMoveset BuildMoveset() {
            int statesCount = (int) MechCharacterState.Running + 1;
            int buttonsCount = (int) AttackType.Count;
            int directionsCount = (int) DirectionalInput.Count;

            AssignDebugStrings();
            
            var movement = new MovementActions() {
                jumpAction = movementActions.JumpAction,
                doubleJumpAction = movementActions.DoubleJumpAction,
                dashAction = movementActions.DashAction,
                backDashAction = movementActions.BackDashAction,
                airDashAction = movementActions.AirDashAction
            };

            var attacks = new MechAction[statesCount, buttonsCount, directionsCount];

            AssignAttacksByStance(ref attacks, MechCharacterState.Standing);
            AssignAttacksByStance(ref attacks, MechCharacterState.Crouching);
            AssignAttacksByStance(ref attacks, MechCharacterState.Jumping);
            AssignAttacksByStance(ref attacks, MechCharacterState.Running);
            
            if (standingAttacks.meleeAttacks.Neutral == null) 
                Debug.LogWarning($"Neutral standing Melee is null on {name}");

            if (standingAttacks.meleeAttacks.Neutral == null) 
                Debug.LogWarning($"Neutral standing Primary is null on {name}");
            
            if (standingAttacks.meleeAttacks.Neutral == null)
                Debug.LogWarning($"Neutral standing Secondary is null on {name}");

            moveset = new MechMoveset(movement, attacks, systemActions.MechActions, GetSpecialActions());
            
            var count = 0;
            for (int x = 0; x < statesCount; x++) {
                for (int y = 0; y < buttonsCount; y++) {
                    for (int z = 0; z < directionsCount; z++) {
                        if (moveset.attacks[x, y, z] != null)
                            count++;
                    }
                }
            }
            Debug.Log($"Assigned {count} attacks");
            return moveset;
            
        }

        MechAction[] GetSpecialActions() {
            var actions = new MechAction[specialActions.Length];
            for (int i = 0; i < specialActions.Length; i++) {
                if (specialActions[i] != null)
                    actions[i] = specialActions[i].GetMechAction();
                else {
                    Debug.LogWarning($"Null action in special actions at index {i} on {name}");
                }
            }
            return actions;
        }
        
        void AssignAttacksByStance(ref MechAction[,,] attacks, MechCharacterState state) {
            int stance = (int) state;
            var source = GetSource(state);
            AssignAttacksByButton(source, attacks, AttackType.Melee, stance);
            AssignAttacksByButton(source, attacks, AttackType.Secondary, stance);
            AssignAttacksByButton(source, attacks, AttackType.Primary, stance);
            attacks[stance, (int) AttackType.Grab, 0] = source.Grab;
        }

        void AssignAttacksByButton(AttackActionObjects source, MechAction[,,] attacks, AttackType attackType, int stance) {
            var button = GetActionsByDirection(source, attackType);
            var type = (int)attackType;
            
            const int n = (int) DirectionalInput.Neutral;
            const int f = (int) DirectionalInput.Forward;
            const int b = (int) DirectionalInput.Back;
            const int l = (int) DirectionalInput.Left;
            const int r = (int) DirectionalInput.Right;
            
            attacks[stance, type, n] = button.Neutral;
            attacks[stance, type, f] = button.Forward;
            attacks[stance, type, b] = button.Back;
            attacks[stance, type, l] = button.Left;
            attacks[stance, type, r] = button.Right;
        }

        ActionsByDirection GetActionsByDirection(AttackActionObjects source, AttackType attackType) {
            switch (attackType) {
                case AttackType.Melee:
                    return source.meleeAttacks;
                case AttackType.Primary:
                    return source.primaryAttacks;
                case AttackType.Secondary:
                    return source.secondaryAttacks;
                default:
                    throw new ArgumentOutOfRangeException(nameof(attackType), attackType, null);
            }
        }
        
        AttackActionObjects GetSource(MechCharacterState state) {
            switch (state) {
                case MechCharacterState.Standing:
                    return standingAttacks;
                case MechCharacterState.Crouching:
                    return crouchingAttacks;
                case MechCharacterState.Jumping:
                    return jumpingAttacks;
                case MechCharacterState.Running:
                    return runningAttacks;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
        
        [Serializable]
        struct MovementActionObjects {
            public MechActionObject jumpAction;
            public MechActionObject doubleJumpAction;
            public MechActionObject dashAction;
            public MechActionObject backDashAction;
            public MechActionObject airDashAction;

            public MechAction JumpAction {
                get {
                    if (jumpAction != null) 
                        return jumpAction.GetMechAction();
                    Debug.LogWarning($"Jump Action is null on {movesetName}");
                    return null;
                }
            }
            
            public MechAction DoubleJumpAction {
                get {
                    if (doubleJumpAction != null) 
                        return doubleJumpAction.GetMechAction();
                    Debug.LogWarning($"doubleJumpAction is null on {movesetName}");
                    return null;
                }
            }
            public MechAction DashAction {
                get {
                    if (dashAction != null) 
                        return dashAction.GetMechAction();
                    Debug.LogWarning($"Dash Action is null on {movesetName}");
                    return null;
                }
            }
            public MechAction BackDashAction {
                get {
                    if (backDashAction != null) 
                        return backDashAction.GetMechAction();
                    Debug.LogWarning($"backDashAction is null on {movesetName}");
                    return null;
                }
            }
            public MechAction AirDashAction {
                get {
                    if (airDashAction != null) 
                        return airDashAction.GetMechAction();
                    Debug.LogWarning($"airDashAction is null on {movesetName}");
                    return null;
                }
            }
        }

        [Serializable]
        struct SystemActions {
            public MechActionObject normalLanding;
            public MechActionObject landingOnBack;
            public MechActionObject getUp;
            public MechActionObject[] getHitLight;
            public MechActionObject getHitOverhead;
            public MechActionObject getHitStanding;
            public MechActionObject getHitAir;
            public MechActionObject knockDown;
            public MechActionObject deathStanding;
            public MechMovesets.SystemActions MechActions => new MechMovesets.SystemActions {
                normalLanding = normalLanding != null? normalLanding.GetMechAction() : null,
                deathStanding = deathStanding != null? deathStanding.GetMechAction() : null,
                getHitStanding = getHitStanding != null? getHitStanding.GetMechAction() : null,
                getHitAir = getHitAir != null? getHitAir.GetMechAction() : null,
                landingOnBack = landingOnBack != null? landingOnBack.GetMechAction() : null,
                getUp = getUp != null? getUp.GetMechAction() : null,
                getHitLight = getHitLight != null? GetHitLights() : null,
                getHitOverhead = getHitOverhead != null? getHitOverhead.GetMechAction() : null,
                knockDown = knockDown != null? knockDown.GetMechAction() : null,
            };

            public MechAction[] GetHitLights() {
                var tab = new MechAction[getHitLight.Length];
                for (int i = 0; i < getHitLight.Length; i++) {
                    tab[i] = getHitLight[i].GetMechAction();
                }

                return tab;
            }
        }
        
        [Serializable]
        struct AttackActionObjects {
            public ActionsByDirection meleeAttacks;
            public ActionsByDirection primaryAttacks;
            public ActionsByDirection secondaryAttacks;
            public MechActionObject grab;
            
            public MechAction Grab => grab != null? grab.GetMechAction() : null;

        }

        [Serializable]
        struct RunningAttacks {
            [SerializeField] MechActionObject melee;
            [SerializeField] MechActionObject primary;
            [SerializeField] MechActionObject secondary;
            
            public MechAction Melee => melee != null? melee.GetMechAction() : null;
            public MechAction Primary => primary != null? melee.GetMechAction() : null;
            public MechAction Secondary => secondary != null? melee.GetMechAction() : null;

        }
        
        [Serializable]
        struct ActionsByDirection {
            [SerializeField] MechActionObject neutral;
            [SerializeField] MechActionObject forward;
            [SerializeField] MechActionObject back;
            [SerializeField] bool areBothDirectionsTheSame;
            [SerializeField] MechActionObject left;
            [SerializeField] MechActionObject right;
            [SerializeField] MechActionObject grab;
            
            public MechAction Neutral => neutral != null? neutral.GetMechAction() : null;
            public MechAction Forward => forward != null? forward.GetMechAction() : null;
            public MechAction Back => back != null? back.GetMechAction() : null;
            public MechAction Left => left != null
                ? left.GetMechAction()
                : (areBothDirectionsTheSame ? (right != null ? right.GetMechAction() : null) : null);
            public MechAction Right => right != null
                ? right.GetMechAction()
                : (areBothDirectionsTheSame ? (left != null ? left.GetMechAction() : null) : null);            
        }
    }
}