using Core.MechActions;
using Core.Utility;
using UnityEngine;

namespace Core.MechData {
    [CreateAssetMenu(fileName = "GunAimStanding", menuName = "MechActions/CustomActions/GunAimStanding", order = 1)]
    //Example File for actions that require additional fields over what standard action object offers
    public class GunAimStandingObject : MechActionObject {
        //just copy fields from your extended Action here and replace "readonly" with "[SerializeField]" and then add then in the constructor
        [SerializeField] Structs.StanceParams stanceParams;
        [SerializeField] Structs.HitScanProperties[] hitScanProperties;
        [SerializeField] ushort specialMeterCost;

        public override MechAction GetMechAction() {
            return new GunAimStanding(actionName,
                startAnimationId,
                endActionFrame,
                counterHitStateOnStart,
                GetMechActionEvents(),
                specialMeterCost, stanceParams, hitScanProperties
            );
            //remember to return the extended action class TYPE, or it won't get saved in mech moveset!
        }
    }
}