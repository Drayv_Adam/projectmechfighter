using UI.InGame.HUD;
using UnityEngine;

namespace Core.Managers {
    public class HUDManager : MonoBehaviour {
        public static HUDManager Instance { get; private set; }
        [SerializeField] MainHUD HUD;
        [SerializeField] LockOnTarget lockOnTarget;
        [SerializeField] Transform HUDParent;
        static MainHUD spawnedHUD;

        void Awake() {
            Instance = this;
            if (!spawnedHUD)
                LoadHUD();
        }
        
        void LoadHUD() {
            spawnedHUD = Instantiate(HUD, HUDParent);
            spawnedHUD.transform.localPosition = Vector3.zero;
            DontDestroyOnLoad(spawnedHUD);
            HUDToggle(false);
        }

        public void HUDToggle(bool enable) {
            switch (enable) {
                case true:
                    EnableHUD();
                    break;
                case false:
                    DisableHUD();
                    break;
            }
        }

        public void ResetScore() {
            spawnedHUD.ResetScore();
        }

        void EnableHUD() {
            spawnedHUD.gameObject.SetActive(true);
        }

        void DisableHUD() {
            spawnedHUD.gameObject.SetActive(false);
        }

        public void SetBlackOut() {
            spawnedHUD.SetBlackOut();
        }
        

        public void SetPlayerThumbnail(int id, int image) {
            spawnedHUD.SetThumbnail(id, image);
        }


    }
}
