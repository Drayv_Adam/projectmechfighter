﻿using System;
using System.Collections.Generic;
using Core;
using Core.Input;
using Core.Utility;
using UnityEngine;

namespace UI.Popups {
    public class PopupManager : MonoBehaviour {
        const float DEFAULT_POPUP_DURATION = 4;

        [SerializeField] List<PopupContainer> popupContainers;
        Dictionary<PopupPosition, PopupContainer> positionToContainer;
        
        static PopupManager instance;
        bool pressed;

        Popup latestPopup;
        
        public void Awake() {
            instance = this;
            if (popupContainers.Count == 0) {
                throw new Exception("No popup containers setup!");
            }
            positionToContainer = new Dictionary<PopupPosition, PopupContainer>();
            for (var i = 0; i < popupContainers.Count; i++) {
                var container = popupContainers[i];
                container.activePopups = new Queue<Popup>();
                container.inactivePopups = new Queue<Popup>();
                positionToContainer.Add(container.position, container);
                popupContainers[i] = container;
            }
            
        }

        public static void ShowPopup(string text, PopupType type = PopupType.Info) {
            ShowPopup(new PopupParams(text, type));
        }

        public static void ShowError(string text) {
            ShowPopup(new PopupParams(text, PopupType.Error));
        }
        
        public static void ShowPopup(PopupParams popupParams) {
            if (MenuManager.Instance == null)
                return;
            EnsurePopupManagerSpawned();
            var container = instance.positionToContainer[popupParams.position];
            SetupPopup(popupParams, container);
#if UNITY_EDITOR
            LogPopup(popupParams);
#endif
        }

#if UNITY_EDITOR
        // ReSharper disable Unity.PerformanceAnalysis
        static void LogPopup(PopupParams popupParams) {
            switch (popupParams.type) {
                case PopupType.Info:
                    Debug.Log($"[Popup] {popupParams.text}");
                    break;
                case PopupType.Warning:
                    Debug.LogWarning($"[Warning Popup] {popupParams.text}");
                    break;
                case PopupType.Error:
                    Debug.LogError($"[Error Popup] {popupParams.text}");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
#endif
        
        static void SetupPopup(PopupParams popupParams, PopupContainer container) {
            var popup = container.inactivePopups.Count == 0
                ? Instantiate(MenuManager.MenuPrefabs.PopupPrefab, container.container)
                : container.inactivePopups.Dequeue();
            popup.Setup(popupParams);
            foreach (var previousPopup in container.activePopups) {
                previousPopup.PushUp();
            }
            container.activePopups.Enqueue(popup);
            instance.latestPopup = popup;
        }

        public static void HideLatestPopup() {
            //todo
            //if (instance.latestPopup != null)
                //MoveToInactive(instance.latestPopup);
            //instance.latestPopup = null;
        }
        
        public static void MoveToInactive(Popup popup) {
            var container = instance.positionToContainer[popup.position];
            if (container.activePopups.Count == 0)
                throw new Exception("Tried to hide a popup, but the active popups in this container is empty");
            if (container.activePopups.Peek() == popup) {
                container.MoveOldestToInactive();
                return;
            }
            FindAndRemove(popup, container);
        }

        static void FindAndRemove(Popup popup, PopupContainer container) {
            var newQueue = new Queue<Popup>();
            foreach (var activePopup in container.activePopups) {
                if (activePopup != popup)
                    newQueue.Enqueue(popup);
            }
            container.activePopups = newQueue;
            container.inactivePopups.Enqueue(popup);
        }

        static void EnsurePopupManagerSpawned() {
            if (instance == null)
                MenuManager.SpawnPopupManager();
        }
        
        public enum PopupType {
            Info,
            Warning,
            Error
        }

        public enum PopupPosition {
            UpperRightCorner,
            LowerLeftCorner
        }

        [Serializable]
        public struct PopupContainer {
            public Transform container;
            public PopupPosition position;
            public Queue<Popup> activePopups;
            public Queue<Popup> inactivePopups;
            
            public void MoveOldestToInactive() {
                if (activePopups.Count == 0)
                    return;
                var popup = activePopups.Dequeue();
                inactivePopups.Enqueue(popup);
            }
        }
        
        public struct PopupParams {
            public string text;
            public PopupType type;
            public PopupPosition position;
            public float duration;

            public PopupParams(string text, PopupType type = PopupType.Info,
                PopupPosition position = PopupPosition.LowerLeftCorner, float duration = DEFAULT_POPUP_DURATION) {
                this.text = text;
                this.type = type;
                this.position = position;
                this.duration = duration;
            }
        }
    }
}