
using System.Collections.Generic;
using Core.Connection;
using Core.Utility;
using Saving;
using UI.Popups;
using UnityEngine;

namespace Core.Managers {
    public class ProfileManager {
        public const int ONLINE_PROFILE_ID = -2;
        public const float DEFAULT_SENSITIVITY = 0.5f;
        public static ProfileManager Instance { get; private set; }

        public static PlayerLoadoutData OnlineLoadout => Instance.playerProfiles[ONLINE_PROFILE_ID].playerInfo.playerLoadoutData;

        public Dictionary<int, PlayerProfile> playerProfiles = new Dictionary<int, PlayerProfile>();

        public static PlayerSettings[] GetAllPlayerSettings() {
            if (Instance == null)
                Initialize();
            var settings = new PlayerSettings[Instance.playerProfiles.Count];
            int i = 0;
            foreach (var profile in Instance.playerProfiles) {
                settings[i++] = profile.Value.playerSettings;
            }
            return settings;
        }
        
        public static void Initialize() {
            if (Instance != null)
                return;
            Instance = new ProfileManager();
            Instance.LoadAll();
        }
        
        public void Save(int profileId, PlayerProfile profileData) {
            if (playerProfiles.ContainsKey(profileId))
                playerProfiles[profileId] = profileData;
            else
                playerProfiles.Add(profileId, profileData);
            PopupManager.ShowPopup("TODO: Save to file");
            //todo: save to JSON
        }

        public PlayerProfile Load(int profileId) {
            //implement loading
            if (playerProfiles.ContainsKey(profileId))
                return playerProfiles[profileId];
            return default;
        }

        public void UpdateOnlineLoadout(PlayerLoadoutData playerLoadoutData) {
            var profile = Load(ONLINE_PROFILE_ID);
            profile.playerInfo.playerLoadoutData = playerLoadoutData;
            Save(ONLINE_PROFILE_ID, profile);
        }

        public void LoadAll() {
            //todo: implement loading
            //temp code until we implement saving
            playerProfiles.Clear();
            for (int i = 0; i < 2; i++) {
                playerProfiles.Add(i, GetDefaultPlayerProfile);
            }
            playerProfiles.Add(ONLINE_PROFILE_ID, GetDefaultPlayerProfile);
        }

        static PlayerProfile GetDefaultPlayerProfile => new PlayerProfile() {
            playerInfo = new PlayerInfo() {
                playerName = $"Random Player {Random.Range(0, 10000)}",
            },
            playerSettings = new PlayerSettings() {
                lookSettings = new Structs.LookSettings(DEFAULT_SENSITIVITY)
            }
        };
    }
}