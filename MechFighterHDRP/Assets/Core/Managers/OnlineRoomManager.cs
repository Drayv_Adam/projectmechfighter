﻿using System.Collections.Generic;
using Connection;
using UI;
using UI.Popups;
using UI.RoomUI;

namespace Core.Connection {
    public class OnlineRoomManager {
        public const byte MIN_PLAYERS = 2;
        public static OnlineRoomManager Instance { get; private set; }
        
        Dictionary<int, PlayerJoinedData> players = new Dictionary<int, PlayerJoinedData>();
        PlayerSlot[] playerSlots = new PlayerSlot[MIN_PLAYERS];

        public RoomInfo RoomInfo { get; private set; }
        OnlineRoomPanel roomView;
        
        PlayerJoinedData myPlayerJoinedData;
        byte mySlot = NetworkMessage.SPECTATOR_SLOT_ID;
        
        public bool IsReady { get; private set; }
        public int PlayerCount => players.Count;

        public OnlineRoomManager(RoomInfo roomInfoData) {
            Instance = this;
            GetOnlineRoomInstance();
            RoomInfo = roomInfoData;
        }

        public void RoomJoined(PlayerJoinedData playerJoinedData) {
            InitializeRoomView();
            myPlayerJoinedData = playerJoinedData;
            OnUpdateHost(RoomInfo.hostConnId == myPlayerJoinedData.connectionId);
        }

        public void SetPlayerList(List<PlayerJoinedData> playersData) {
            if (playersData == null || playersData.Count == 0) {
                PopupManager.ShowPopup("Error loading player data to list");
                return;
            }
            players.Clear();
            roomView.ClearPlayerList();
            for (int i = 0; i < playersData.Count; i++) {
                AddPlayer(playersData[i]);
            }
        }

        public void AddPlayer(PlayerJoinedData playerData) {
            if (players.ContainsKey(playerData.connectionId)) {
                PopupManager.ShowError($"Error adding player. Key already exists");
                return;
            }
            players.Add(playerData.connectionId, playerData);
            if (!IsSpectator(playerData.playerSlot))
                UpdatePlayerSlot(playerData.connectionId, playerData.playerSlot);
            roomView.AddPlayer(playerData.connectionId, GetDisplayData(playerData));
        }
        
        public void TrySetReady() {
            IsReady = !IsReady;
            ServerConnectionManager.Instance.OnBecomeReady(IsReady);
        }

        public void TryTakeSlot(byte id) {
            ServerConnectionManager.Instance.SendSlotTaken(id);
            if (id >= playerSlots.Length) {
                PopupManager.ShowError("Error trying to take slot");
                return;
            }
            if (playerSlots[id].taken) {
                if (IsAnySlotFree(out id))
                    UpdatePlayerSlot(myPlayerJoinedData.connectionId, id);
                else
                    PopupManager.ShowError("Slot already taken");
            }
        }

        public void UpdatePlayerSlot(int connectionId, byte slotId) {
            bool isMe = connectionId == myPlayerJoinedData.connectionId;
            if (!players.ContainsKey(connectionId)) {
                PopupManager.ShowError("connectionId does not exist in room");
                return;
            }

            if (players[connectionId].playerSlot < RoomInfo.maxPlayers) {
                playerSlots[players[connectionId].playerSlot].taken = false;
                roomView.LeaveSlot(players[connectionId].playerSlot);
            }

            players[connectionId].playerSlot = slotId;
            if (slotId < playerSlots.Length) {
                playerSlots[slotId].taken = true;
                playerSlots[slotId].playerJoinedData = players[connectionId];
                roomView.EnterOrUpdateSlot(slotId, players[connectionId], isMe);
            }
            
            roomView.UpdatePlayerInList(connectionId, GetDisplayData(players[connectionId]));
        }
        
        
        bool IsAnySlotFree(out byte i) {
            for (i = 0; i < playerSlots.Length; i++) {
                if (!playerSlots[i].taken)
                    return true;
            }
            return false;
        }
        
        public void AutoEnterSlot(int connectionId) {
            if (IsAnySlotFree(out byte freeSlot)) {
                UpdatePlayerSlot(connectionId, freeSlot);
            }
        }

        public void UpdateReady(int connectionId, bool isReady) {
            if (!players.ContainsKey(connectionId)) {
                PopupManager.ShowError("connectionId does not exist in room");
                return;
            }
            bool isMe = connectionId == myPlayerJoinedData.connectionId;
            var slot = players[connectionId].playerSlot;
            if (slot < playerSlots.Length) {
                playerSlots[slot].playerJoinedData.isReady = isReady;
                roomView.EnterOrUpdateSlot(slot, playerSlots[slot].playerJoinedData, isMe);
            }
        }
        
        public void EnterSpectate() {
            ServerConnectionManager.Instance.SendSlotTaken(NetworkMessage.SPECTATOR_SLOT_ID);
            if (!IsSpectator(mySlot)) {
                EmptySlot(mySlot);
                mySlot = NetworkMessage.SPECTATOR_SLOT_ID;
            }
        }

        void EmptySlot(int slotId) {
            if (slotId >= playerSlots.Length) {
                PopupManager.ShowError("Error leaving slot");
                return;
            }
            roomView.LeaveSlot(slotId);
            playerSlots[slotId].taken = false;
            playerSlots[slotId].playerJoinedData = default;
            
        }
        
        public void RoomLeft() {
            InitializeRoomView();
        }
        
        void InitializeRoomView() {
            roomView.Initialize(RoomInfo.roomCapacity);
        }

        void GetOnlineRoomInstance() {
            var menu = MenuManager.GetMenu(MenuType.OnlineRoom);
            if (menu is OnlineRoomPanel onlineRoomPanel)
                roomView = onlineRoomPanel;
            else 
                PopupManager.ShowPopup("FAILED TO GET ONLINE ROOM REFERENCE", PopupManager.PopupType.Error);
        }

        public void UpdateRoomInfo(RoomInfo roomInfoData) {
            RoomInfo = roomInfoData;
            OnUpdateHost(RoomInfo.hostConnId == myPlayerJoinedData.connectionId);
        }
        
        public void RemovePlayer(int connectionID) {
            for (int i = 0; i < playerSlots.Length; i++) {
                if (playerSlots[i].taken && playerSlots[i].playerJoinedData.connectionId == connectionID) {
                    playerSlots[i].taken = false;
                    EmptySlot(i);
                }
            }

            if (players.ContainsKey(connectionID)) {
                players.Remove(connectionID);
                roomView.RemovePlayer(connectionID);
            }
        }

        void OnUpdateHost(bool isHost) {
            roomView.UpdateHost(isHost);
        }
        
        void UpdateActivePlayers() {
            playerSlots = new PlayerSlot[MIN_PLAYERS];
            int activePlayerCount = 0;
            for (var i = 0; i < players.Count; i++) {
                if (activePlayerCount >= playerSlots.Length) {
                    PopupManager.ShowError("Too many active players!");
                    return;
                }
                if (IsSpectator(players[i].playerSlot))
                    continue;
                playerSlots[activePlayerCount].taken = true;
                playerSlots[activePlayerCount++].playerJoinedData = players[i];
            }
            //todo: update roomview
        }

        public static bool IsSpectator(byte playerSlot) {
            return playerSlot > Instance.RoomInfo.maxPlayers;
        }

        public static PlayerDisplayData GetDisplayData(PlayerJoinedData playerData) {
            return new PlayerDisplayData(playerData.playerName,
                IsSpectator(playerData.playerSlot),
                playerData.connectionId == Instance.RoomInfo.hostConnId);
        }
        
        struct PlayerSlot {
            public bool taken;
            public PlayerJoinedData playerJoinedData;
        }
    }
}