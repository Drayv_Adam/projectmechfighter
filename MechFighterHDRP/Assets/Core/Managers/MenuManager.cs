using System;
using System.Collections;
using System.Collections.Generic;
using Audio;
using Core;
using Core.Input;
using Core.Managers;
using Rewired;
using Rewired.Integration.UnityUI;
using SharedGame;
using UI.Menu;
using UI.OnlineMenu;
using UI.PauseMenu;
using UI.Popups;
using UI.RoomUI;
using UI.Shared;
using UI.Visuals;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI {
    public class MenuManager : MonoBehaviour {
        static MenuManager instance;
        
        public static MenuManager Instance => instance;
        public static event Action OnLoadingCancelledByPlayer;

        [SerializeField] ColorTheme colorTheme;
        [SerializeField] MenuPrefabs menuPrefabs;
        [SerializeField] InputManager rewiredInputManager;
        [SerializeField] Transform contentCanvas;
        [SerializeField] Transform inGameContentCanvas;
        [SerializeField] Transform sharedContentCanvas;
        [SerializeField] Transform mainCanvas;

        static Stack<IMechGameMenu> openedMenus;

        ControlMapperPanel controlMapperPanel;
        ProfileScreen profilePanel;
        MainMenuPanel mainMenuPanel;
        OnlineModePanel onlineModePanel;
        LocalModePanel localPlayPanel;
        LocalVersusPanel localVersusPanel;
        SettingsPanel settingsPanel;
        RewiredEventSystem rewiredEventSystem;
        OnlineRoomPanel onlineRoom;
        LoadingScreen loadingScreen;
        BackgroundChanger backgroundChanger;
        PauseMenuPanel pauseMenuPanel;
        RematchMenuPanel rematchMenuPanel;
        CreditsMenuPanel creditsMenuPanel;
        SoundSettings soundSettingsPanel;

        public static int[] playerVictoriesValue;
        public static int lastWinner;
        public static bool isPracticeModeSelected;
        public static MenuPrefabs MenuPrefabs => instance.menuPrefabs;
        public static readonly WaitForSeconds LoadingScreenDelay = new WaitForSeconds(1f);
        GameManager GameManager => GameManager.Instance;

        bool pauseButtonHeld;
        static Coroutine loadSceneCor;

        public static MenuType CurrentOpenMenuType => openedMenus.Count > 0? openedMenus.Peek().GetMenuType() : MenuType.None;
        
        bool IsGameUnPaused => MechGameRunner.Instance != null &&
                               MechGameRunner.Instance.IsRunning &&
                               !MechGame.IsGamePaused;

        void Awake() {
            playerVictoriesValue = new int[2];
            ColorTheme.instance = colorTheme;
            if (mainCanvas != null)
                DontDestroyOnLoad(mainCanvas);
        }

        void Start() {
            instance = this;
            DontDestroyOnLoad(this);
            CollectObjectsFromScene();
            openedMenus = new Stack<IMechGameMenu>();
            OpenMainMenu();
            AudioManager.PlayTheme(AudioManager.Theme.Menu);
        }

        void Update() {
            HandlePauseAction();
        }

        void OnDestroy() {
            if (controlMapperPanel != null)
                controlMapperPanel.ControlMapper.onScreenClosed -= OnInputMapperClosed;
        }

        #region Static

        public static void ClearContent() {
            if (instance == null)
                return;
            while (openedMenus.Count > 0) {
                openedMenus.Pop().Hide();
            }
        }

        public static void SpawnPopupManager() {
            if (instance == null)
                return;
            Instantiate(instance.menuPrefabs.PopupManagerPrefab, instance.mainCanvas.parent);
        }

        public static void FallbackToMainMenu() {
            if (instance == null)
                return;
            ClearContent();
            instance.OpenMainMenu();
        }

        public static void ReturnToMainMenu() {
            if (loadSceneCor != null)
                instance.StopCoroutine(loadSceneCor);
            loadSceneCor = instance.StartCoroutine(LoadSceneCoroutine(0));
            HUDManager.Instance.HUDToggle(false);
            AudioManager.PlayTheme(AudioManager.Theme.Menu);
            instance.GameManager.Shutdown();
        }

        public static void ResumeGame() {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            MechGame.IsGamePaused = false;
        }
        
        public static void OpenMenu(MenuType menuType) {
            switch (menuType) {
                case MenuType.None:
                    return;
                case MenuType.MainMenu:
                    instance.OpenMainMenu();
                    break;
                case MenuType.LocalMode:
                    instance.OpenLocalModeMenu();
                    break;
                case MenuType.OnlineMode:
                    instance.OpenOnlineModePanel();
                    break;
                case MenuType.Profile:
                    instance.OpenProfileScreen();
                    break;
                case MenuType.ControlMapper:
                    instance.OpenControlMapper();
                    break;
                case MenuType.Settings:
                    instance.OpenSettingsPanel();
                    break;
                case MenuType.OnlineRoom:
                    instance.OpenOnlineRoom();
                    break;
                case MenuType.LoadingScreen:
                    // we want this empty
                    break;
                case MenuType.LocalVersus:
                    instance.OpenLocalVersus();
                    break;
                case MenuType.Pause:
                    instance.OpenPauseMenuPanel();
                    break;
                case MenuType.Rematch:
                    instance.OpenRematchMenuPanel();
                    break;
                case MenuType.Credits:
                    instance.OpenCreditsMenuPanel();
                    break;
                case MenuType.Sound:
                    instance.OpenSoundSettingsPanel();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(menuType), menuType, null);
            }
        }

        public static void SetupLoadingScreen(MenuType targetMenu = MenuType.None,
            string message = LoadingScreen.DEFAULT_HEADLINE, bool allowCancel = false) {
            instance.ShowLoadingScreen(targetMenu, message, allowCancel);
        }

        public static void OnLoadingFinished(MenuType targetMenu = MenuType.None, bool isReturning = false) {
            instance.HideLoadingScreen();
            if (targetMenu == MenuType.None) {
                SetInGameContentActive();
                return;
            }

            if (isReturning)
                openedMenus.Peek().Open();
            else
                OpenMenu(targetMenu);
        }

        public static void SetInGameContentActive() {
            if (instance != null && instance.contentCanvas != null)
                instance.contentCanvas.gameObject.SetActive(false);
            if (instance != null && instance.inGameContentCanvas != null)
                instance.inGameContentCanvas.gameObject.SetActive(true);
        }

        public static void CancelLoadingScreen() {
            instance.HideLoadingScreen();
            openedMenus.Peek().Open();
            OnLoadingCancelledByPlayer?.Invoke();
        }

        public static void CloseCurrentMenu() {
            if (openedMenus.Count == 0) return;
            var openMenu = openedMenus.Pop();
            openMenu.Hide();
            if (openMenu.GetMenuType() == MenuType.ControlMapper)
                OnInputMapperClosed();
            if (openedMenus.Count == 0) {
                Debug.LogError("Are you sure you closed the right number of windows?");
                return;
            }

            openedMenus.Peek().Open();
        }

        public static IMechGameMenu GetMenu(MenuType menuType) {
            switch (menuType) {
                case MenuType.MainMenu:
                    instance.EnsureMainMenuPanelSpawned();
                    return instance.mainMenuPanel;
                case MenuType.LocalMode:
                    instance.EnsureLocalModeMenuSpawned();
                    return instance.localPlayPanel;
                case MenuType.OnlineMode:
                    instance.EnsureOnlineModePanelSpawned();
                    return instance.onlineModePanel;
                case MenuType.Profile:
                    instance.EnsureProfilePanelSpawned();
                    return instance.profilePanel;
                case MenuType.ControlMapper:
                    instance.EnsureControlMapperSpawned();
                    return instance.controlMapperPanel;
                case MenuType.Settings:
                    instance.EnsureSettingsPanelSpawned();
                    return instance.settingsPanel;
                case MenuType.OnlineRoom:
                    instance.EnsureOnlineRoomSpawned();
                    return instance.onlineRoom;
                case MenuType.Pause:
                    instance.EnsurePauseMenuSpawned();
                    return instance.pauseMenuPanel;
                case MenuType.Rematch:
                    instance.EnsureRematchMenuSpawned();
                    return instance.rematchMenuPanel;
                default:
                    throw new ArgumentOutOfRangeException(nameof(menuType), menuType, null);
            }
        }

        public static void Quit() {
            //todo: show "Are you sure" prompt
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        #endregion

        #region OpenMenu

        void OpenMenu(IMechGameMenu menu) {
            if (openedMenus.Count > 0)
                openedMenus.Peek().Hide();
            menu.Open();
            openedMenus.Push(menu);
        }

        public void OpenLocalModeMenu() {
            EnsureLocalModeMenuSpawned();
            OpenMenu(localPlayPanel);
        }

        public void OpenLocalVersus() {
            EnsureLocalVersusPanelSpawned();
            OpenMenu(localVersusPanel);
        }


        public void OpenMainMenu() {
            EnsureMainMenuPanelSpawned();
            OpenMenu(mainMenuPanel);
        }

        public void OpenOnlineModePanel() {
            EnsureOnlineModePanelSpawned();
            OpenMenu(onlineModePanel);
        }

        public void OpenOnlineRoom() {
            EnsureOnlineRoomSpawned();
            OpenMenu(onlineRoom);
        }

        public void OpenSettingsPanel() {
            EnsureSettingsPanelSpawned();
            OpenMenu(settingsPanel);
        }

        public void OpenProfileScreen() {
            EnsureProfilePanelSpawned();
            OpenMenu(profilePanel);
        }

        public void OpenControlMapper() {
            EnsureControlMapperSpawned();
            OpenMenu(controlMapperPanel);
            controlMapperPanel.ControlMapper.onScreenClosed += OnInputMapperClosed;
        }

        public void OpenPauseMenuPanel() {
            EnsurePauseMenuSpawned();
            instance.inGameContentCanvas.gameObject.SetActive(true);
            OpenMenu(pauseMenuPanel);
        }
        
        public void OpenRematchMenuPanel() {
            EnsureRematchMenuSpawned();
            instance.inGameContentCanvas.gameObject.SetActive(true);
            OpenMenu(rematchMenuPanel);
        }
        
        public void OpenCreditsMenuPanel() {
            EnsureCreditsMenuPanelSpawned();
            OpenMenu(creditsMenuPanel);
        }


        public void OpenSoundSettingsPanel() {
            EnsureSoundSettingsPanelSpawned();
            OpenMenu(soundSettingsPanel);
        }
        
        #endregion

        #region RewiredEssentials

        void EnsureRewiredInputManagerSpawned() {
            if (rewiredInputManager == null && !TryFindRewiredInputManager())
                rewiredInputManager = Instantiate(menuPrefabs.RewiredInputManager);
        }

        bool TryFindRewiredInputManager() {
            rewiredInputManager = FindObjectOfType<InputManager>();
            return rewiredInputManager != null;
        }

        void EnsureRewiredInputSystemSpawned() {
            if (rewiredEventSystem == null && !TryFindRewiredInputSystem())
                rewiredEventSystem = Instantiate(menuPrefabs.GetRewiredEventSystem, contentCanvas);
        }

        bool TryFindRewiredInputSystem() {
            rewiredEventSystem = FindObjectOfType<RewiredEventSystem>();
            return rewiredEventSystem != null;
        }

        #endregion

        #region private Methods

        static IEnumerator LoadSceneCoroutine(int sceneID) {
            MechGameRunner.Instance.Shutdown();
            instance.inGameContentCanvas.gameObject.SetActive(false);
            instance.contentCanvas.gameObject.SetActive(true);
            SetupLoadingScreen();
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneID);
            while (!asyncLoad.isDone)
                yield return null;
            yield return LoadingScreenDelay;

            OnLoadingFinished(CurrentOpenMenuType, true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void ShowLoadingScreen(MenuType targetMenu, string message, bool allowCancel) {
            if (loadingScreen == null)
                loadingScreen = Instantiate(menuPrefabs.LoadingScreen, sharedContentCanvas);
            if (openedMenus.Count > 0)
                openedMenus.Peek().Hide();
            loadingScreen.Setup(targetMenu, message, allowCancel);
            loadingScreen.Open();
        }

        void HideLoadingScreen() {
            if (loadingScreen != null && loadingScreen.IsOpen)
                loadingScreen.Hide();
            else
                Debug.LogWarning("Tried to hide loading screen, but no loading screen is being shown");
        }
        
        void HandlePauseAction() {
            if (InputCollector.rawInput.Length == 0)
                return;
            var pauseButtonPressed = false;
            for (int i = 0; i < InputCollector.rawInput.Length; i++)
                pauseButtonPressed |= InputCollector.rawInput[i].menuButtons.pause;

            if (pauseButtonPressed && !pauseButtonHeld) 
                HandlePausePressed();

            pauseButtonHeld = pauseButtonPressed;
        }

        void HandlePausePressed() {
            if (!IsGameUnPaused) {
                if (CurrentOpenMenuType == MenuType.MainMenu) {
                    PopupManager.ShowPopup("If you want to quit, press the quit button");
                    return;
                }
                if (CurrentOpenMenuType == MenuType.Pause)
                    ResumeGame();
                CloseCurrentMenu();
            }
            else
                OpenMenu(MenuType.Pause);
        }
        
        void EnsureLocalModeMenuSpawned() {
            if (localPlayPanel == null)
                localPlayPanel = Instantiate(menuPrefabs.LocalModePanel, contentCanvas);
        }

        void EnsureLocalVersusPanelSpawned() {
            if (localVersusPanel == null)
                localVersusPanel = Instantiate(menuPrefabs.LocalVersusPanel, contentCanvas);
        }

        void EnsureMainMenuPanelSpawned() {
            if (mainMenuPanel == null)
                mainMenuPanel = Instantiate(menuPrefabs.MainMenuPanel, contentCanvas);
        }

        void EnsureOnlineModePanelSpawned() {
            if (onlineModePanel == null)
                onlineModePanel = Instantiate(menuPrefabs.OnlineMenuPanel, contentCanvas);
        }

        void EnsureOnlineRoomSpawned() {
            if (onlineRoom == null)
                onlineRoom = Instantiate(menuPrefabs.OnlineRoomPanel, contentCanvas);
        }

        void EnsureSettingsPanelSpawned() {
            if (settingsPanel == null)
                settingsPanel = Instantiate(menuPrefabs.SettingsPanel, contentCanvas);
        }

        void EnsureProfilePanelSpawned() {
            if (profilePanel == null)
                profilePanel = Instantiate(menuPrefabs.ProfileScreenPanel, contentCanvas);
        }

        void EnsurePauseMenuSpawned() {
            if (pauseMenuPanel == null)
                pauseMenuPanel = Instantiate(menuPrefabs.PauseMenuPanel, inGameContentCanvas);
        }
        
        void EnsureRematchMenuSpawned() {
            if (rematchMenuPanel == null) {
                rematchMenuPanel = Instantiate(menuPrefabs.RematchMenuPanel, inGameContentCanvas);
            }
        }
        
        void EnsureCreditsMenuPanelSpawned() {
            if (creditsMenuPanel == null) {
                creditsMenuPanel = Instantiate(menuPrefabs.CreditsMenuPanel, contentCanvas);
            }
        }
        
        void EnsureSoundSettingsPanelSpawned() {
            if (soundSettingsPanel == null) {
                soundSettingsPanel = Instantiate(menuPrefabs.SoundSettings, sharedContentCanvas);
            }
        }

        void EnsureControlMapperSpawned() {
            if (controlMapperPanel != null)
                return;
            controlMapperPanel = Instantiate(menuPrefabs.ControlMapperPanel);
            EnsureRewiredInputSystemSpawned();
            EnsureRewiredInputManagerSpawned();
            controlMapperPanel.ControlMapper.rewiredInputManager = rewiredInputManager;
        }

        void CollectObjectsFromScene() {
            var objects = FindObjectsOfType<GameObject>();
            foreach (var o in objects) {
                var menu = o.GetComponent<IMechGameMenu>();
                if (menu != null) {
                    CheckAndAddMenuObject(menu);
                    o.SetActive(false);
                    continue;
                }

                var rewiredES = o.GetComponent<RewiredEventSystem>();
                if (rewiredES != null) {
                    rewiredEventSystem = rewiredES;
                    continue;
                }

                var rewiredIM = o.GetComponent<InputManager>();
                if (rewiredIM != null)
                    rewiredInputManager = rewiredIM;
            }

            if (rewiredEventSystem == null)
                rewiredEventSystem = Instantiate(menuPrefabs.GetRewiredEventSystem, contentCanvas);
            if (rewiredInputManager == null)
                rewiredInputManager = Instantiate(menuPrefabs.RewiredInputManager);
        }

        void CheckAndAddMenuObject(IMechGameMenu menu) {
            switch (menu) {
                case MainMenuPanel main:
                    mainMenuPanel = main;
                    break;
                case LocalModePanel local:
                    localPlayPanel = local;
                    break;
                case OnlineModePanel online:
                    onlineModePanel = online;
                    break;
                case ControlMapperPanel control:
                    controlMapperPanel = control;
                    break;
                case SettingsPanel settings:
                    settingsPanel = settings;
                    break;
                case ProfileScreen profile:
                    profilePanel = profile;
                    break;
                case OnlineRoomPanel room:
                    onlineRoom = room;
                    break;
            }
        }

        #endregion

        #region eventHandlers

        static void OnInputMapperClosed() {
            Instance.controlMapperPanel.ControlMapper.onScreenClosed -= OnInputMapperClosed;
            RotationHelper.LoadLookSensitivity(ProfileManager.GetAllPlayerSettings());
            if (openedMenus.Count == 0 || openedMenus.Peek().GetMenuType() != MenuType.ControlMapper) return; //close
            var openedMenu = openedMenus.Pop();
            if (openedMenu == null) {
                Debug.LogError($"Closing input mapper, but currently open menu is {openedMenu}!");
            }

            if (openedMenus.Count > 0)
                openedMenus.Peek().Open();
            else
                instance.OpenMainMenu();
        }

        #endregion
    }
}