﻿using System;
using Core.Utility;
using MechAI;
using MechFighter;
using UnityEngine;

namespace Core.Input {
    using static MechGameConstants;
    using static Structs;
    using static MechUtils;
    
    public class InputParser {
        public static MechInputs ParseMechInputs(long inputs, bool disconected) {
            var mechInputs = new MechInputs((int)Buttons.Count);
            
            var bytes = BitConverter.GetBytes(inputs);
            
            //####    ROTATION   #####
            ushort shortYaw = BitConverter.ToUInt16(bytes, INPUT_YAW);
            short shortPitch = BitConverter.ToInt16(bytes, INPUT_PITCH);
            
            mechInputs.aimData.Heading  = shortYaw;
            mechInputs.aimData.pitch  = shortPitch;
            
            //####    MOVEMENT   #####
            byte movementX = bytes[INPUT_MOVEMENT];
            movementX &= MOVEMENT_X_MASK;
            byte movementY = bytes[INPUT_MOVEMENT];
            movementY &= MOVEMENT_Y_MASK;
            movementY >>= MOVEMENT_BITS;

            mechInputs.movement = new Vector2Int(GetMovement(movementX), GetMovement(movementY));

            //####    BUTTONS   #####
            for (int i = 0; i < (int)Buttons.Count; i++) {
                int buttonBitIndex = i + INPUT_FIRST_BUTTON;
                mechInputs.buttons[i] = (inputs & ((long) 1 << buttonBitIndex)) != 0;
            }

            return mechInputs;
        }

        public static long ReadInputs(int controllerId) {
            var inputs = new MechInputs();
            if (DummyCPU.IsCPU(controllerId)) {
                return SerializeMechInputs(DummyCPU.GetMechInputs(controllerId));
            }
            
            var rawInput = InputCollector.rawInput[controllerId];

            inputs.movement = Vector2ToInt(rawInput.movement);
            inputs.aimData = RotationHelper.aimInfo[controllerId];
            inputs.buttons = rawInput.gameButtons.ToArray();

            return SerializeMechInputs(inputs);
        }

        public static long SerializeMechInputs(MechInputs mechInputs) {
            long input = 0;
            byte[] bytes = new byte[8];
            
            //####    AIM   #####
            BitConverter.GetBytes(mechInputs.aimData.Heading).CopyTo(bytes, INPUT_YAW);
            BitConverter.GetBytes(mechInputs.aimData.pitch).CopyTo(bytes, INPUT_PITCH);
            
            //####    MOVEMENT   #####
            byte movementX = SerializeMovementInput(mechInputs.movement.x);
            byte movementY = SerializeMovementInput(mechInputs.movement.y);
            
            byte movement = movementY;
            movement <<= MOVEMENT_BITS;
            movement |= movementX;
            BitConverter.GetBytes(movement).CopyTo(bytes, INPUT_MOVEMENT);
            
            //write bits to long
            input = BitConverter.ToInt64(bytes, 0);
            
            //####    BUTTONS   #####
            for (int i = 0; i < (int)Buttons.Count; i++) {
                int buttonBitIndex = i + INPUT_FIRST_BUTTON;
                if (buttonBitIndex > INPUT_MAX)
                    Debug.LogError("[ MechGame ] overflown buttons input range");
                if (mechInputs.buttons[i])
                    input |= (long) 1 << buttonBitIndex;
            }

            return input;
        }
    }
}