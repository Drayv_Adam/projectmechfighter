﻿using System;
using Cinemachine;
using Core.Utility;
using GameViewAssets;
using MechFighter;
using Saving;
using UnityEngine;

namespace Core.Input {
    using static MechGameConstants;
    using static Structs;

    public class RotationHelper : MonoBehaviour {
        public static LookSettings[] lookSettings;
        public static RotationHelper[] rotationHelpers;
        public static AimData[] aimInfo;

        [SerializeField] private CinemachineVirtualCamera vCam;
        [SerializeField] CinemachineVirtualCamera lockedOnVCam;
        
        
        public Vector2 pitchMinMax = new Vector2(-40, 85);
        public float followHeightOffset = 1.5f;
        public float followLowProfileOffset = 0.3f;
        public float lockOnTurnMaxSpeed = 100f;

        [SerializeField] private float currentHeading;
        [SerializeField] private float currentPitch;
        [SerializeField] private Transform followTarget;
        [SerializeField] private bool lockOn;
        [SerializeField] private Transform lockOnTarget;
        [SerializeField] private CameraMode cameraMode;
        public bool LockOn => lockOn;
        
        private int controllerID = -1;
        private int lockOnID = -1;

        public short GetPitch() {
            return (short) (currentPitch * ANGLE_SCALE);
        }

        public ushort GetHeading() {
            return (ushort) (currentHeading * ANGLE_SCALE);
        }

        private void Update() {
            if (MechGame.IsGamePaused)
                return;
            UpdatePosition();
            HandleActiveCamera();
        }

        void HandleActiveCamera() {
            vCam.gameObject.SetActive(!lockOn);
            lockedOnVCam.gameObject.SetActive(lockOn);
            lockedOnVCam.LookAt = lockOnTarget;
        }
        
        public void Initialize(int controllerIndex, Transform target, AimData aimData) {
            controllerID = controllerIndex;
            followTarget = target;
            currentHeading = MechUtils.HeadingToFloat(aimData.Heading);
            currentPitch = MechUtils.PitchToFloat(aimData.pitch);
            vCam.gameObject.layer = V_CAM_LAYER[controllerIndex];
            lockedOnVCam.gameObject.layer = V_CAM_LAYER[controllerIndex];
            UpdateRotation(controllerIndex, Vector2.zero);
        }

        public static void LoadLookSensitivity(PlayerSettings[] settings) {
            for (var i = 0; i < lookSettings.Length; i++) {
                if (i >= settings.Length)
                    return;
                lookSettings[i] = settings[i].lookSettings;
            }
        }

        public static void UpdateRotation(int controllerID, Vector2 cameraDelta) {
            if (rotationHelpers == null || controllerID >= rotationHelpers.Length || MechGame.IsGamePaused) return;
            var helper = rotationHelpers[controllerID];
            if (helper == null)
                return;
            if (helper.lockOn)
                helper.LookAtLockedOnTarget();
            else
                helper.RotateCamera(cameraDelta);
            aimInfo[controllerID] = new AimData(helper.GetPitch(), helper.GetHeading());
        }

        public static void UpdateLockOn(int controllerID, bool lockedOn, byte targetID) {
            if (rotationHelpers == null || controllerID >= rotationHelpers.Length) return;
            var helper = rotationHelpers[controllerID];
            helper.UpdateLockOn(lockedOn, targetID);
        }

        public void UpdateLockOn(bool lockedOn, byte targetID) {
            if (!lockedOn) {
                BreakLockOn();
                return;
            }

            if (cameraMode == CameraMode.Cinematic || targetID == controllerID)
                return;
            SetLockOn(targetID);
        }

        private void SetLockOn(int targetID) {
            lockOn = true;
            lockOnID = targetID;
            lockOnTarget = MechGameViewManager.MechViews[targetID].transform;
            cameraMode = CameraMode.UserLockOn;
        }

        public Vector3 FollowTargetPosition => lockOnTarget == null
            ? Vector3.zero
            : lockOnTarget.position + Vector3.up * (IsTargetLow() ? followLowProfileOffset : followHeightOffset);
        
        private void LookAtLockedOnTarget() {
            if (lockOnTarget == null)
                BreakLockOn();

            var current = Quaternion.Euler(currentPitch, currentHeading, 0);
            
            var lookOffset = FollowTargetPosition - transform.position;
            if (lookOffset.sqrMagnitude < 0.01f)
                lookOffset = Vector3.down;
            var target = Quaternion.LookRotation(lookOffset, Vector3.up);
            var euler = Quaternion.RotateTowards(current, target, lockOnTurnMaxSpeed * Time.deltaTime).eulerAngles;

            if (euler.x  > 180)
                euler.x  -= 360;
            euler.x = Mathf.Clamp(euler.x, pitchMinMax.x, pitchMinMax.y);
            euler.z = 0;
            currentHeading = euler.y;
            currentPitch = euler.x;
            

            transform.rotation = Quaternion.Euler(euler);
        }

        public void BreakLockOn() {
            cameraMode = CameraMode.UserFree;
            lockOn = false;
            lockOnID = -1;
        }

        bool IsTargetLow() {
            switch (MechGameViewManager.MechViews[lockOnID].CurrentState.characterState) {
                case MechCharacterState.Crouching:
                case MechCharacterState.Knockdown:
                    return true;
            }
            return false;
        }
        
        private void RotateCamera(Vector2 cameraDelta) {
            currentHeading += cameraDelta.x;
            currentPitch -= cameraDelta.y;
            currentPitch = Mathf.Clamp(currentPitch, pitchMinMax.x, pitchMinMax.y);
            transform.rotation = Quaternion.Euler(currentPitch, currentHeading, 0);
        }

        private void UpdatePosition() {
            if (followTarget == null)
                return;
            InstantFollow(); //todo: add other kinds of follow modes, like smooth for certain situations, like far knockback
        }

        private void InstantFollow() {
            transform.position = followTarget.position + Vector3.up * followHeightOffset;
        }
    }
}