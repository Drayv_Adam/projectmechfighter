﻿using System;
using System.Linq;
using Core.Utility;
using JetBrains.Annotations;
using Rewired;
using UnityEngine;

namespace Core.Input {
    
    using static RewiredConsts.Action;
    public class InputCollector : MonoBehaviour {

#if UNITY_EDITOR
        [SerializeField] InputDisplay[] inputDisplay;
        [UsedImplicitly, Serializable] struct InputDisplay {
            public RawInput rawInput;
            public Structs.AimData aimData;
        }
#endif

        public static RawInput[] rawInput;
        
        static Player[] players;

        static int currentRuleSet;

        void Awake() {
            players = ReInput.players.GetPlayers().ToArray();
            rawInput = new RawInput[players.Length];
            RotationHelper.lookSettings = new Structs.LookSettings[players.Length];
            currentRuleSet = 0;
            Debug.Log($"{players.Length} rewired players");
        }

        void Update() {
            if (rawInput.Length != players.Length)
                rawInput = new RawInput[players.Length];

            for (var i = 0; i < players.Length; i++) {
                var player = players[i];
                rawInput[i] = GetInput(player, i);
                RotationHelper.UpdateRotation(i, rawInput[i].cameraMovement);
            }
#if UNITY_EDITOR
            inputDisplay = new InputDisplay[players.Length];
            for (int i = 0; i < players.Length; i++) {
                inputDisplay[i].rawInput = rawInput[i];
                if (RotationHelper.aimInfo != null && RotationHelper.aimInfo.Length > i)
                    inputDisplay[i].aimData = RotationHelper.aimInfo[i];
            }
#endif
        }

        RawInput GetInput(Player player, int controllerID) {
            return new RawInput() {
                movement = GetMovementVector(player),
                cameraMovement = GetCameraDelta(player, controllerID),
                menuButtons = {
                    confirm = player.GetButton(CONFIRM),
                    back = player.GetButton(BACK),
                    context = player.GetButton(CONTEXT),
                    pause = player.GetButton(PAUSE_MENU)
                },
                gameButtons = {
                    lockOn = player.GetButton(LOCKON),
                    jump = player.GetButton(JUMP),
                    dash = player.GetButton(DASH),
                    crouch = player.GetButton(CROUCH),
                    primary = player.GetButton(PRIMARY),
                    secondary = player.GetButton(SECONDARY),
                    melee = player.GetButton(MELEE),
                    guard = player.GetButton(GUARD),
                    utility1 = player.GetButton(UTILITY1),
                    utility2 = player.GetButton(UTILITY2),
                    utility3 = player.GetButton(UTILITY3),
                    utility4 = player.GetButton(UTILITY4)
                }
            };
        }

        static Vector2 GetMovementVector(Player player) {
            var movement = new Vector2(
                player.GetAxis(MOVE_LEFT_RIGHT),
                player.GetAxis(MOVE_BACK_FORWARD));
            return movement;
        }

        static Vector2 GetCameraDelta(Player player, int controllerID) {
            var sensitivity = RotationHelper.lookSettings[controllerID];
            var joystickDelta = new Vector2(
                player.GetAxis(JOYSTICK_LOOK_X) * sensitivity.joystick.X * 10,
                player.GetAxis(JOYSTICK_LOOK_Y) * sensitivity.joystick.Y * 10);
            var cameraDelta = new Vector2(
                player.GetAxis(MOUSE_LOOK_X) * sensitivity.mouse.X,
                player.GetAxis(MOUSE_LOOK_Y)  * sensitivity.mouse.Y);
            return cameraDelta + joystickDelta;
        }
        
        public static void SwitchAllPlayersRuleset(int rulesetID, bool hideCursor) {
            Cursor.lockState = hideCursor ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !hideCursor;
            if (currentRuleSet == rulesetID)
                return;
            for (int i = 0; i < players.Length; i++) {
                SwitchRuleset(players[i], rulesetID);
            }
            currentRuleSet = rulesetID;
        }
        
        static void SwitchRuleset(Player player, int rulesetID) {
            var mapEnabler = player.controllers.maps.mapEnabler;
            if (mapEnabler.ruleSets.Count <= rulesetID) {
                Debug.LogError("Invalid Rewired MapEnabler ruleset id");
                return;
            }
            mapEnabler.ruleSets[rulesetID].enabled = true;
            for (var i = 0; i < mapEnabler.ruleSets.Count; i++) {
                mapEnabler.ruleSets[i].enabled = i == rulesetID;
            }

            mapEnabler.Apply();
        }

        [Serializable]
        public struct RawInput {
            public Vector2 movement;
            public Vector2 cameraMovement;
            public Structs.MenuButtons menuButtons;
            public Structs.GameButtons gameButtons;
        }
    }
}