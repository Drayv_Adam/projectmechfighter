﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.StageInfo {
    public class StageInfoSetter : MonoBehaviour {
        [SerializeField]  StageInfoObject stageInfoObject;

        public StageInfoObject StageInfoObject => stageInfoObject;
        
        public void SaveInfo(int sceneId, int boundsRadius) {
            if (stageInfoObject == null) {
                Debug.LogError("No stage info object selected. Select a stage info object to save data to.");
                return;
            }

            TrySaveSceneId(sceneId);
            SaveSpawnPoints();
            TrySaveBoundsRadius(boundsRadius);
        }

        void TrySaveSceneId(int sceneId) {
            if (sceneId < 0 || sceneId > SceneManager.sceneCountInBuildSettings)
                Debug.LogError("Invalid Scene ID. Check Build Settings!");
            else
                stageInfoObject.stageInfo.sceneID = sceneId;
        }

        void SaveSpawnPoints() {
            var spawnPointSetters = FindObjectsOfType<SpawnPointSetter>();
            if (spawnPointSetters.Length < 2) {
                Debug.LogError("There are less than 2 spawn points on the scene. Add more Spawn Point Setters!");
                return;
            }
            stageInfoObject.stageInfo.spawnInfo = new SpawnInfo[spawnPointSetters.Length];
            for (var i = 0; i < spawnPointSetters.Length; i++) {
                stageInfoObject.stageInfo.spawnInfo[i] = spawnPointSetters[i].GetSpawnInfo();
            }
        }

        void TrySaveBoundsRadius(int boundsRadius) {
            if (boundsRadius <= 0)
                Debug.LogError("Invalid bounds radius. Has to be a positive number!");
            else
                stageInfoObject.stageInfo.boundsRadius = boundsRadius;
        }
    }
}