using Core.Utility;
using UnityEngine;

namespace Core.StageInfo {
    
    public class SpawnPointSetter : MonoBehaviour {
        const float GIZMO_SPHERE_RADIUS = 0.2f;
        const float GIZMO_LINE_HEIGHT = 2f;
    
        public SpawnInfo GetSpawnInfo() {
            return new SpawnInfo() {
                position = MechUtils.Vector3ToInt(transform.position),
                heading = MechUtils.FloatToHeading(transform.eulerAngles.y)
            };
        }
        
        #if UNITY_EDITOR
        void OnDrawGizmos() {
            var position = transform.position;
        
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(position, GIZMO_SPHERE_RADIUS);
            Gizmos.DrawLine(position, position + Vector3.up * GIZMO_LINE_HEIGHT);
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(position + Vector3.up * GIZMO_LINE_HEIGHT / 2, position + Vector3.up * GIZMO_LINE_HEIGHT / 2 + transform.forward);
        }
        #endif
    }
}
