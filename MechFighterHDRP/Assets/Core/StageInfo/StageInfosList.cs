﻿using Core.StageInfo;
using MechFighter;
using UnityEngine;

namespace Core.Utility {
    
    [CreateAssetMenu(fileName = "StageInfoCollection", menuName = "ScriptableObjects/Stage Info/Create StageInfo Collection Object", order = 1)]
    public class StageInfosList : ScriptableObject {
        public StageInfoObject[] stageInfoObjects;
        
        public StageInfo.StageInfo GetStageInfo(int stageId) {
            if (stageId >= stageInfoObjects.Length || stageId < 0) {
                Debug.LogError($"Failed To Find Given Stage Info stage id: {stageId} is out of range. Selecting a default one");
                if (stageInfoObjects[0].stageInfo.boundsRadius <= 0) {
                    Debug.LogError($"Stage {stageId} has bounds 0 or smaller. Needs to be a positive value! setting 20");
                    stageInfoObjects[0].stageInfo.boundsRadius =  20 * MechGameConstants.SCALE;
                }
                return stageInfoObjects[0].stageInfo;
            }
            if (stageInfoObjects[stageId] != null)
                return stageInfoObjects[stageId].stageInfo;
            Debug.LogError($"Failed To Find Given Stage Info stage id: {stageId}. StageInfoObject is null. Selecting a default one");
            return stageInfoObjects[0].stageInfo;
        }
    }
}