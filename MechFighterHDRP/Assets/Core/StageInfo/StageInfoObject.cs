﻿using System;
using UnityEngine;

namespace Core.StageInfo {
    [CreateAssetMenu(fileName = "StageInfo", menuName = "ScriptableObjects/Stage Info/Create StageInfo Object", order = 1)]
    public class StageInfoObject : ScriptableObject {
        public StageInfo stageInfo;
    }

    [Serializable]
    public struct StageInfo {
        public int sceneID;
        public SpawnInfo[] spawnInfo;
        public int boundsRadius;
    }

    [Serializable]
    public struct SpawnInfo {
        public Vector3Int position;
        public ushort heading;
    }
}