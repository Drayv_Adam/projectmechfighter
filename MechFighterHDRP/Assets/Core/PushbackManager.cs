﻿using Core.Utility;
using MechFighter;
using UnityEngine;

namespace Core {
    using static MechGameConstants;
    public class PushbackManager {
        const int MIN_MECH_SQR_DISTANCE = 60000000;
        public void UpdateMechPushback(Mech[] mechs) {
            var offset = mechs[0].position - mechs[1].position;
            offset.y = 0;

            if (offset == Vector3Int.zero) {
                var force = new Vector3Int(500, 0, 500);
                mechs[0].physicsComponent.AddForce(force);
                mechs[1].physicsComponent.AddForce(-force);
                return;
            }
            
            if (MechUtils.SqrMagnitude(offset) < MIN_MECH_SQR_DISTANCE && 
                Mathf.Abs(mechs[0].position.y - mechs[1].position.y) < MECH_HALF_HEIGHT) {

                offset.x = FunnyInverse(offset.x) / 50;
                offset.z = FunnyInverse(offset.z) / 50;
                
                mechs[0].physicsComponent.AddForce(offset);
                mechs[1].physicsComponent.AddForce(-offset);
            }
        }

        int FunnyInverse(int x) {
            if (x < 0)
                return x - SCALE;
            return SCALE - x;
        }
    }
}