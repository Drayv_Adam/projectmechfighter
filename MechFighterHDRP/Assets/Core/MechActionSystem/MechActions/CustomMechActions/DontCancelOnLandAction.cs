using Core.MechMovesets.MechActions;

namespace Core.MechActions {
    public class DontCancelOnLandAction : MechAction {
        
        public DontCancelOnLandAction(string actionName, int animationId, ushort endActionFrame, bool counterHitOnStart,
            MechActionEvent[] mechActionEvents) : base(actionName, animationId, endActionFrame, counterHitOnStart,
            mechActionEvents) {
            
        }

        public override bool OnLandingCancel() {
            return false;
        }
    }
}