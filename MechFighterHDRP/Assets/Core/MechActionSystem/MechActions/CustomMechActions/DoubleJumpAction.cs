using Core.MechComponents;
using Core.MechMovesets.MechActions;
using Core.Utility;

namespace Core.MechActions {
    public class DoubleJumpAction : MechAction {
        const short DOUBLEJUMP_COST = 2000;
        public override bool ConditionsSatisfied(Mech mech) {
            return !mech.grounded && mech.hasDoubleJump;
        }

        public DoubleJumpAction(string name, int startAnimationId, ushort endFrame, bool counterHitStateOnStart, MechActionEvent[] actionEvents) : base(
            name, startAnimationId, endFrame, counterHitStateOnStart, actionEvents) { }

        public override void Start(Structs.MechInputs mechInputs, int frameNumber, MechActionHandler handler) {
            base.Start(mechInputs, frameNumber, handler);
            handler.Mech.hasDoubleJump = false;
            handler.Mech.velocity.y = 0;
            if (handler.Mech.dashMeter < DOUBLEJUMP_COST)
                handler.Mech.dashMeter = 0;
            else
                handler.Mech.dashMeter -= DOUBLEJUMP_COST;
            handler.Mech.dashGainLockoutStart = frameNumber;
        }
    }
}