using Core.MechComponents;
using Core.MechMovesets.MechActions;
using Core.Utility;

namespace Core.MechActions {
    public class JumpAction : MechAction {
        public override bool ConditionsSatisfied(Mech mech) {
            return mech.grounded;
        }

        public JumpAction(string name, int startAnimationId, ushort endFrame, bool counterHitStateOnStart, MechActionEvent[] actionEvents) : base(
            name, startAnimationId, endFrame, counterHitStateOnStart, actionEvents) { }

        public override void Start(Structs.MechInputs mechInputs, int frameNumber, MechActionHandler handler) {
            base.Start(mechInputs, frameNumber, handler);
            handler.Mech.grounded = false;
            handler.Mech.TriggerOnAirborne(frameNumber);
            handler.Mech.physicsComponent.DisableGroundCheck = true;
        }
    }
}
