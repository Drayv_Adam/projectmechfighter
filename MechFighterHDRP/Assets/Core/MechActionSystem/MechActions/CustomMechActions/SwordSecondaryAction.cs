using Core.MechComponents;
using Core.MechMovesets.MechActions;
using Core.Utility;

namespace Core.MechActions {
    public class SwordSecondaryAction : MechAction {
        const short COST = 2000;
        
        public SwordSecondaryAction(string name, int startAnimationId, ushort endFrame, bool counterHitStateOnStart,
            MechActionEvent[] actionEvents) : base(name, startAnimationId, endFrame, counterHitStateOnStart,
            actionEvents) { }

        public override bool ConditionsSatisfied(Mech mech) {
            return mech.specialMeter >= COST;
        }

        public override void Start(Structs.MechInputs mechInputs, int frameNumber, MechActionHandler handler) {
            base.Start(mechInputs, frameNumber, handler);
            handler.Mech.specialMeter -= COST;
            handler.Mech.superGainLockoutStart = frameNumber;
        }
    }
}