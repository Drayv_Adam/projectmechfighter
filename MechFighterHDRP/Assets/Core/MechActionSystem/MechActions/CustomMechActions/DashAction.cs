using Core.MechComponents;
using Core.MechMovesets.MechActions;
using Core.Utility;

namespace Core.MechActions {
    public class DashAction : MechAction {
        
        const short MIN_DASH_COST = 350;
        const short TICK_DASH_COST = 30;

        public override bool ConditionsSatisfied(Mech mech) {
            return mech.dashMeter > MIN_DASH_COST;
        }

        public DashAction(string name, int startAnimationId, ushort endFrame, bool counterHitStateOnStart, MechActionEvent[] actionEvents) : base(
            name, startAnimationId, endFrame, counterHitStateOnStart, actionEvents) { }


        public override void Start(Structs.MechInputs mechInputs, int frameNumber, MechActionHandler handler) {
            base.Start(mechInputs, frameNumber, handler);
            handler.Mech.physicsComponent.HardResetVelocity();
            handler.Mech.characterState = Structs.MechCharacterState.Dashing;
            handler.Mech.dashGainLockoutStart = frameNumber;
        }
        
        public override void OnUpdate(byte lastEventId, int frameNumber, MechActionHandler handler, out bool isHolding) {
            base.OnUpdate(lastEventId, frameNumber, handler, out isHolding);
            handler.Mech.dashGainLockoutStart = frameNumber;
            if (handler.Mech.dashMeter > TICK_DASH_COST)
                handler.Mech.dashMeter -= TICK_DASH_COST;
            else {
                handler.Mech.dashGainLockoutStart = frameNumber + TICK_DASH_COST - handler.Mech.dashMeter;
                handler.Mech.dashMeter = 0;
            }
        }
    }
}