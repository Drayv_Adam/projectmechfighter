using Core.MechComponents;
using Core.MechMovesets.MechActions;
using Core.Utility;
using MechFighter;

namespace Core.MechActions {
    
    /*
     * Example Extended action utilizing as many of MechAction overrides as possible
     * to give examples and explain how to create more complex actions
     *
     * In general, this action basically a special prepare-to-shoot stance. Probably will actually be reused for GunMech Secondary
     */
    
    using static Structs;
    public class GunAimStanding : MechAction {
        readonly StanceParams stanceParams;
        readonly HitScanProperties[] hitScanProperties;

        readonly ushort specialMeterCost;

        public override bool LockOnOnStart => true;

        public GunAimStanding(string name, int startAnimationId, ushort endFrame, bool counterHitStateOnStart,
            MechActionEvent[] actionEvents, ushort specialMeterCost, StanceParams stanceParams,
            HitScanProperties[] hitScanProperties)
            : base(name, startAnimationId, endFrame, counterHitStateOnStart, actionEvents) {
            this.specialMeterCost = specialMeterCost;
            this.stanceParams = stanceParams;
            this.hitScanProperties = hitScanProperties;
        }

        public override void Start(MechInputs mechInputs, int frameNumber, MechActionHandler handler) {
            base.Start(mechInputs, frameNumber, handler);
            handler.Mech.selectedWeaponId = stanceParams.weaponId;
        }

        public override void OnUpdate(byte lastEventId, int frameNumber, MechActionHandler handler, out bool isHolding) {
            //base.OnUpdate(mechInputs, frameNumber, handler); //no need to call base.Methods. they're empty anyway
            isHolding = false;
            handler.Mech.lockedOn = true;
            handler.Mech.lockOnTarget = handler.Mech.handle == 0 ? (byte) 1 : (byte) 0; //works only for player, not cpu
            handler.Mech.CurrentWeaponRange = hitScanProperties[0].hitScanRange;

            //checking if we're in the part of the action, where we can shoot, or cancel and return to idle
            if (lastEventId != stanceParams.readyEventId) 
                return;
            
            if (HandleHold(frameNumber, handler)) return;
            isHolding = true; //setting isHolding to true, so actionHandler doesn't increment CurrentActionFrame
            HandleRotation(handler.Mech, handler.CurrentMechInputs.aimData.Heading);
            TryShoot(frameNumber, handler);
        }

        bool HandleHold(int frameNumber, MechActionHandler handler) {
            if (!handler.CurrentMechInputs.Secondary) {
                handler.JumpToEvent(stanceParams.recoveryStartEventId, frameNumber);
                //progress action to any of it's events.
                //in this case we jump to recovery portion of the action
                //frameNumber is nescesarry in TriggerEvent in actionHandler
                return true;
            }

            return false;
        }

        void TryShoot(int frameNumber, MechActionHandler handler) {
            if (handler.ButtonPressedOrBuffered(Buttons.Primary)) {
                //in this case we jump to the event where something happens, like shooting the gun
                //keep in mind that here we call an event one before the fire event,
                //we might want to have a slight delay between pulling the trigger and spawning the bullet (or firing a raycast)
                if (handler.Mech.specialMeter >= specialMeterCost) {
                    handler.Mech.specialMeter -= (short) specialMeterCost;
                    handler.Mech.superGainLockoutStart = frameNumber;
                    handler.JumpToEvent(stanceParams.attackStartEventId, frameNumber);
                    return;
                }

                if (handler.Mech.grounded)
                    handler.JumpToEvent(stanceParams.eventBId, frameNumber);
            }
        }

        void HandleRotation(Mech mech, ushort targetHeading) {
            mech.aimData.Heading = MechUtils.RotateTowards(mech.aimData.Heading, targetHeading, mech.parameters.turnSpeed);
            mech.aimData.pitch = mech.controllerComponent.CurrentAimData.pitch;
        }
        
        public override bool TriggerEvent(byte eventId, int frameNumber, MechActionHandler handler) {
            base.TriggerEvent(eventId, frameNumber, handler);
            if (eventId == stanceParams.triggerC) {
                handler.Mech.specialMeter += MechGameConstants.RELOAD_METER_GAIN;
                handler.JumpToEvent(stanceParams.readyEventId, frameNumber);
                return false;
            }
            if (eventId == stanceParams.triggerB) {
                Shoot(handler.Mech, true);
                return true;
            }

            if (IsEventA(eventId)) {
                Shoot(handler.Mech, false);
            }
            return true;
        }


        bool IsEventA(byte eventId) {
            if (stanceParams.triggersA == null)
                return false;
            for (int i = 0; i < stanceParams.triggersA.Length; i++) {
                if (eventId == stanceParams.triggersA[i])
                    return true;
            }

            return false;
        }
        
        void Shoot(Mech mech, bool finalShot) {
            var pos = mech.position;
            pos.y += hitScanProperties[0].originHeightOffset;
            
            
            HitScanProperties props;
            if (hitScanProperties.Length > 1 && finalShot)
                props = hitScanProperties[1];
            else
                props = hitScanProperties[0];
            
            mech.hitboxManager.AddHitScan(mech.handle, new HitScanData(pos, mech.aimData, props));
            //spawn bullet or do a raycast or smth
        }
    }
}