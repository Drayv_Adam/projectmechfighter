using System;
using Core.Utility;
using UnityEngine;

namespace Core.MechMovesets.MechActions {
    [Serializable]
    public class MechActionEvent {
        public bool ignoreNormally;
        public uint onFrame;
        public bool changeAnimation;
        public bool changeMovement;
        public bool changeHitbox;
        public bool changeCharacterState;
        public bool disableGroundCheck;
        public bool updateCancels;
        public bool changeChState;
        public bool counterHitState;
        public int animationId;
        public MovementData movementData;
        public HitboxData hitboxData;
        public Structs.MechCharacterState characterState;
        public ActionCancels actionCancels;
        //change stance
        //change mech counterhit state
        //
    }

    [Serializable]
    public struct ActionCancels {
        public bool resetHitResult;
        public Structs.HitResult cancelOn;
        public bool jump;
        public bool dash;
        public bool block;
        public bool grab;
        public bool primary;
        public bool secondary;
        public bool melee;
        public bool onlyDirectional;
        public bool onlyCrouching;
        public bool onlyStanding;
        public int[] specialActionIds;

        public bool IsAny => jump || dash || block || grab || primary || secondary || melee ||
                          specialActionIds != null && specialActionIds.Length > 0;
    }
    
    [Serializable]
    public struct MovementData {
        public Vector3Int localAcceleration;
        public int targetSpeed;
        public bool influencedByDInput;
        public bool normalizeDInput;
        public bool impactForce;
        public bool resetVelocity;
        public bool ignoreWeight;
    }

    [Serializable]
    public struct HitboxData {
        public Structs.Hitbox[] hitboxes;
        public bool resetHitFlags;
    }
}