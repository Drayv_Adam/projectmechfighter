using System;
using Core.MechComponents;
using Core.MechMovesets.MechActions;
using Core.Utility;

namespace Core.MechActions {

    [Serializable]
    public class MechAction {

        public readonly string name;
        public readonly MechActionEvent[] actionEvents;

        public readonly int startAnimationId;
        public readonly ushort endFrame;
        public readonly bool counterHitStateOnStart;
        
        public MechAction(string name, int startAnimationId, ushort endFrame, bool counterHitStateOnStart, MechActionEvent[] actionEvents) {
            this.name = name;
            this.startAnimationId = startAnimationId;
            this.endFrame = endFrame;
            this.actionEvents = actionEvents;
            this.counterHitStateOnStart = counterHitStateOnStart;
        }

        public virtual bool ConditionsSatisfied(Mech mech) => true; //check resources
        public virtual bool LockOnOnStart => false;
        
        public virtual void Start(Structs.MechInputs mechInputs, int frameNumber, MechActionHandler handler) {
            if (startAnimationId >= 0)
                handler.UpdateAnimation(startAnimationId, frameNumber);
            handler.Mech.counterHitState = counterHitStateOnStart;
        }

        public virtual void OnUpdate(byte lastEventId, int frameNumber,
            MechActionHandler handler, out bool isHolding) {
            isHolding = false; 
        }

        public virtual bool TriggerEvent(byte eventId, int frameNumber, MechActionHandler handler) {
            var evnt = actionEvents[eventId];
            if (evnt.changeAnimation) 
                handler.UpdateAnimation(evnt.animationId, frameNumber);
            
            if (evnt.changeHitbox) {
                handler.UpdateHitboxes(evnt.hitboxData);
            }

            if (evnt.changeMovement)
                handler.UpdateMovement(actionEvents[eventId].movementData);
            
            if (evnt.changeCharacterState)
                handler.Mech.characterState = evnt.characterState;

            if (evnt.changeChState)
                handler.Mech.counterHitState = evnt.counterHitState;

            if (evnt.updateCancels)
                handler.UpdateCancels(evnt.actionCancels);
            
            handler.Mech.physicsComponent.DisableGroundCheck = evnt.disableGroundCheck;
            return true;
        }

        public virtual bool OnLandingCancel() {
            return true;
        }
    }
}