using System;
using System.IO;
using Core.Connection;
using Core.Input;
using Core.MechComponents;
using Core.MechMovesets;
using Core.StageInfo;
using Core.Utility;
using SharedGame;
using Unity.Collections;
using UnityEngine;

namespace Core {
    using static Structs;

    [Serializable]
    public class MechGame : IGame {
        #region nonGameState

        //No need to serialize these, because they are set in the beginning and we can be sure are the same across all players and don't change
        public readonly SpawnInfo[] spawnPoints;
        public readonly PlayerInfo[] playerInfos;
        public readonly int boundsRadius;
        public readonly byte playerCount;
        public readonly MechParameters[] mechParameters;
        public int FramesLeft => gameModeManager.NextPhaseEndFrame - Framenumber;
        
        public readonly MechMoveset[] mechMovesets;
        readonly GameModeManager gameModeManager;
        readonly PushbackManager pushbackManager;

        public static bool IsGamePaused { get; set; }
        public static bool IsGameLocal { get; set; }

        #endregion

        public int Framenumber { get; private set; }
        public int Checksum => GetHashCode();

        //currentGameMode

        public readonly Mech[] mechs;
        public readonly MechController[] mechControllers;
        public readonly MechPhysicsComponent[] mechPhysicsComponents;
        public readonly MechHitController[] mechHitControllers;
        public readonly MechResourceController[] mechResourceControllers;
        public readonly HitboxManager hitboxManager;
        public readonly HitLagManager hitLagManager;
        
        public GamePhase gamePhase;

        #region Core

        public MechGame(MechGameParams mechGameParams) {
            Framenumber = 0;
            IsGameLocal = mechGameParams.isGameLocal;
            IsGamePaused = false;
            playerCount = (byte) mechGameParams.playerNum;
            spawnPoints = mechGameParams.stageInfo.spawnInfo;
            boundsRadius = mechGameParams.stageInfo.boundsRadius;
            mechs = new Mech[mechGameParams.playerNum];
            playerInfos = mechGameParams.playerInfos;
            mechParameters = mechGameParams.mechParams;
            mechMovesets = mechGameParams.mechMovesets;
            mechInputs = new MechInputs[mechGameParams.playerNum];
            mechControllers = new MechController[mechGameParams.playerNum];
            mechPhysicsComponents = new MechPhysicsComponent[mechGameParams.playerNum];
            mechHitControllers = new MechHitController[mechGameParams.playerNum];
            hitLagManager = new HitLagManager();
            hitboxManager = new HitboxManager(mechGameParams.playerNum);
            mechResourceControllers = new MechResourceController[mechGameParams.playerNum];
            gameModeManager = new GameModeManager();
            gamePhase = GamePhase.Awaiting;
            pushbackManager = new PushbackManager();

            InitializeMechs();
            RepositionAndRotateMechs(spawnPoints);
            GameModeManager.OnAwaitStart += HandleOnAwaitStart;
        }

        MechInputs[] mechInputs;
        
        public void Update(long[] inputs, int disconnectFlags) {
            if (IsGameLocal && IsGamePaused)
                return;
            Framenumber++;

            gamePhase = gameModeManager.HandleGamePhase(mechs, Framenumber, gamePhase);
            if (gamePhase != GamePhase.Game && gamePhase != GamePhase.GameEnd)
                return;
            
            for (int i = 0; i < playerCount; i++) {
                bool disconnected = (disconnectFlags & (1 << i)) != 0;
                mechInputs[i] = InputParser.ParseMechInputs(inputs[i], disconnected);
                mechControllers[i].UpdateInputs(mechInputs[i]);
            }

            if (hitLagManager.IsWaitingForHitLag(Framenumber))
                return;
            
            pushbackManager.UpdateMechPushback(mechs);

            for (int i = 0; i < playerCount; i++) {
                mechHitControllers[i].Update(Framenumber);
                mechControllers[i].Update(mechInputs[i], Framenumber);
                mechResourceControllers[i].Update(Framenumber);
            }

            hitboxManager.CheckHits(mechs, Framenumber);
            
            for (int i = 0; i < playerCount; i++) 
                mechPhysicsComponents[i].Update(Framenumber);
        }

        public void RestartGame() {
            gamePhase = GamePhase.Awaiting;
            ResetMechStates();
            RepositionAndRotateMechs(spawnPoints);
            gameModeManager.HandleAwaitStart(Framenumber);
        }
        
        #endregion

        #region Private Methods

        void InitializeMechs() {
            for (byte i = 0; i < playerCount; i++) {
                var mechParams = LoadMechParams(i);
                var movesetID = playerInfos[i].playerLoadoutData.movesetType;
                if (movesetID >= mechMovesets.Length) {
                    Debug.LogError($"Invalid movesetID of {movesetID}. Setting default");
                    movesetID = 0;
                }
                
                mechPhysicsComponents[i] = new MechPhysicsComponent(boundsRadius, mechParams);
                var actionHandler = new MechActionHandler();
                mechControllers[i] = new MechController(mechMovesets[movesetID]);
                mechHitControllers[i] = new MechHitController(hitLagManager);
                mechResourceControllers[i] = new MechResourceController();
                
                mechs[i] = new Mech(i, new MechComponent[] {
                        mechControllers[i],
                        mechPhysicsComponents[i],
                        actionHandler,
                        mechHitControllers[i],
                        mechResourceControllers[i]
                    }, mechParams, movesetID, hitboxManager);
                mechPhysicsComponents[i].Disabled = false;
                mechs[i].ResetState();
            }
        }

        void ResetMechStates() {
            for (int i = 0; i < playerCount; i++) {
                mechs[i].ResetState();
            }
        }
        
        void EnablePhysics() {
            for (int i = 0; i < playerCount; i++)
                mechPhysicsComponents[i].Disabled = false;
        }

        void HandleOnAwaitStart() {
            ResetMechStates();
            RepositionAndRotateMechs(spawnPoints);
        }

        MechParameters LoadMechParams(int handle) {
            var mechId = 0;
            if (playerInfos[handle].playerLoadoutData != null)
                mechId = playerInfos[handle].playerLoadoutData.mechId;
            if (mechId < 0 || mechId > mechParameters.Length) {
                Debug.LogError($"Invalid mechId of {mechId} on player {handle}");
                mechId = 0;
            }
            return mechParameters[mechId];
        }

        void RepositionAndRotateMechs(SpawnInfo[] spawnInfos) {
            var mechsCount = mechs.Length;
            var pointsCount = spawnInfos.Length;
            
            for (int i = 0; i < mechsCount; i++) {
                var index = i;
                if (i >= pointsCount) {
                    // get spawn offset, so mechs don't spawn on top of each other
                    index = i % pointsCount;
                }
                mechs[i].position = spawnInfos[index].position;
                mechs[i].aimData.Heading = spawnInfos[index].heading;
                mechs[i].aimData.pitch = 0;
            }
        }
        
        #endregion

        #region Serialization

        public override int GetHashCode() {
            return HashCode.Of(Framenumber).And(gamePhase).AndEach(mechs);
        }

        public void Serialize(BinaryWriter bw) {
            bw.Write(Framenumber);
            for (int i = 0; i < mechs.Length; i++) {
                mechs[i].Serialize(bw);
            }
        }

        public void Deserialize(BinaryReader br) {
            Framenumber = br.ReadInt32();

            for (int i = 0; i < mechs.Length; ++i) {
                mechs[i].Deserialize(br);
            }
        }

        public NativeArray<byte> ToBytes() {
            using var memoryStream = new MemoryStream();
            using var writer = new BinaryWriter(memoryStream);
            Serialize(writer);
            return new NativeArray<byte>(memoryStream.ToArray(), Allocator.Persistent);
        }

        public long ReadInputs(int controllerId) {
            return InputParser.ReadInputs(controllerId);
        }

        public void FromBytes(NativeArray<byte> bytes) {
            using var memoryStream = new MemoryStream(bytes.ToArray());
            using var reader = new BinaryReader(memoryStream);
            Deserialize(reader);
        }

        public void FreeBytes(NativeArray<byte> data) {
            if (data.IsCreated) {
                data.Dispose();
            }
        }

        public void LogInfo(string filename) {
            string fp = "";
            fp += "GameState object.\n";
            fp += $"  num_mechs: {mechs.Length}";
            for (int i = 0; i < mechs.Length; i++) {
                var mech = mechs[i];
                fp += $"  mech {i} position:  {mech.position.x}, {mech.position.y}, {mech.position.z}";
                fp += $"  mech {i} velocity:  {mech.velocity.x}, {mech.velocity.y}, {mech.velocity.z}";
                fp += $"  mech {i} aimData:   {mech.aimData.Heading}, {mech.aimData.pitch}";
                fp += $"  mech {i} health:    {mech.health}";
                //fp += $"  mech {i} state:     {mech.state}, lagEnd: {mech.lagEnd}";
                fp += $"  mech {i} score:     {mech.score}";
            }

            File.WriteAllText(filename, fp);
        }

        #endregion
    }
}