using System.Collections;
using System.Collections.Generic;
using Audio;
using Connection;
using Core.Connection;
using Core.Input;
using Core.Managers;
using Core.MechData;
using Core.Utility;
using MechAI;
using SharedGame;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityGGPO;

namespace Core {
    public class MechGameRunner : GameManager {

        [SerializeField] GameObject runnerContainer;
        [SerializeField] MechDataMapper mechDataMapper;
        [SerializeField] StageInfosList stageInfoList;
        [SerializeField] GameObject menuCamera;

        [SerializeField] LayerMask mechHitLayers;
        
        Coroutine loadSceneCor;

        Structs.MechGameParams mechGameParams;
        bool wasMechGameParamsSet;
        IGameRunner runner;

        public static LayerMask LayerMask { get; private set; }
        static int LocalPlayerNumber { get; set; }
        public static int LocalNonCPU { get; private set; }
        static int PlayerCount { get; set; }
        public new static MechGameRunner Instance { get; private set; }
        static GameObject MenuCamera { get; set; }
        public static MechGame MechGame => Instance.runner.Game is MechGame mechGame ? mechGame : default;
        public void Awake() {
            Instance = this;
            LayerMask = mechHitLayers;
            MenuCamera = menuCamera;
            GGPORunner.OnGameLog += HandleGGPOLog;
        }

        public void OnDestroy() {
            GGPORunner.OnGameLog -= HandleGGPOLog;
        }

        #region Public Methods

        public static void SetMenuCameraActive(bool active = true) {
            MenuCamera.SetActive(active);
        }

        #endregion
        
        #region StartGame

        public void StartLocalGame(StartGameData startGameData, bool[] cpuFlags) {
            mechGameParams = new Structs.MechGameParams(startGameData.playerInfos,
                stageInfoList.GetStageInfo(startGameData.stageId),
                mechDataMapper.MechParameters, mechDataMapper.MechMovesets, true); //todo: optimze and load only parameters and movesets used in game
            wasMechGameParamsSet = true;
            StartLocalGame(cpuFlags);
            InitializeControls(false);
            MenuManager.SetInGameContentActive();
        }
        
        public void StartGGPOGame(IPerfUpdate perfPanel, StartGameData startGameData, int localPort, bool spectate, int frameDelay) {
            mechGameParams = new Structs.MechGameParams(startGameData.playerInfos,
                stageInfoList.GetStageInfo(startGameData.stageId),
                mechDataMapper.MechParameters, mechDataMapper.MechMovesets, false);
            wasMechGameParamsSet = true;
            var connections = GetConnections(startGameData);
            StartGGPOGame(perfPanel, localPort, connections, spectate, frameDelay);
        }

        public void StartLocalGameWithoutLoadingScene(bool[] cpuFlags, StageInfo.StageInfo stageInfo) {
            var startGameData = StartGameData.DefaultStartGameData;
            mechGameParams = new Structs.MechGameParams(startGameData.playerInfos, stageInfo,
                mechDataMapper.MechParameters, mechDataMapper.MechMovesets, true);
            CountPlayers(cpuFlags);
            runner = new LocalRunner(new MechGame(mechGameParams));
            MenuManager.SetInGameContentActive();
            InitializeControls(false);
            StartGame(runner);
            AudioManager.PlayRandomBattleTheme();
        }
        
        #endregion
        
        #region GameManagerOverloads

        public override void StartLocalGame(bool[] cpuFlags) {
            if (!AreMechGameParamsSet())
                return;
            HUDManager.Instance.SetBlackOut();
            CountPlayers(cpuFlags);
            MechGame.IsGameLocal = true;
            runner = new LocalRunner(new MechGame(mechGameParams));
            StartLoadScene();
        }
        
        public override void StartGGPOGame(IPerfUpdate perfPanel, int localPort, IList<Connections> connections, bool spectate, int frameDelay) {
            if (!AreMechGameParamsSet())
                return;
            CountPlayers(connections);
            MechGame.IsGameLocal = false;
            var mechGame = new MechGame(mechGameParams);
            var ggpoRunner = new GGPORunner("mechfighter", mechGame, perfPanel);
            if (spectate)
                ggpoRunner.InitSpectator(localPort, mechGameParams.playerNum, connections[0].ip, connections[0].port); //todo: connect to non-spectator
            else
                ggpoRunner.Init(localPort, connections, frameDelay);
            runner = ggpoRunner;
            StartLoadScene(spectate);
        }

        #endregion

        #region LoadScene

        void StartLoadScene(bool spectate = false) {
            DontDestroyOnLoad(runnerContainer);
            if (loadSceneCor != null)
                StopCoroutine(loadSceneCor);
            loadSceneCor = StartCoroutine(LoadSceneCoroutine(mechGameParams.stageInfo.sceneID, spectate));
        }
        
        IEnumerator LoadSceneCoroutine(int sceneID, bool spectate) {
            MenuManager.SetupLoadingScreen(MenuType.None);
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneID);
            while (!asyncLoad.isDone)
                yield return null;
            yield return MenuManager.LoadingScreenDelay;
            InitializeControls(spectate);
            MenuManager.OnLoadingFinished();
            HUDManager.Instance.HUDToggle(true);
            StartGame(runner);
            AudioManager.PlayRandomBattleTheme();
        }

        #endregion

        #region Private Methods

        void InitializeControls(bool spectate) {
            if (spectate) {
                SpawnSpectator();
            }
            else  {
                if (RotationHelper.rotationHelpers == null) {
                    RotationHelper.rotationHelpers = new RotationHelper[LocalNonCPU];
                }
                
                RotationHelper.LoadLookSensitivity(ProfileManager.GetAllPlayerSettings());
                RotationHelper.aimInfo = new Structs.AimData[LocalNonCPU];
                //Initialize CPUs?
            }
            InputCollector.SwitchAllPlayersRuleset(RewiredConsts.MapEnablerRuleSet.GAME, true);
        }

        void HandleGGPOLog(string text) {
            Debug.Log($"[GGPO]: {text}");
        }

        void SpawnSpectator() {
            //todo
        }

        #endregion
        
        #region Helpers

        void CountPlayers(IEnumerable<Connections> connections) {
            ResetPlayerNumbers();
            foreach (var conn in connections) {
                if (!conn.spectator)
                    PlayerCount++;
                if (!conn.local) continue;
                LocalPlayerNumber++;
                DummyCPU.CPUFlags.Add(conn.cpu);
                if (!conn.cpu)
                    LocalNonCPU++;
            }
        }

        static void ResetPlayerNumbers() {
            PlayerCount = 0;
            LocalPlayerNumber = 0;
            LocalNonCPU = 0;
            DummyCPU.CPUFlags.Clear();
        }

        static void CountPlayers(bool[] cpuFlags) {
            ResetPlayerNumbers();
            foreach (var cpuFlag in cpuFlags) {
                PlayerCount++;
                LocalPlayerNumber++;
                DummyCPU.CPUFlags.Add(cpuFlag);
                if (!cpuFlag)
                    LocalNonCPU++;
            }
        }

        static List<Connections> GetConnections(StartGameData startGameData) {
            var list = new List<Connections>();
            foreach (var connectionData in startGameData.connectionsList) {
                list.Add(new Connections() {
                    port = connectionData.port,
                    ip = connectionData.ip,
                    spectator = connectionData.spectator,
                    local = connectionData.connectionId == ServerConnectionManager.Instance.MyConnectionId,
                    cpu = false, //todo?
                });
            }
            return list;
        }
        
        bool AreMechGameParamsSet() {
            if (wasMechGameParamsSet)
                return true;
            Debug.LogError("MechGameParams not set. If you call StartGame on GameManager make sure to set mech game params first!");
            return false;
        }
        
        #endregion
    }
}
