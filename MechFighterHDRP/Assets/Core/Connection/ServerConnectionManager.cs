using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Core;
using Core.Connection;
using Core.Managers;
using UI;
using UI.Popups;
using UnityEngine;
using Newtonsoft.Json;
using UnityGGPO;
using Random = UnityEngine.Random;

namespace Connection {
    public class ServerConnectionManager : MonoBehaviour {
        const string GAME_VERSION = "1";
        const int MAX_MESSAGE_SIZE = 1024;
        const byte MAX_ACTIVE_PLAYERS = 2;
        const byte MAX_PLAYERS_IN_ROOM = 8;
        public static ServerConnectionManager Instance;
        
        static Status status;
        
        static GameLiftClient gameLiftClient;

#if LOCALTESTING
        public string gameliftGameSessionId = "gsess-abc";
#endif
        [SerializeField] GgpoPerformancePanel performancePanel;
        [SerializeField] int frameDelay; //temp
        [SerializeField] int maxPlayersInRoom = 4; //temp
        [SerializeField] PlayerInfo playerInfo; //temp

        readonly Telepathy.Client telepathyClient = new Telepathy.Client(MAX_MESSAGE_SIZE);
        
        OpMessageHelper opMessageHelper;
        OnlineRoomManager onlineRoomManager;

        Coroutine antiSpamCor; //todo
        
        ushort ggpoPort;
        string playerSessionId;
        public int MyConnectionId { get; private set; }
        public bool IsHost => gameLiftClient.IsHost;
        public int MaxPlayersInRoom => Mathf.Clamp(maxPlayersInRoom, 0, MAX_PLAYERS_IN_ROOM);
        bool isJoiningSession;
        
        public static bool IsConnectedToServer { get; private set; }

        bool initialized;

        #region UnityCallbacks

        void Awake() {
            Instance = this;
            status = Status.Offline;
            
#if !UNITY_EDITOR
            playerInfo.playerName = "Random player " + Random.Range(0, 10000);
#endif
        }

        void Update() {
            // tick to process messages, (even if not connected so we still process disconnect messages)
            telepathyClient.Tick(100);
        }
        
        void OnApplicationQuit() {
            Disconnect();
        }

        #endregion

        #region Connection Callbacks

        void OnDisconnected() {
            //todo: Return To Menu;
            Debug.Log("Return To MainMenu");
            FallBackToOnlineMode();
            PopupManager.ShowPopup("Disconected");
        }
        
        public void OnJoinedSession(EnterRoomData enterRoomData) {
            if (isJoiningSession) {
                PopupManager.ShowPopup("Connected");
                IsConnectedToServer = true;
                isJoiningSession = false;
                
                MyConnectionId = enterRoomData.myConnectionId;
                
                onlineRoomManager = new OnlineRoomManager(enterRoomData.roomInfo);
                onlineRoomManager.RoomJoined(GetMyJoinedData(enterRoomData));
                onlineRoomManager.SetPlayerList(enterRoomData.playersInRoom);

                ShowOnlineRoomIfEnoughPlayers();
            }
        }

        public void PlayerJoined(PlayerJoinedData playerJoinedData) {
            onlineRoomManager.AddPlayer(playerJoinedData);
            ShowOnlineRoomIfEnoughPlayers();
        }

        void ShowOnlineRoomIfEnoughPlayers() {
            if (onlineRoomManager.PlayerCount >= OnlineRoomManager.MIN_PLAYERS)
                MenuManager.OnLoadingFinished(MenuType.OnlineRoom);
        }
        
        #endregion
        
        #region TelepathyMethods

        public void ConnectToServer(string ip, int port, string sessionId) {
            Debug.Log($"ConnectToServer ip: {ip}, port: {port}, session id: {sessionId}");
            playerSessionId = sessionId;

            // had to set these to 0 or else the TCP connection would timeout after the default 5 seconds.  
            // TODO: Investivate further.
            telepathyClient.SendTimeout = 0;
            telepathyClient.ReceiveTimeout = 0;

            telepathyClient.Connect(ip, port);
        }

        void SendNetworkMessage(NetworkMessage networkMessage) {
            var data = JsonConvert.SerializeObject(networkMessage);
            var encoded = Encoding.UTF8.GetBytes(data);
            var buffer = new ArraySegment<Byte>(encoded, 0, encoded.Length);
            telepathyClient.Send(buffer);
            //todo: use antispam cor
        }
        
        #endregion
        
        #region TelepaphyCallbacks
        void OnDataReceived(ArraySegment<byte> message) {
            string convertedMessage = Encoding.UTF8.GetString(message.Array ?? Array.Empty<byte>(), 0, message.Count);
            Debug.Log("Converted message: " + convertedMessage);
            NetworkMessage networkMessage = JsonConvert.DeserializeObject<NetworkMessage>(convertedMessage);
            opMessageHelper.ProcessMessage(networkMessage);
        }

        void OnConnected() {
            Debug.Log("Client Connected");
            ggpoPort = GetRandomPort();
            var connectionData = new PlayerConnectData {
                port = ggpoPort,
                playerInfo = playerInfo
            };
            NetworkMessage networkMessage = new NetworkMessage(NetworkMessage.OpCode.Connect, playerSessionId, Serialized(connectionData));
            Instance.SendNetworkMessage(networkMessage);
        }

        #endregion

        #region Public Methods

        public void InitializeOnlineMode() {
            if (initialized)
                return;
            opMessageHelper ??= new OpMessageHelper();
            opMessageHelper.Initialize(this);
            SubscribeToTelepathyEvents();
            gameLiftClient = new GameLiftClient(this);
            
            if (ProfileManager.Instance == null)
                ProfileManager.Initialize();
            ProfileManager.Instance?.UpdateOnlineLoadout(playerInfo.playerLoadoutData); //temp until we have saving
            initialized = true;
        }
        
        public void SetStatus(Status newStatus) {
            status = newStatus;
        }

        public void Disconnect() {
            if (!initialized)
                return;
            onlineRoomManager?.RoomLeft();
            gameLiftClient.CancelTasks();
            Instance.telepathyClient.Disconnect();
        }

        public void JoinRandomRoom() {
            if (!initialized)
                InitializeOnlineMode();
            isJoiningSession = true;
            gameLiftClient.JoinRandomSessionOrCreateOne();
            MenuManager.SetupLoadingScreen(MenuType.OnlineRoom, "Searching for opponents", true);
        }

        public void OnBecomeReady(bool ready) {
            if (status == Status.Connected && IsConnectedToServer) {
                SendNetworkMessage(new NetworkMessage(NetworkMessage.OpCode.Ready, playerSessionId, BitConverter.GetBytes(ready)));
            }
            else {
                PopupManager.ShowPopup("Tried Setting ready, but connection status isn't connected");
            }
        }

        public void OnPlayerReady(int connectionId, bool isReady) {
            onlineRoomManager.UpdateReady(connectionId, isReady);
        }
        
        public void TryTakeSlot(int connectionId, byte slotIndex) {
            onlineRoomManager.UpdatePlayerSlot(connectionId, slotIndex);
        }
        
        public void SendSlotTaken(byte slotIndex) {
            SendNetworkMessage(new NetworkMessage(NetworkMessage.OpCode.TryTakeSlot, playerSessionId, BitConverter.GetBytes(slotIndex)));
        }
        
        public static void TryStartGame(StartGameData startGameData) {
            if (status != Status.Connected)
                return;
            status = Status.Started;
            MechGameRunner.Instance.StartGGPOGame(Instance.performancePanel, startGameData, Instance.ggpoPort , false, GetFrameDelay());
        }
        
        public void RoomInfoChanged(RoomInfo roomInfo) {
            onlineRoomManager.UpdateRoomInfo(roomInfo);
        }
        
        public void LeaveRoom() {
            PopupManager.ShowPopup("Leaving Room");
            Disconnect();
        }

        public void CancelSearch() {
            MenuManager.CancelLoadingScreen();
        }
        
        public void FallBackToOnlineMode() {
            MenuManager.FallbackToMainMenu();
            MenuManager.OpenMenu(MenuType.OnlineMode);
        }

        public void InformNewStage(byte stageId){
            var data = BitConverter.GetBytes(stageId);
            SendNetworkMessage(new NetworkMessage(NetworkMessage.OpCode.SelectStage, playerSessionId, data));
        }
        
        #endregion
        

        #region Private Methods

        void SubscribeToTelepathyEvents() {
            telepathyClient.OnConnected = OnConnected;
            telepathyClient.OnData = OnDataReceived;
            telepathyClient.OnDisconnected = OnDisconnected;
        }

        static byte[] Serialized(object o) {
            var data = JsonConvert.SerializeObject(o);
            return Encoding.UTF8.GetBytes(data);
        }
        
        ushort GetRandomPort(ushort min = 7000, ushort max = 8000) {
            var port = Random.Range(min, max);
            TcpListener listener = new TcpListener(IPAddress.Loopback, port);
            listener.Start();
            port = ((IPEndPoint) listener.LocalEndpoint).Port;
            listener.Stop();
            Debug.Log($"Reserved port {port}");
            return (ushort)port;
        }
        
        PlayerJoinedData GetMyJoinedData(EnterRoomData enterRoomData) {
            return new PlayerJoinedData() {
                connectionId = enterRoomData.myConnectionId,
                isReady = false,
                playerSlot = enterRoomData.playerSlot,
                playerLoadout = ProfileManager.OnlineLoadout
            };
        }

        static int GetFrameDelay() {
            return Mathf.Clamp(Instance.frameDelay, 0, 10);
        }

        #endregion

        #region Structs

        public enum Status {
            Offline,
            Connected,
            Started,
            Finished,
        }
        
        #endregion
    }
}
