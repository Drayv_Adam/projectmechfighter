using System;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.GameLift;
using Amazon.GameLift.Model;
using Connection;
using UI.Popups;
using UnityEngine;

// Largely based on: 
// https://github.com/aws-samples/amazon-gamelift-unity/blob/master/Assets/Scripts/GameLift.cs
namespace Core.Connection {
   public class GameLiftClient
   {
      AmazonGameLiftClient amazonGameLiftClient;
      readonly ServerConnectionManager connectionManager;
      readonly string playerUuid;
      const string COGNITO_IDENTITY_POOL = "eu-central-1:edd5e3cd-c064-4141-a43e-87ed20ebf105";
      static readonly RegionEndpoint RegionEndpoint = RegionEndpoint.EUCentral1;
      const string FLEET_ID = "fleet-498c7286-893f-4ecb-a51f-974470385d58"; // TODO: probably don't hardcode this, use alias or something
      const string ALIAS_ID = "alias-8d795bd2-03a6-447b-8218-891a070f5cc8"; // TODO: probably don't hardcode this, use alias or something

      public bool IsHost { get; private set; }
      
      CancellationTokenSource cancellationTokenSource;
      CancellationToken cancellationToken;
      
      public GameLiftClient(ServerConnectionManager serverConnectionManager) {
         connectionManager = serverConnectionManager;
         // for this demo just create a randomly generated user id.  Eventually the ID may be tied to a user account.
         playerUuid = Guid.NewGuid().ToString();

         CreateGameLiftClient();
      }
      
      async void CreatePlayerSession(GameSession gameSession)
      {
         //this is effectivelly connecting to server. but due to how creating game session works,
         //we might prefer to keep it that we connect to server only once we have joined or created game session
         //since we can search gamesession before we connect to server... yeah... aws gamelift magic... kinda cool
         PlayerSession playerSession = null;

         var maxRetryAttempts = 3;
         await RetryHelper.RetryOnExceptionAsync<Exception>
         (maxRetryAttempts, async () => {
            playerSession = await CreatePlayerSessionAsync(gameSession);
         }, cancellationToken);

         if (playerSession != null) {
            // establish connection with server
            connectionManager.ConnectToServer(playerSession.IpAddress, playerSession.Port, playerSession.PlayerSessionId);
         }
      }

      async Task<PlayerSession> CreatePlayerSessionAsync(GameSession gameSession) {
         cancellationToken = cancellationTokenSource.Token;
         var createPlayerSessionRequest = new CreatePlayerSessionRequest {
            GameSessionId = gameSession.GameSessionId, 
            PlayerId = playerUuid
         };

         Task<CreatePlayerSessionResponse> createPlayerSessionResponseTask = amazonGameLiftClient.CreatePlayerSessionAsync(createPlayerSessionRequest, cancellationToken);
         CreatePlayerSessionResponse createPlayerSessionResponse = await createPlayerSessionResponseTask;

         string playerSessionId = createPlayerSessionResponse.PlayerSession != null ? createPlayerSessionResponse.PlayerSession.PlayerSessionId : "N/A";
         Debug.Log((int)createPlayerSessionResponse.HttpStatusCode + " PLAYER SESSION CREATED: " + playerSessionId);
         return createPlayerSessionResponse.PlayerSession;
      }

      async Task<GameSession> CreateGameSessionAsync() {
         Debug.Log("CreateGameSessionAsync");
         var createGameSessionRequest = new CreateGameSessionRequest {
#if LOCALTESTING
            FleetId = FLEET_ID,
            GameSessionId = connectionManager.gameliftGameSessionId,
#else        
            AliasId = ALIAS_ID,
#endif
            CreatorId = playerUuid,
            MaximumPlayerSessionCount = connectionManager.MaxPlayersInRoom
         };
         // can also use AliasId
         // search for two player game

         try {
            Task<CreateGameSessionResponse> createGameSessionRequestTask = amazonGameLiftClient.CreateGameSessionAsync(createGameSessionRequest, cancellationToken);
            CreateGameSessionResponse createGameSessionResponse = await createGameSessionRequestTask;
            IsHost = true;
            string gameSessionId = createGameSessionResponse.GameSession != null ? createGameSessionResponse.GameSession.GameSessionId : "N/A";
            Debug.Log((int)createGameSessionResponse.HttpStatusCode + " GAME SESSION CREATED: " + gameSessionId);
            return createGameSessionResponse.GameSession;
         }
         catch (Exception e) {
#if LOCALTESTING
            connectionManager.gameliftGameSessionId += "1";
#endif
            Debug.LogError(e.Message);
         }
         //Go Back
         return null;
      }

      async Task<GameSession> SearchGameSessionsAsync() {
         Debug.Log("SearchGameSessions");
         var searchGameSessionsRequest = new SearchGameSessionsRequest {
            AliasId = ALIAS_ID, // can also use AliasId
            FilterExpression = "hasAvailablePlayerSessions=true", // only ones we can join
            SortExpression = "creationTimeMillis ASC", // return oldest first
            Limit = 1 // only one session even if there are other valid ones
         };

         try {
            Task<SearchGameSessionsResponse> SearchGameSessionsResponseTask = amazonGameLiftClient.SearchGameSessionsAsync(searchGameSessionsRequest, cancellationToken);
            SearchGameSessionsResponse searchGameSessionsResponse = await SearchGameSessionsResponseTask;
            int gameSessionCount = searchGameSessionsResponse.GameSessions.Count;
            Debug.Log($"GameSessionCount:  {gameSessionCount}");

            if (gameSessionCount > 0) {
               Debug.Log("We have game sessions!");
               Debug.Log(searchGameSessionsResponse.GameSessions[0].GameSessionId);
               return searchGameSessionsResponse.GameSessions[0];
            }
         }
         catch (Exception e) {
            Debug.LogError(e.Message);
            //todo: remove logs
            PopupManager.ShowPopup("Failed to connect to server", PopupManager.PopupType.Error);
         }
         return null;
      }

      public async void JoinRandomSessionOrCreateOne() {
         // Mock game session queries aren't implemented for local GameLift server testing, so just return null to create new one
         IsHost = false;
         cancellationTokenSource = new CancellationTokenSource();
         cancellationToken = cancellationTokenSource.Token;

         var maxRetryAttempts = 3;
#if LOCALTESTING
         maxRetryAttempts = 5;
#endif
         
         
         
#if LOCALTESTING
         GameSession gameSession = null;
         await RetryHelper.RetryOnExceptionAsync<Exception>(maxRetryAttempts, async () => {
            gameSession = await TryDescribeGameSession();
         }, cancellationToken);
#else
         GameSession gameSession = await SearchGameSessionsAsync();
#endif

         if (gameSession != null) {
            Debug.Log("Game session found.");
            // game session found, create player session and connect to server
            CreatePlayerSession(gameSession);
            return;
         }

         // create one game session
            
         await RetryHelper.RetryOnExceptionAsync<Exception> (maxRetryAttempts, async () => {
            gameSession = await CreateGameSessionAsync();
         }, cancellationToken);

         if (gameSession != null) {
            Debug.Log("Game session created.");
            CreatePlayerSession(gameSession);
         }
         else {
            Debug.LogWarning("FAILED to create new game session.");
            PopupManager.ShowPopup("Failed to create new game sessions", PopupManager.PopupType.Error);
            connectionManager.CancelSearch();
         }
      }

#if LOCALTESTING
      async Task<GameSession> TryDescribeGameSession() {
         var describePlayerSession = new DescribeGameSessionsRequest() {
            GameSessionId = connectionManager.gameliftGameSessionId,
         };
         try {
            var describePlayerSessionsTask = amazonGameLiftClient.DescribeGameSessionsAsync(describePlayerSession, cancellationToken);
            var response = await describePlayerSessionsTask;
            if (response.GameSessions.Count > 0) {
               Debug.Log("We have game sessions!");
               Debug.Log(response.GameSessions[0].GameSessionId);
               return response.GameSessions[0];
            }
         }
         catch (Exception e) {
#if LOCALTESTING
            connectionManager.gameliftGameSessionId += "1";
#endif
            Debug.LogError(e.Message);
         }
         return null;
      }
#endif
      void CreateGameLiftClient() {
         Debug.Log("CreateGameLiftClient");
#if LOCALTESTING
         // local testing
            // guide: https://docs.aws.amazon.com/gamelift/latest/developerguide/integration-testing-local.html
            AmazonGameLiftConfig amazonGameLiftConfig = new AmazonGameLiftConfig() {
               ServiceURL = "http://localhost:9080"
            };
            amazonGameLiftClient = new AmazonGameLiftClient("asdfasdf", "asdf", amazonGameLiftConfig);
#else
         CognitoAWSCredentials credentials = new CognitoAWSCredentials (
            COGNITO_IDENTITY_POOL, // Identity pool ID
            RegionEndpoint // Region
         );
         amazonGameLiftClient = new AmazonGameLiftClient(credentials, RegionEndpoint);
#endif
      }

      public void CancelTasks() {
         if (cancellationTokenSource == null)
            return;
         cancellationTokenSource.Cancel();
         CleanupCancellationTokenSource();
      }

      void CleanupCancellationTokenSource() {
         cancellationTokenSource.Dispose();
         cancellationTokenSource = null;
      }
   }
}
