﻿using System;
using System.Text;
using Connection;
using Newtonsoft.Json;
using UI.Popups;
using UnityEngine;

namespace Core.Connection {
    public class OpMessageHelper {
        const string SEPARATOR = "\n";

        ServerConnectionManager manager;

        public void Initialize(ServerConnectionManager serverConnectionManager) {
            manager = serverConnectionManager;
        }
        
        public void ProcessMessage(NetworkMessage networkMessage) {
            switch (networkMessage.opCode) {
                case NetworkMessage.OpCode.Connect:
                    OnConnectionAcknowledged(networkMessage.optionalData);
                    break;
                case NetworkMessage.OpCode.Start:
                    OnStart(networkMessage);
                    break;
                case NetworkMessage.OpCode.Disconnect:
                    OnPlayerDisconnected();
                    break;
                case NetworkMessage.OpCode.Close:
                    GameEnded();
                    break;
                case NetworkMessage.OpCode.SelectStage:
                    //they can't select stage, maybe at best show a picture of currently selected stage, with no way to interact with it?
                    PopupManager.ShowPopup("Select stage op");
                    break;
                case NetworkMessage.OpCode.UpdateLoadout:
                    OnPlayerUpdatedLoadout(networkMessage.optionalData);
                    break;
                case NetworkMessage.OpCode.Ready:
                    OnPlayerReady(networkMessage.optionalData);
                    break;
                case NetworkMessage.OpCode.PlayerJoined:
                    OnPlayerJoined(networkMessage.optionalData);
                    break;
                case NetworkMessage.OpCode.TryTakeSlot:
                    OnPlayerTookSlot(networkMessage.optionalData);
                    break;
                case NetworkMessage.OpCode.GameResult:
                    //we shouldn't get this if we send it
                    //we could get this if we entered a room while a match was in progress and we get notified about it's end
                    break;
                case NetworkMessage.OpCode.Error:
                    //Depending on which state we are in, error should do diffirent things, such as disconect or simply notify about failed action.
                    PopupManager.ShowPopup(Encoding.UTF8.GetString(networkMessage.optionalData));
                    break;
                case NetworkMessage.OpCode.RoomInfoChanged:
                    OnRoomInfoChanged(networkMessage.optionalData);
                    break;
                default:
                    Debug.LogWarning($"Unknown message type received {networkMessage.opCode}, with {networkMessage.optionalData.Length} bytes of data: \n {BitConverter.ToInt64(networkMessage.optionalData, 0)} \n to string: {Encoding.UTF8.GetString(networkMessage.optionalData)}");
                    break;
            }
        }
        
        void OnRoomInfoChanged(byte[] data) {
            var roomInfo = JsonConvert.DeserializeObject<RoomInfo>(ConvertedData(data));
            manager.RoomInfoChanged(roomInfo);
        }

        void OnConnectionAcknowledged(byte[] data) {
            var enterRoomData = JsonConvert.DeserializeObject<EnterRoomData>(ConvertedData(data));
            //enterRoomData.playersInRoom
            PopupManager.ShowPopup($"There are {enterRoomData.playersInRoom.Count} players. My ConnectionID = {enterRoomData.myConnectionId}");
            manager.SetStatus(ServerConnectionManager.Status.Connected);
            manager.OnJoinedSession(enterRoomData);
            //todo show quickplay room
        }

        void OnStart(NetworkMessage networkMessage) {
            var convertedMessage = ConvertedData(networkMessage.optionalData);
            var startGameData = JsonConvert.DeserializeObject<StartGameData>(convertedMessage);
            if (startGameData.connectionsList == null || startGameData.connectionsList.Count < 2) {
                return;
            }
            PopupManager.ShowPopup($"Player Connections: {startGameData.connectionsList.Count}");

            var text = string.Concat(startGameData.connectionsList[0].ip, ":", startGameData.connectionsList[0].port,
                startGameData.connectionsList[0].spectator ? " spectator \n" : " player \n",
                startGameData.connectionsList[1].ip, ":", startGameData.connectionsList[1].port,
                startGameData.connectionsList[1].spectator ? " spectator \n" : " player \n");
            PopupManager.ShowPopup(text);
            ServerConnectionManager.TryStartGame(startGameData);
            //todo: start ggpo session
        }

        void OnPlayerReady(byte[] data) {
            var readyData = JsonConvert.DeserializeObject<UpdateReadyData>(ConvertedData(data));
            manager.OnPlayerReady(readyData.connectionId, readyData.isReady);
        }

        void OnPlayerJoined(byte[] data) {
            var playerData = JsonConvert.DeserializeObject<PlayerJoinedData>(ConvertedData(data));
            manager.PlayerJoined(playerData);
        }

        void OnPlayerTookSlot(byte[] data) {
            var takeSlotData = JsonConvert.DeserializeObject<TakeSlotData>(ConvertedData(data));
            manager.TryTakeSlot(takeSlotData.connectionId, takeSlotData.playerSlot);
        }
        
        void OnPlayerUpdatedLoadout(byte[] data) {
            var loadout = JsonConvert.DeserializeObject<PlayerLoadoutData>(ConvertedData(data));
            
            PopupManager.ShowPopup($"Updated loadout: mechId: {loadout.mechId} moveset: {loadout.movesetType}");
        }
        
        void OnPlayerDisconnected() {
            PopupManager.ShowPopup("Player disconected", PopupManager.PopupType.Warning);
            manager.LeaveRoom();
            //todo: pause game
        }

        void GameEnded() {
            manager.SetStatus(ServerConnectionManager.Status.Finished);
        }
        
        static string ConvertedData(byte[] data) => Encoding.UTF8.GetString(data, 0, data.Length);
    }
}