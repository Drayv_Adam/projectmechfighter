﻿using System;
using Saving;

namespace Core.Connection {
    [Serializable]
    public struct PlayerProfile {
        public PlayerInfo playerInfo; //name, loadout
        public PlayerSettings playerSettings;
    }
}