﻿using System;
using System.Collections.Generic;

namespace Core.Connection {
    

    [Serializable]
    public struct NetworkMessage {
        public const byte SPECTATOR_SLOT_ID = 100;
        
        public OpCode opCode;
        public string playerSessionId;
        public byte[] optionalData;

        public NetworkMessage(OpCode opCode, string playerSessionId, byte[] optionalData = null) {
            this.opCode = opCode;
            this.playerSessionId = playerSessionId;
            this.optionalData = optionalData;
        }
    
        public enum OpCode {
            Connect, //client: PlayerConnectionData, server: EnterRoomData
            PlayerJoined, //PlayerJoinedData
            Disconnect, //was disconnected player a player
            RoomInfoChanged, //newRoomInfo. Remove All players from player slots
            SelectStage, //new stage
            UpdateLoadout, //UpdateLoadoutData
            TryTakeSlot, // sbyte slot ID
            GameResult,
            Ready, //client: isReady, server: UpdateReadyData
            Start, //ipAddresses and Game info (stage + loadout)
            Close,
            Error //string
        }
    }

    [Serializable]
    public struct GameFinishResult {
        public int winnerId;
    }

    [Serializable]
    public struct EnterRoomData {
        public int myConnectionId;
        public byte playerSlot;
        public List<PlayerJoinedData> playersInRoom;
        public RoomInfo roomInfo;
    }

    [Serializable]
    public struct RoomInfo {
        public byte roomCapacity;
        public byte maxPlayers;
        public byte minPlayers;
        public int hostConnId;
        public byte selectedStageId;
    }
    
    [Serializable]
    public struct PlayerConnectData {
        public ushort port;
        public PlayerInfo playerInfo;
    }

    [Serializable]
    public struct UpdateReadyData {
        public int connectionId;
        public bool isReady;
    }

    [Serializable]
    public struct UpdateLoadoutData {
        public int connectionId;
        public PlayerLoadoutData newLoudout;
    }
    
    [Serializable]
    public class PlayerJoinedData {
        public int connectionId;
        public string playerName;
        public byte playerSlot;
        public bool isReady;
        public PlayerLoadoutData playerLoadout;
    }

    [Serializable]
    public struct TakeSlotData {
        public int connectionId;
        public byte playerSlot;
    }
    
    [Serializable]
    public struct StartGameData {
        public List<PlayerConnectionData> connectionsList;
        public PlayerInfo[] playerInfos;
        public byte stageId;

        public static readonly StartGameData DefaultStartGameData = new StartGameData {
            connectionsList = new List<PlayerConnectionData> {
                new PlayerConnectionData {connectionId = 1, ip = "127.0.0.1", port = 7000, spectator = false},
                new PlayerConnectionData {connectionId = 2, ip = "127.0.0.1", port = 7001, spectator = false}
            },
            playerInfos = new [] {
                new PlayerInfo {playerName = "player 1", playerLoadoutData = new PlayerLoadoutData{mechId = 0, movesetType = 0}},
                new PlayerInfo {playerName = "player 2", playerLoadoutData = new PlayerLoadoutData{mechId = 1, movesetType = 1}}
            },
            stageId = 0
        };
    }

    [Serializable]
    public struct PlayerConnectionData {
        public ushort port;
        public string ip;
        public int connectionId; //if on startGame this connectionId is equal to connection ID player got on connecting, he is this player
        public bool spectator;
    }

    [Serializable]
    public struct PlayerInfo {
        public string playerName;
        public PlayerLoadoutData playerLoadoutData;
    }

    [Serializable]
    public class PlayerLoadoutData {
        public int mechId;
        public byte movesetType;
    }
}