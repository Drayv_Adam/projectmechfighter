namespace Core {
    public class HitLagManager {

        int hitLagEnd;
        
        public bool IsWaitingForHitLag(int framenumber) {
            return hitLagEnd > framenumber;
        }

        public void AddHitlag(byte deathHitStop, int frameNumber) {
            hitLagEnd = frameNumber + deathHitStop;
        }
    }
}