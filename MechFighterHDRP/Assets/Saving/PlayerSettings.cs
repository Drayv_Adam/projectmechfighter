﻿using System;
using Core.Utility;

namespace Saving {
    
    using static Structs;
    
    [Serializable]
    public struct PlayerSettings {
        public LookSettings lookSettings;
    }
}