using Rewired;
using Rewired.Integration.UnityUI;
using UI.Menu;
using UI.OnlineMenu;
using UI.PauseMenu;
using UI.Popups;
using UI.RoomUI;
using UI.Visuals;
using UnityEngine;

namespace UI {
    [CreateAssetMenu(fileName = "MechGameMenuList", menuName = "ScriptableObjects/UI/MenuList ")]
    public class MenuPrefabs : ScriptableObject {
        [SerializeField] RewiredEventSystem rewiredEventSystemPrefab;
        [SerializeField] InputManager rewiredInputManager;
        [SerializeField] MainMenuPanel mainMenuPanel;
        [SerializeField] OnlineModePanel onlineMenuPanel;
        [SerializeField] ProfileScreen profileScreenPanel;
        [SerializeField] ControlMapperPanel controlMapperPanel;
        [SerializeField] LocalModePanel localModePanel;
        [SerializeField] LocalVersusPanel localVersusPanel;
        [SerializeField] SettingsPanel settingsPanel;
        [SerializeField] LoadingScreen loadingScreen;
        [SerializeField] OnlineRoomPanel onlineRoomPanel;
        [SerializeField] PopupManager popupManagerPrefab;
        [SerializeField] Popup popupPrefab;
        [SerializeField] BackgroundChanger backgroundChanger;
        [SerializeField] PauseMenuPanel pauseMenuPanel;
        [SerializeField] RematchMenuPanel rematchMenuPanel;
        [SerializeField] CreditsMenuPanel creditsMenuPanel;
        [SerializeField] SoundSettings soundSettings;
        public RewiredEventSystem GetRewiredEventSystem => rewiredEventSystemPrefab;
        public InputManager RewiredInputManager => rewiredInputManager;
        public MainMenuPanel MainMenuPanel => mainMenuPanel;
        public OnlineModePanel OnlineMenuPanel => onlineMenuPanel;
        public ProfileScreen ProfileScreenPanel => profileScreenPanel;
        public ControlMapperPanel ControlMapperPanel => controlMapperPanel;
        public LocalModePanel LocalModePanel => localModePanel;
        public LocalVersusPanel LocalVersusPanel => localVersusPanel;
        public SettingsPanel SettingsPanel => settingsPanel;
        public LoadingScreen LoadingScreen => loadingScreen;
        public OnlineRoomPanel OnlineRoomPanel => onlineRoomPanel;
        public PopupManager PopupManagerPrefab => popupManagerPrefab;
        public Popup PopupPrefab => popupPrefab;
        public BackgroundChanger BackgroundChanger => backgroundChanger;
        public PauseMenuPanel PauseMenuPanel => pauseMenuPanel;
        public RematchMenuPanel RematchMenuPanel => rematchMenuPanel;

        public CreditsMenuPanel CreditsMenuPanel => creditsMenuPanel;

        public SoundSettings SoundSettings => soundSettings;

    }
    public enum MenuType {
        None,
        MainMenu,
        LocalMode,
        OnlineMode,
        Profile,
        ControlMapper,
        Settings,
        LoadingScreen,
        OnlineRoom,
        LocalVersus,
        BackgroundChanger,
        Pause,
        Rematch,
        Credits,
        Sound
    }
}
    