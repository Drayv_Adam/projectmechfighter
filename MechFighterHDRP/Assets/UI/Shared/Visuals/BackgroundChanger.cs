using UnityEngine;
using UnityEngine.UI;

namespace UI.Visuals{
    public class BackgroundChanger : MonoBehaviour {
        public const int MAP_COUNT = 3;
        public static BackgroundChanger Instance{ get; private set; }
        [SerializeField] Sprite[] mapImagesToSet;
        [SerializeField] RawImage[] mapVideosToSet;
        [SerializeField] Sprite[] menuImagesToSet;
        [SerializeField] RawImage[] menuVideosToSet;
        [SerializeField] Image backgroundImage;
        


        void Awake() {
            Instance = this;
        }
        
        public static void SetMapBackground(int mapType) {
            if (Instance.mapImagesToSet.Length > mapType && Instance.mapImagesToSet[mapType]) {
                Instance.backgroundImage.sprite = Instance.mapImagesToSet[mapType];
                SetAnimatedMapBackground(mapType);
            }
        }
        
        public static void SetMenuBackground(int menuType) {
            if (Instance.menuImagesToSet.Length > menuType && Instance.menuImagesToSet[menuType]) {
                Instance.backgroundImage.sprite = Instance.menuImagesToSet[menuType];
                SetAnimatedMainBackground(menuType);
            }
        }
        
        static void SetAnimatedMapBackground(int mapType) {
            DisableAnimatedBackgrounds();
            Instance.mapVideosToSet[mapType].enabled = true;
        }
        
        static void SetAnimatedMainBackground(int menuType) {
            DisableAnimatedBackgrounds();
            Instance.menuVideosToSet[menuType].enabled = true;
        }

        static void DisableAnimatedBackgrounds() {
            foreach (var map in Instance.menuVideosToSet)
                map.enabled = false;
            foreach (var map in Instance.mapVideosToSet)
                map.enabled = false;
        }


        public void Open() {
            gameObject.SetActive(true);
        }

        public void Close() {
            gameObject.SetActive(false);
            
        }
        
        
    }
}
