using System;
using Core.Connection;
using Core.Managers;
using Saving;
using UI.Popups;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class ProfileScreen : MonoBehaviour, IMechGameMenu {
        [SerializeField] InputField playerNameField;
        [SerializeField] InputField customField;
        [SerializeField] Button save;
        [SerializeField] Button close;
        
        public static int EditedProfileId { get; set; }

        void Awake() {
            save.onClick.AddListener(Save);
            close.onClick.AddListener(GoBack);
        }

        void OnDestroy() {
            save.onClick.RemoveListener(Save);
            close.onClick.RemoveListener(GoBack);        
        }

        public MenuType GetMenuType() {
            return MenuType.Profile;
        }

        public void Open() {
            gameObject.SetActive(true);
            close.Select();
            PopupManager.ShowPopup("Todo: Remember to add player settings here too. Including controls", PopupManager.PopupType.Warning);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        void GoBack() {
            MenuManager.CloseCurrentMenu();
        }
        
        void Save() {
            if (ProfileManager.Instance == null)
                ProfileManager.Initialize();

            var profile = new PlayerProfile() {
                playerInfo = new PlayerInfo() {
                    playerName = GetPlayerName(),
                    playerLoadoutData = new PlayerLoadoutData() {
                        mechId = GetCustomFieldValue(),
                        movesetType = (byte)GetCustomFieldValue()
                    }
                },
                //todo: playerSettings
            };
            
            ProfileManager.Instance?.Save(EditedProfileId, profile);
        }

        string GetPlayerName() {
            if (playerNameField == null || playerNameField.text == string.Empty) return "No name";
            return playerNameField.text;
        }

        int GetCustomFieldValue() {
            if (customField == null) return 0;
            return Int32.TryParse(customField.text, out int num) ? num : 0;
        }
    }
}
