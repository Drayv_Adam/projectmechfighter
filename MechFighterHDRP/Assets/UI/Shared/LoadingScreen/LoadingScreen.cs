using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class LoadingScreen : MonoBehaviour, IMechGameMenu {
        public const string DEFAULT_HEADLINE = "Loading";
        const char DOT = '.';
        const float ADD_DOT_TIME = 0.5f;
        const byte MAX_DOTS_COUNT = 4;
        
        [SerializeField] Text header;
        [SerializeField] Button cancelButton;
        [SerializeField] Slider loadingSlider;
        [SerializeField] float loadingSliderSpeed;
        public string headline = DEFAULT_HEADLINE;
        public MenuType targetMenu;
        
        string dots;
        byte dotCount;
        float timer;
        bool allowCancel;
        
        public bool IsOpen { get; private set; }

        void Awake() {
            cancelButton.onClick.AddListener(OnCancelClicked);
        }

        void OnDestroy() {
            cancelButton.onClick.RemoveListener(OnCancelClicked);
        }

        public void Setup(MenuType target, string newHeadline = DEFAULT_HEADLINE, bool allowCancel = false) {
            targetMenu = target;
            headline = newHeadline;
            dotCount = 0;
            timer = ADD_DOT_TIME;
            dots = string.Empty;
            IsOpen = true;
            this.allowCancel = allowCancel;
            UpdateHeader();
            UpdateCancelButton(allowCancel);
        }

        public void Update() {
            if (!enabled) return;
            loadingSlider.value = Mathf.Lerp(loadingSlider.value, 100, loadingSliderSpeed * Time.deltaTime);
            if (loadingSlider.value>80) {
                loadingSlider.value = 100;
            }
            if (timer > 0)
                timer -= Time.deltaTime;
            else {
                timer = ADD_DOT_TIME;
                //AddDot();
            }
        }

        void AddDot() {
            dots += DOT;
            UpdateHeader();
            dotCount++;
            if (dotCount > MAX_DOTS_COUNT) {
                dotCount = 0;
                dots = string.Empty;
            }
        }

        void UpdateHeader() {
            header.text = headline + dots;
        }

        void UpdateCancelButton(bool cancelButtonActive) {
            cancelButton.enabled = cancelButtonActive;
            cancelButton.gameObject.SetActive(cancelButtonActive);
        }

        void OnCancelClicked() {
            if (!allowCancel || !IsOpen)
                return;
            MenuManager.CancelLoadingScreen();
        }
        
        public MenuType GetMenuType() {
            return MenuType.LoadingScreen;
        }

        public void Open() {
            loadingSlider.value = 0;
            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
            timer = 0;
            dots = string.Empty;
            dotCount = 0;
            IsOpen = false;
        }
    }
}
