﻿using System;
using UnityEngine;

namespace UI.Shared {
    [CreateAssetMenu(fileName = "ColorTheme", menuName = "ScriptableObjects/UI/ColorTheme Object", order = 1)]
    public class ColorTheme : ScriptableObject {

        public static ColorTheme instance;

        public static PlayerListColors PlayerList => instance.playerListColors;
        public static Color DefaultFont => instance.defaultFontColor;
        
        public Color defaultBackgroundColor;
        public Color defaultFontColor = new Color(0.196f, 0.196f, 0.196f);
        
        public PlayerListColors playerListColors;
        public PopupIconColors popupIconColors;
        public ButtonColors buttonColors;

        [Serializable]
        public struct PlayerListColors {
            public Color hostBackground;
            public Color defaultPlayerBackground;
            public Color playerIconColor;
            public Color spectatorIconColor;
        }
        
        [Serializable]
        public struct PopupIconColors {
            public Color info;
            public Color warning;
            public Color error;
        }

        [Serializable]
        public class ButtonColors {
            public Color readyButton_Ready = new Color(0, 118, 0);
            public Color readyButton_NotReady = new Color(0, 255, 0);
            public Color readyText_Ready = new Color(255, 255, 255);
            public Color readyText_NotReady = new Color(50, 50, 50);

            public Color GetReadyButtonColor(bool isReady) {
                return isReady ? readyButton_Ready : readyButton_NotReady;
            }

            public Color GetReadyButtonTextColor(bool isReady) {
                return isReady ? readyText_Ready : readyText_NotReady;
            }
        }
    }
}