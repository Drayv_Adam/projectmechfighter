using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace UI.RoomUI.StageSelect {
    public class StageSelectPanel : MonoBehaviour{

        public event Action<byte> OnMapChange;

        [SerializeField] StageSelectButton[] stageButtons;
        [SerializeField] GameObject backGroundPanel;
        public StageSelectButton[] StageButtons => stageButtons;

        int selectedStageId;

        bool interactable;

        void Awake() {
            for (var i = 0; i < stageButtons.Length; i++) {
                stageButtons[i].stageId = i;
                stageButtons[i].OnStageSelected += SetSelectedStageId;
            }
        }

        void OnDestroy() {
            for (var i = 0; i < stageButtons.Length; i++) {
                stageButtons[i].stageId = i;
                stageButtons[i].OnStageSelected -= SetSelectedStageId;
            }
        }

        void SetSelectedStageId(int id) {
            foreach (var stageButton in stageButtons){
                stageButton.SelectBorder.gameObject.SetActive(false);
                stageButton.image.enabled = true;
            }
            if (StageButtons != null && StageButtons.Length > id) {
                StageButtons[id].SelectBorder.gameObject.SetActive(true);
                StageButtons[id].image.enabled = false;
                StageButtons[id].Select();
                selectedStageId = id;
                OnMapChange?.Invoke((byte) id);
            }
            else
                Debug.LogError("There is no stage with id: " + id);
        }
        
        public int GetSelectedStageId() {
            return selectedStageId;
        }
    }
}
