using System;
using Core;
using UI.Menu;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.RoomUI.StageSelect {
    public class StageSelectButton : Button, ISelectHandler , IPointerEnterHandler{
        [SerializeField] Transform selectBorder;
        [SerializeField] Transform choiceBorder;
        public event Action<int> OnStageSelected;
        public int stageId;

        public Transform SelectBorder => selectBorder;

        protected override void Awake() {
            base.Awake();
            selectBorder.gameObject.SetActive(false);
            choiceBorder.gameObject.SetActive(false);
            onClick.AddListener(SelectStage);
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            onClick.RemoveListener(SelectStage);
        }

        public void SelectStage() {
            OnStageSelected?.Invoke(stageId);
        }
        
        public override void OnPointerEnter(PointerEventData eventData){
            base.OnPointerEnter(eventData);
            choiceBorder.gameObject.SetActive(true);
        }

        public override void OnSelect(BaseEventData eventData){
            base.OnSelect(eventData);
            choiceBorder.gameObject.SetActive(true);
        }
        
        public override void OnDeselect(BaseEventData eventData){
            base.OnDeselect(eventData);
            choiceBorder.gameObject.SetActive(false);
        }
        
        public override void OnPointerExit(PointerEventData eventData){
            base.OnPointerExit(eventData);
            choiceBorder.gameObject.SetActive(false);
        }
    }
}
