﻿namespace UI.Shared {
    public interface IMechGameMenu {
        public MenuType GetMenuType();
        public void Open();
        public void Hide();
    }
}