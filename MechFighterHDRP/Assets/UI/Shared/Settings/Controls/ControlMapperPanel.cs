using Rewired.UI.ControlMapper;
using UI.Shared;
using UnityEngine;

namespace UI {
    public class ControlMapperPanel : MonoBehaviour, IMechGameMenu {
        [SerializeField] ControlMapper controlMapper;

        public ControlMapper ControlMapper => controlMapper;
        
        public MenuType GetMenuType() {
            return MenuType.ControlMapper;
        }

        public void Open() {
            gameObject.SetActive(true);
            controlMapper.Open();
        }

        public void Hide() {
            controlMapper.Close(true);
            gameObject.SetActive(false);
        }
        
    }
}
