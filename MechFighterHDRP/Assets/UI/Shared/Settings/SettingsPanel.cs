﻿using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class SettingsPanel: MonoBehaviour, IMechGameMenu {
        [SerializeField] Button goBackButton;
        [SerializeField] Button graphicSettingsButton;
        [SerializeField] Button soundSettingsButton;
        [SerializeField] Button controlSettingsButton;

        void Awake() {
            goBackButton.onClick.AddListener(GoBack);
            controlSettingsButton.onClick.AddListener(OnControlSettings);
            soundSettingsButton.onClick.AddListener(OpenSoundSettings);
        }

        void OnDestroy() {
            goBackButton.onClick.RemoveListener(GoBack);
            controlSettingsButton.onClick.RemoveListener(OnControlSettings);
            soundSettingsButton.onClick.RemoveListener(OpenSoundSettings);
        }

        void GoBack() {
            MenuManager.CloseCurrentMenu();
        }

        void OpenSoundSettings() {
            MenuManager.OpenMenu(MenuType.Sound);
        }
        
        void OnControlSettings() {
            MenuManager.OpenMenu(MenuType.ControlMapper);
        }
        
        public MenuType GetMenuType() {
            return MenuType.Settings;
        }

        public void Open() {
            gameObject.SetActive(true);
            goBackButton.Select();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}