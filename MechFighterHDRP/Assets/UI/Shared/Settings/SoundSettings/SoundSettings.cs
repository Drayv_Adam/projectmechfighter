using Audio;
using UI;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettings : MonoBehaviour, IMechGameMenu {

    [SerializeField] Slider masterSlider;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider sfxSlider;
    [SerializeField] Slider ambienceSlider;
    
    [SerializeField] Button goBackButton;

    void Awake() {
        goBackButton.onClick.AddListener(HandleGoBack);
        masterSlider.onValueChanged.AddListener(ChangeMasterVolume);
        musicSlider.onValueChanged.AddListener(ChangeMusicVolume);
        sfxSlider.onValueChanged.AddListener(ChangeSfxVolume);
        ambienceSlider.onValueChanged.AddListener(ChangeAmbientVolume);
    }

    void OnDestroy() {
        goBackButton.onClick.RemoveListener(HandleGoBack);
        masterSlider.onValueChanged.RemoveListener(ChangeMasterVolume);
        musicSlider.onValueChanged.RemoveListener(ChangeMusicVolume);
        sfxSlider.onValueChanged.RemoveListener(ChangeSfxVolume);
        ambienceSlider.onValueChanged.RemoveListener(ChangeAmbientVolume);
    }

    void HandleGoBack() {
        AudioManager.SaveSettings();
        MenuManager.CloseCurrentMenu();
    }


    void ChangeMasterVolume(float value) {
        AudioManager.SetMasterVolume(value);
    }
    
    void ChangeMusicVolume(float value) {
        AudioManager.SetMusicVolume(value);
    }
    
    void ChangeSfxVolume(float value) {
        AudioManager.SetSFXVolume(value);
    }
    
    void ChangeAmbientVolume(float value) {
        AudioManager.SetAmbientVolume(value);
    }
    
    public MenuType GetMenuType() {
        return MenuType.Sound;
    }

    void MoveSlidersToRealPosition() {
        masterSlider.value = AudioManager.masterVolume;
        musicSlider.value = AudioManager.musicVolume;
        sfxSlider.value = AudioManager.sfxVolume;
        ambienceSlider.value = AudioManager.ambientVolume;
    }
    
    public void Open() {
        MoveSlidersToRealPosition();
        
        gameObject.SetActive(true);
        goBackButton.Select();
    }

    public void Hide() {
        gameObject.SetActive(false);
    }
}
