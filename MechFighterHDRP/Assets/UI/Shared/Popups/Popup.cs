using System;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popups {
    public class Popup : MonoBehaviour {
        const string INFO = "i";
        const string WARNING = "!";
        const string ERROR = "x";
        
        const float HEIGHT = 100;
        const float MARGIN_BOTTOM = 5;
        
        [SerializeField] Text textField;
        [SerializeField] Text textIcon;
        [SerializeField] Image iconBackground;
        RectTransform rectTransform;
        public PopupManager.PopupPosition position;
        public bool active;
        public float duration;

        public ColorTheme.PopupIconColors Colors => ColorTheme.instance.popupIconColors;

        //todo: add animations (fade in, fade out, move up when a new popup appears when another is still there)

        void Awake() {
            rectTransform = GetComponent<RectTransform>();
        }

        public void Setup(PopupManager.PopupParams popupParams) {
            if (!active)
                gameObject.SetActive(true);
            SetText(popupParams.text);
            duration = popupParams.duration;
            AdjustToPosition(popupParams.position);
            SetIcon(popupParams.type);
            active = true;
        }

        void Update() {
            if (active) {
                duration -= Time.deltaTime;
                if (duration < 0) 
                    HidePopup();
            }
        }

        public void AdjustToPosition(PopupManager.PopupPosition position) {
            this.position = position;
            rectTransform.anchoredPosition = Vector2.zero;
            //todo: change allignments
        }
        
        public void SetText(string text) {
            textField.text = text;
        }

        void SetIcon(PopupManager.PopupType type) {
            switch (type) {
                case PopupManager.PopupType.Info:
                    textIcon.text = INFO;
                    iconBackground.color = Colors.info;
                    break;
                case PopupManager.PopupType.Warning:
                    textIcon.text = WARNING;
                    iconBackground.color = Colors.warning;
                    break;
                case PopupManager.PopupType.Error:
                    textIcon.text = ERROR;
                    iconBackground.color = Colors.error;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
        
        void HidePopup() {
            duration = 0;
            active = false;
            rectTransform.anchoredPosition = Vector2.zero;
            PopupManager.MoveToInactive(this);
            gameObject.SetActive(false);
        }

        public void PushDown() {
            rectTransform.anchoredPosition -= new Vector2(0, HEIGHT + MARGIN_BOTTOM);
        }
        
        public void PushUp() {
            rectTransform.anchoredPosition += new Vector2(0, HEIGHT + MARGIN_BOTTOM);
        }
    }
}
