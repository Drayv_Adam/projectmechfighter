using System.Collections;
using System.Collections.Generic;
using UI;
using UI.Popups;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

public class CreditsMenuPanel : MonoBehaviour, IMechGameMenu {

        [SerializeField] Button goBackButton;

        void Awake() {
            goBackButton.onClick.AddListener(GoBack);
        }

        void OnDestroy() {
            goBackButton.onClick.RemoveListener(GoBack);
        }

        void GoBack() {
            MenuManager.CloseCurrentMenu();
        }

        public MenuType GetMenuType() {
            return MenuType.Credits;
        }

        public void Open() {
            gameObject.SetActive(true);
            goBackButton.Select();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
}
