using Core.Connection;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class PlayerSetupPanel : MonoBehaviour {
        [SerializeField] Button playerReadyButton;
        [SerializeField] Transform playerReadyBackground;
        [SerializeField] Toggle swordsChoice;
        [SerializeField] Toggle gunChoice;

        bool playerReady;

        public int PlayerId { get; set; }
        public bool IsPlayer { get; set; }
        public bool IsReady => playerReady;

        public LocalVersusPanel versusPanel;

        PlayerLoadoutData currentLoadoutData;

        void Awake() {
            swordsChoice.onValueChanged.AddListener(SetSwordsMoveset);
            gunChoice.onValueChanged.AddListener(SetGunMoveset);
            playerReadyButton.onClick.AddListener(HighlightReadyBackground);
        }


        void OnDestroy() {
            swordsChoice.onValueChanged.RemoveListener(SetSwordsMoveset);
            gunChoice.onValueChanged.RemoveListener(SetGunMoveset);
            playerReadyButton.onClick.RemoveListener(HighlightReadyBackground);
        }

        public void Setup(LocalVersusPanel localVersusPanel, int playerId,
            PlayerLoadoutData initialLoadoutData = null) {
            currentLoadoutData = initialLoadoutData ?? new PlayerLoadoutData();
            PlayerId = playerId;
            versusPanel = localVersusPanel;
            playerReady = false;
            playerReadyBackground.gameObject.SetActive(playerReady);
        }

        void SetSwordsMoveset(bool isChecked) {
            if (isChecked) {
                currentLoadoutData.mechId = 0;
                currentLoadoutData.movesetType = 0;
                HUDManager.Instance.SetPlayerThumbnail(PlayerId, 0);
                UpdatePlayerData();
            }
        }

        void SetGunMoveset(bool isChecked) {
            if (isChecked) {
                currentLoadoutData.mechId = 1;
                currentLoadoutData.movesetType = 1;
                HUDManager.Instance.SetPlayerThumbnail(PlayerId, 1);
                UpdatePlayerData();
            }
        }


        void HighlightReadyBackground() {
            playerReady = !playerReady;
            versusPanel.OnReadyChanged();
            playerReadyBackground.gameObject.SetActive(playerReady);
        }

        void UpdatePlayerData() {
            versusPanel.UpdateLoadout(PlayerId, currentLoadoutData);
        }
    }
}