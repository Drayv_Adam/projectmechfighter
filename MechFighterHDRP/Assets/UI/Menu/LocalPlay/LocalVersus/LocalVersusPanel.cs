using System.Collections.Generic;
using Core;
using Core.Connection;
using UI.Popups;
using UI.RoomUI.StageSelect;
using UI.Shared;
using UI.Visuals;
using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class LocalVersusPanel : MonoBehaviour, IMechGameMenu{
        [SerializeField] Button startGame;
        [SerializeField] Button editControlsButton;
        [SerializeField] Button goBackButton;
        [SerializeField] StageSelectPanel stageSelectPanel;
        [SerializeField] List<PlayerSetupPanel> playerSetups = new List<PlayerSetupPanel>();
        [SerializeField] byte stageId;
        [SerializeField] PlayerLoadoutData[] playerLoadouts = new PlayerLoadoutData[2];
        [SerializeField] Text player2Text;
        [SerializeField] Text localVersusText;

        bool allPlayersReady;

        void Awake() {
            stageSelectPanel.OnMapChange += OnMapChanged;
            startGame.onClick.AddListener(StartGame);
            editControlsButton.onClick.AddListener(OpenControlSettings);
            goBackButton.onClick.AddListener(GoBack);

            InitializeStartGameData();

            for (var i = 0; i < playerSetups.Count; i++){
                playerSetups[i].Setup(this, i, playerLoadouts[i]);
            }

            OnReadyChanged();
        }

        void OnDestroy() {
            stageSelectPanel.OnMapChange -= OnMapChanged;
            startGame.onClick.RemoveListener(StartGame);
            editControlsButton.onClick.RemoveListener(OpenControlSettings);
            goBackButton.onClick.RemoveListener(GoBack);
        }

        public void OnReadyChanged() {
            bool allReady = true;
            foreach (var playerSetup in playerSetups){
                allReady &= playerSetup.IsReady;
            }

            allPlayersReady = allReady;
            startGame.interactable = allReady;
        }


        public void OnMapChanged(byte map) {
            BackgroundChanger.SetMapBackground(map);
            stageId = map;
        }


        #region MenuOverloads

        public MenuType GetMenuType() {
            return MenuType.LocalMode;
        }

        public void Open() {
            gameObject.SetActive(true);
            CheckForTrainingMode();
            var randomMap = Random.Range(0, BackgroundChanger.MAP_COUNT);
            stageSelectPanel.StageButtons[randomMap].SelectStage();
            for (var i = 0; i < playerSetups.Count; i++){
                playerSetups[i].Setup(this, i, playerLoadouts[i]);
            }
            goBackButton.Select();
            OnReadyChanged();
        }

        public void Hide() {
            gameObject.SetActive(false);
            BackgroundChanger.SetMenuBackground(0);
        }

        #endregion

        #region public methods

        public void UpdateLoadout(int playerId, PlayerLoadoutData playerLoadoutData) {
            if (playerId < playerLoadouts.Length){
                playerLoadouts[playerId] = playerLoadoutData;
                return;
            }

            Debug.LogError($"Error setting loadout for player {playerId}");
        }

        #endregion

        #region private Methods

        void StartGame() {
            if (!allPlayersReady){
                PopupManager.ShowPopup("Not all players ready");
                return;
            }
            MechGameRunner.Instance.StartLocalGame(GetStartGameData(), GetCPUFlags());
        }

        void OpenControlSettings() {
            MenuManager.OpenMenu(MenuType.ControlMapper);
        }

        void GoBack() {
            MenuManager.CloseCurrentMenu();
        }

        void CheckForTrainingMode() {
            player2Text.text = MenuManager.isPracticeModeSelected ? "Training Dummy" : "Player 2";
            localVersusText.text = MenuManager.isPracticeModeSelected ? "Practice Mode" : "Local Versus";
            playerSetups[0].IsPlayer = true;
            playerSetups[1].IsPlayer = !MenuManager.isPracticeModeSelected;
                

        }

        #endregion

        #region Helpers

        StartGameData GetStartGameData() => new StartGameData() {
            playerInfos = GetPlayerInfos(),
            stageId = stageId,
        };

        PlayerInfo[] GetPlayerInfos() {
            var playerInfos = new PlayerInfo[playerSetups.Count];
            for (int i = 0; i < playerInfos.Length; i++){
                playerInfos[i].playerName = $"Player {(i + 1)}";
                if (i < playerLoadouts.Length){
                    playerInfos[i].playerLoadoutData = playerLoadouts[i];
                    continue;
                }

                Debug.LogError($"Index out of range. Missing playerLoadoutData for player {i}");
                playerInfos[i].playerLoadoutData = new PlayerLoadoutData();
            }

            return playerInfos;
        }


        void InitializeStartGameData() {
            if (playerLoadouts == null || playerLoadouts.Length != playerSetups.Count)
                playerLoadouts = new PlayerLoadoutData[playerSetups.Count];
        }

        bool[] GetCPUFlags() {
            var cpuFlags = new bool[playerSetups.Count];
            for (var i = 0; i < playerSetups.Count; i++){
                cpuFlags[i] = !playerSetups[i].IsPlayer;
            }
            return cpuFlags;
        }

        #endregion
    }
}