﻿using UI.Popups;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class LocalModePanel : MonoBehaviour, IMechGameMenu {

        [SerializeField] Button campaignButton;
        [SerializeField] Button versusButton;
        [SerializeField] Button trainingModeButton;
        [SerializeField] Button editControlsButton;
        [SerializeField] Button goBackButton;

        void Awake() {
            //campaignButton.onClick.AddListener(GoToCampaignMode);
            versusButton.onClick.AddListener(GoToVersusMode);
            trainingModeButton.onClick.AddListener(GoToTrainingMode);
            editControlsButton.onClick.AddListener(OpenControlSettings);
            goBackButton.onClick.AddListener(GoBack);
        }

        void OnDestroy() {
            //campaignButton.onClick.RemoveListener(GoToCampaignMode);
            versusButton.onClick.RemoveListener(GoToVersusMode);
            trainingModeButton.onClick.RemoveListener(GoToTrainingMode);
            editControlsButton.onClick.RemoveListener(OpenControlSettings);
            goBackButton.onClick.RemoveListener(GoBack);
        }

        void GoToCampaignMode() {
            PopupManager.ShowPopup("Not implemented");
        }

        void GoToVersusMode() {
            MenuManager.isPracticeModeSelected = false;
            MenuManager.OpenMenu(MenuType.LocalVersus);
        }

        void GoToTrainingMode() {
            MenuManager.isPracticeModeSelected = true;
            MenuManager.OpenMenu(MenuType.LocalVersus);
        }
        
        void OpenControlSettings() {
            MenuManager.OpenMenu(MenuType.ControlMapper);
        }
        
        void GoBack() {
            MenuManager.CloseCurrentMenu();
        }

        public MenuType GetMenuType() {
            return MenuType.LocalMode;
        }

        public void Open() {
            gameObject.SetActive(true);
            versusButton.Select();
        }

        public void Hide() {
            gameObject.SetActive(false);
            
        }
    }
}