using Connection;
using UI.Popups;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI.OnlineMenu {
    public class OnlineModePanel : MonoBehaviour, IMechGameMenu {
        [SerializeField] Button quickPlayButton;
        [SerializeField] Button onlineLobbyButton;
        [SerializeField] Button editProfileButton;
        [SerializeField] Button goBackButton;

        void Awake() {
            quickPlayButton.onClick.AddListener(HandleQuickPlay);
            onlineLobbyButton.onClick.AddListener(HandleOnlineLobby);
            editProfileButton.onClick.AddListener(HandleEditProfile);
            goBackButton.onClick.AddListener(HandleGoBack);
        }

        void OnDestroy() {
            quickPlayButton.onClick.RemoveListener(HandleQuickPlay);
            onlineLobbyButton.onClick.RemoveListener(HandleOnlineLobby);
            editProfileButton.onClick.RemoveListener(HandleEditProfile);
            goBackButton.onClick.RemoveListener(HandleGoBack);
        }

        void HandleQuickPlay() {
            ServerConnectionManager.Instance.JoinRandomRoom();
        }

        void HandleOnlineLobby() {
            PopupManager.ShowPopup("Browsing Lobbies not supported. ");
        }
        
        void HandleGoBack() {
            ServerConnectionManager.Instance.Disconnect();
            MenuManager.CloseCurrentMenu();
        }

        void HandleEditProfile() {
            MenuManager.Instance.OpenProfileScreen();
        }

        public MenuType GetMenuType() {
            return MenuType.OnlineMode;
        }

        public void Open() {
            gameObject.SetActive(true); 
            quickPlayButton.Select();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
