using System.Collections.Generic;
using SharedGame;
using UnityEngine;
using UnityEngine.UI;
using UnityGGPO;

namespace UI {
    public class SimpleConnectionMenu : MonoBehaviour {
        //[SerializeField] PlayerListItem playerListItemPrefab;
        [SerializeField] InputField localPortField;
        [SerializeField] InputField localDelayField;
        [SerializeField] Button playLocalButton;
        [SerializeField] Button connectButton;
        [SerializeField] Button spectateButton;
        [SerializeField] Button backButton;
        [SerializeField] Button controlSettingsButton;
        [SerializeField] Transform playerListContainer;
        [SerializeField] Canvas controlSettingsPage;
        
        GameManager GameManager => GameManager.Instance;
        [SerializeField] GgpoPerformancePanel performancePanel;

        //List<PlayerListItem> playerListItems;
        
        void Awake() {
            connectButton.onClick.AddListener(Connect);
            playLocalButton.onClick.AddListener(StartLocal);
            backButton.onClick.AddListener(GoBack);
            spectateButton.onClick.AddListener(JoinSpectate);
            controlSettingsButton.onClick.AddListener(OpenControlSettings);
           // playerListItems = new List<PlayerListItem>();
            
            if (performancePanel != null)
                performancePanel.Setup();

            localPortField.text = "7000";
            localDelayField.text = "2";
            
            var connections = new List<Connections>();
            connections.Add(new Connections() {
                ip = "127.0.0.1",
                port = 7000,
                spectator = false,
                local = true,
                cpu = false
            });
            connections.Add(new Connections() {
                ip = "127.0.0.1",
                port = 7001,
                spectator = false,
               local = true,
                cpu = false
            });
            LoadConnectionInfo(connections);
        }

        void OnDestroy() {
            connectButton.onClick.RemoveListener(Connect);
            playLocalButton.onClick.RemoveListener(StartLocal);
            backButton.onClick.RemoveListener(GoBack);
            spectateButton.onClick.RemoveListener(JoinSpectate);
            controlSettingsButton.onClick.RemoveListener(OpenControlSettings);
        }

        void LoadConnectionInfo(List<Connections> connections) {
            DestroyPlayerListItems();
            for (var i = 0; i < connections.Count; i++) {
                var connection = connections[i];
                //PlayerListItem item = Instantiate(playerListItemPrefab, playerListContainer);
                //item.transform.localPosition = item.transform.localPosition + new Vector3(0, -i * 30);
                //item.Setup(connection);
                //playerListItems.Add(item);
            }
        }

        void DestroyPlayerListItems() {
            //foreach (var playerListItem in playerListItems) {
            //    Destroy(playerListItem.gameObject);
            //}
            //playerListItems.Clear();
        }

        List<Connections> GetConnections() {
            var list = new List<Connections>();
            //foreach (var playerListItem in playerListItems) {
            //    list.Add(playerListItem.GetConnection());
            //}
            return list;
        }

        void Connect() {
            GameManager.StartGGPOGame(performancePanel, GetLocalPort(), GetConnections(), false, GetFrameDelay()); //todo: add player profiles and scene ID to load
        }

        void JoinSpectate() {
            GameManager.StartGGPOGame(performancePanel, GetLocalPort(), GetConnections(), true, GetFrameDelay());
        }

        void StartLocal() {
            GameManager.StartLocalGame(new [] {false, false});
        }
        
        int GetFrameDelay() {
            if (localDelayField != null && int.TryParse(localDelayField.text, out var localDelay))
                return localDelay;
            return 2;
        }

        int GetLocalPort() {
            if (localPortField != null && int.TryParse(localPortField.text, out var port))
                return port;
            return 7000;
        }

        void OpenControlSettings() {
            MenuManager.Instance.OpenControlMapper();
        }
        
        void GoBack() {
            Application.Quit();
        }
    }
}
