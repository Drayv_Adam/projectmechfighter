using UnityEngine;
using UnityEngine.UI;

namespace UI.RoomUI.VSPanel {
    public class PlayerButton : MonoBehaviour {
        [SerializeField] Text playerNameField;
        [SerializeField] Text playerInfoField;

        
        public void Setup(string playerName, string info) {
            playerNameField.text = playerName;
            playerInfoField.text = info;
        }

        public void SetHidden(bool hidden) {
            playerInfoField.gameObject.SetActive(!hidden);
            playerInfoField.gameObject.SetActive(!hidden);
        }
    }
}
