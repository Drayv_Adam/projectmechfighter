using Core.Connection;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RoomUI.VSPanel {
    public class PlayerSlotView : MonoBehaviour {
        const string EMPTY_PLAYER_NAME = "";
        
        public byte id;
        [SerializeField] Image characterImage;
        [SerializeField] Button mainButton;
        [SerializeField] GameObject inactiveImage;
        [SerializeField] GameObject statsContainer;
        [SerializeField] GameObject[] readyOverlays;
        [SerializeField] Text playerNameField;
        [SerializeField] Text playerStatsField;
        [SerializeField] GameObject[] meIndicators;

        public bool free;

        OnlineRoomManager OnlineRoomManager => OnlineRoomManager.Instance;
        
        void OnEnable() {
            mainButton.onClick.AddListener(MainButtonClick);
        }

        void OnDisable() {
            mainButton.onClick.RemoveListener(MainButtonClick);
        }

        public void Initialize(byte slotId) {
            id = slotId;
        }
        
        public void SetupEmpty() {
            playerNameField.text = EMPTY_PLAYER_NAME;
            free = true;
            mainButton.interactable = true;
            characterImage.gameObject.SetActive(false);
            inactiveImage.SetActive(true);
            statsContainer.SetActive(false);
            SetReady(false);
            SetMeIndicators(false);
        }

        public void UpdateData(PlayerJoinedData data, bool isMe) {
            //todo: do something with the image
            free = false;
            mainButton.interactable = false;
            playerNameField.text = data.playerName;
            string info = "";
            if (data.playerLoadout != null) {
                info = "mech id:" + data.playerLoadout.mechId + "\n movesetType:" + data.playerLoadout.movesetType;
            }
            playerStatsField.text = info;
            characterImage.gameObject.SetActive(true);
            inactiveImage.SetActive(false);
            statsContainer.SetActive(true);
            SetReady(data.isReady);
            SetMeIndicators(isMe);
        }

        public void SetMeIndicators(bool isMe) {
            for (int i = 0; i < meIndicators.Length; i++) {
                meIndicators[i].SetActive(isMe);
            }
        }
        
        public void SetReady(bool ready) {
            for (int i = 0; i < readyOverlays.Length; i++) {
                readyOverlays[i].SetActive(ready);
            }
        }
        
        void MainButtonClick() {
            if (!free)
                return;
            OnlineRoomManager.TryTakeSlot(id);
        }
    }
}
