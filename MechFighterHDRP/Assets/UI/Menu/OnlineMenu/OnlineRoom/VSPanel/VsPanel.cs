using Core.Connection;
using UI.Popups;
using UI.RoomUI.StageSelect;
using UI.RoomUI.VSPanel;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class VsPanel : MonoBehaviour {
        [SerializeField] PlayerSlotView[] playerSlots;
        [SerializeField] StageSelectPanel stageSelectPanel;
        
        [SerializeField] Button readyButton;
        [SerializeField] Text readyButtonText;
        [SerializeField] Button editLoadoutButton;
        [SerializeField] GameObject readyBtBorder;
        
        OnlineRoomManager OnlineRoomManager => OnlineRoomManager.Instance;
        
        void Awake() {
            readyButton.onClick.AddListener(ReadyClicked);
            editLoadoutButton.onClick.AddListener(OpenEditLoadout);
            for (byte i = 0; i < playerSlots.Length; i++) {
                playerSlots[i].Initialize(i);
            }
        }

        void OnDestroy() {
            readyButton.onClick.RemoveListener(ReadyClicked);
            editLoadoutButton.onClick.RemoveListener(OpenEditLoadout);
        }

        public void Initialize() {
            foreach (var playerSlot in playerSlots) {
                playerSlot.SetupEmpty();
            }
            stageSelectPanel.gameObject.SetActive(false);
            ChangeReadyButtonColor(OnlineRoomManager.IsReady);
        }

        public void UpdateSlot(int index, PlayerJoinedData playerJoinedData, bool isMe) {
            if (index >= playerSlots.Length) {
                PopupManager.ShowError("[EnterSlot] Bad player index");
                return;
            }
            playerSlots[index].UpdateData(playerJoinedData, isMe);
        }

        public void LeaveSlot(int index) {
            if (index >= playerSlots.Length) {
                PopupManager.ShowError("[LeaveSlot] Bad player index");
                return;
            }
            playerSlots[index].SetupEmpty();
        }
        
        public void EnableStageSelect(bool isHost) {
            stageSelectPanel.gameObject.SetActive(isHost);
        }

        void OpenEditLoadout() {
            PopupManager.ShowPopup("Enter edit loadout");
            MenuManager.OpenMenu(MenuType.Profile);
        }

        void ReadyClicked() {
            OnlineRoomManager.TrySetReady();
            ChangeReadyButtonColor(OnlineRoomManager.IsReady);
        }

        void ChangeReadyButtonColor(bool isReady) {
            var colors = readyButton.colors;
            var buttonColors = ColorTheme.instance.buttonColors;
            var color = buttonColors.GetReadyButtonColor(isReady);
            colors.normalColor = color;
            colors.selectedColor = color;
            colors.pressedColor = color;
            readyButton.colors = colors;
            readyButtonText.color = buttonColors.GetReadyButtonTextColor(isReady);
            readyBtBorder.SetActive(isReady);
        }
    }
}
