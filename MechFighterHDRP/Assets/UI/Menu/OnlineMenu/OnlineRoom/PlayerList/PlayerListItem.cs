using System;
using System.Collections.Generic;
using Core.Connection;
using MechFighter;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class PlayerListItem : MonoBehaviour {
        public event Action OnItemClicked;
        
        [SerializeField] Image background;
        [SerializeField] Image icon;
        [SerializeField] Text playerNameDisplay;
        [SerializeField] Text pingDisplay;
        [SerializeField] Text spectatorDisplay;
        [SerializeField] Button button;

        public PlayerDisplayData data;
        
        public int Height => (int) background.rectTransform.rect.height;

        RectTransform rectTransform;
        public RectTransform RectTransform {
            get {
                if (rectTransform == null)
                    rectTransform = GetComponent<RectTransform>();
                return rectTransform;
            }
        }
        
        ColorTheme.PlayerListColors Colors => ColorTheme.PlayerList;
        
        void Awake() {
            data = new PlayerDisplayData();
            button.onClick.AddListener(HandlePlayerClicked);
        }

        void OnDestroy() {
            button.onClick.RemoveListener(HandlePlayerClicked);
        }

        void HandlePlayerClicked() {
            Debug.Log($"{data.playerName} clicked!");
            OnItemClicked?.Invoke();
        }

        public void Setup(PlayerDisplayData displayData) {
            data = displayData;
            UpdateAll();
        }

        void UpdateAll() {
            pingDisplay.gameObject.SetActive(data.showingPing);
            playerNameDisplay.text = data.playerName;
            UpdateSpectator(data.isSpectator);
            UpdatePing(data.ping);
            UpdateColors();
        }

        void UpdateColors() {
            if (ColorTheme.instance == null) return;
            background.color = data.isHost ? Colors.hostBackground : Colors.defaultPlayerBackground;
            icon.color = data.isSpectator ? Colors.spectatorIconColor : Colors.playerIconColor;
        }
        
        public void UpdatePing(ushort newPing) {
            data.ping = newPing;
            pingDisplay.text = $"{data.ping} ms";
            pingDisplay.color = (data.ping > MechGameConstants.HIGH_PING_THRESHOLD) ? Color.red : ColorTheme.DefaultFont;
        }

        public void UpdateHost(bool isHost) {
            data.isHost = isHost;
            UpdateColors();
        }
        
        public void UpdateSpectator(bool spectator) {
            data.isSpectator = spectator;
            spectatorDisplay.text = data.isSpectator ? "S" : "P";
            UpdateColors();
        }
    }
    
    public struct PlayerDisplayData {
        public string playerName;
        public ushort ping;
        public bool isHost;
        public bool isSpectator;
        public bool showingPing;

        public PlayerDisplayData(string playerName, bool isSpectator, bool isHost) {
            this.playerName = playerName;
            this.isSpectator = isSpectator;
            this.isHost = isHost;
            ping = 0;
            showingPing = false;
        }
    }
}
