using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class PlayerList : MonoBehaviour {
        [SerializeField] PlayerListItem playerListItemPrefab;
        [SerializeField] Transform contentContainer;
        [SerializeField] Text playerCountDisplay;

        [Header("Styles")] [SerializeField] int verticalMargin;
        
        Dictionary<int, PlayerListItem> playerDisplays;

        byte maxPlayers = 4;
        int playerCount;
        int verticalDisplacement = 30;

        void Awake() {
            playerDisplays = new Dictionary<int, PlayerListItem>();
            verticalDisplacement = playerListItemPrefab.Height + verticalMargin;
        }

        public void Setup(byte maxPlayerNum) {
            maxPlayers = maxPlayerNum;
        }

        public void AddPlayer(int connectionId, PlayerDisplayData data, bool shouldRearrangeList = true) {
            var player = Instantiate(playerListItemPrefab, contentContainer);
            player.Setup(data);
            playerDisplays.Add(connectionId, player);
            playerCount++;
            if (!shouldRearrangeList)
                return;
            UpdatePlayerCountDisplay();
            ReArrangeList();
        }

        public void RemovePlayer(int index) {
            var player = playerDisplays[index];
            Destroy(player); //todo: deactive and add to inactive list
            playerDisplays.Remove(index);
            playerCount--;
            UpdatePlayerCountDisplay();
            ReArrangeList();
        }

        public void ClearList() {
            for (int i = 0; i < playerCount; i++)
                RemovePlayer(i);
            playerDisplays.Clear();
            playerCount = 0;
        }
        
        public void UpdatePings(ushort[] pings) {
            if (pings.Length != playerCount) {
                Debug.LogError("new ping list count is not equal to player count!");
                return;
            }
            for (int i = 0; i < playerCount; i++)
                playerDisplays[i].UpdatePing(pings[i]);
        }

        public void UpdateListItem(int playerId, PlayerDisplayData data) {
            if (playerDisplays.ContainsKey(playerId))
                playerDisplays[playerId].Setup(data);
        }

        void ReArrangeList() {
            int i = 0;
            foreach (var playerDisplay in playerDisplays) {
                PlaceItemAtIndex(playerDisplay.Value, i++);
            }
        }

        void PlaceItemAtIndex(PlayerListItem item, int index) {
            item.RectTransform.anchoredPosition = new Vector2(0, -index * verticalDisplacement);
        }

        void UpdatePlayerCountDisplay() {
            playerCountDisplay.text = $" Room: {playerCount} / {maxPlayers}";
        }
    }
}
