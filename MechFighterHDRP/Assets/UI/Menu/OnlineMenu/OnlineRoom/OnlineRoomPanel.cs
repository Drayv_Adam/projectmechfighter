using Connection;
using Core.Connection;
using UI.Popups;
using UI.RoomUI.StageSelect;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RoomUI {
    public class OnlineRoomPanel : MonoBehaviour, IMechGameMenu {
        [SerializeField] PlayerList playerList;
        [SerializeField] VsPanel vsPanel;
        [SerializeField] StageSelectPanel stageSelectPanel;

        [SerializeField] Button goBackButton;
        [SerializeField] Button spectateButton;
        [SerializeField] Button editLoadoutButton;

        void Awake() {
            stageSelectPanel.OnMapChange += OnMapChanged;
            goBackButton.onClick.AddListener(OnGoBack);
            spectateButton.onClick.AddListener(EnterSpectate);
            editLoadoutButton.onClick.AddListener(EditLoadout);
        }

        void OnDestroy() {
            stageSelectPanel.OnMapChange -= OnMapChanged;
            goBackButton.onClick.RemoveListener(OnGoBack);
            spectateButton.onClick.RemoveListener(EnterSpectate);
            editLoadoutButton.onClick.RemoveListener(EditLoadout);
        }

        public void Initialize(byte maxPlayers) {
            vsPanel.Initialize();
            playerList.Setup(maxPlayers);
        }

        public void ClearPlayerList() {
            playerList.ClearList();
        }
        
        public void AddPlayer(int connectionId, PlayerDisplayData playerDisplayData) {
            playerList.AddPlayer(connectionId, playerDisplayData);
        }

        public void RemovePlayer(int index) {
            playerList.RemovePlayer(index);
        }

        public void EnterOrUpdateSlot(int index, PlayerJoinedData playerJoinedData, bool isMe) {
            vsPanel.UpdateSlot(index, playerJoinedData, isMe);
        }

        public void LeaveSlot(int index) {
            vsPanel.LeaveSlot(index);
        }

        public void UpdatePlayerInList(int connectionId, PlayerDisplayData playerDisplayData) {
            playerList.UpdateListItem(connectionId, playerDisplayData);

        }
        
        public void EnterSpectate() {
            OnlineRoomManager.Instance.EnterSpectate();
        }
        
        void EditLoadout() {
            PopupManager.ShowPopup("Enter edit loadout");
            MenuManager.OpenMenu(MenuType.Profile);
        }
        
        void OnGoBack() {
            ServerConnectionManager.Instance.LeaveRoom();
            MenuManager.CloseCurrentMenu();
        }
        
        public MenuType GetMenuType() {
            return MenuType.OnlineRoom;
        }

        public void Open() {
            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }


        public void UpdateHost(bool isHost) {
            vsPanel.EnableStageSelect(isHost);
        }

        void OnMapChanged(byte map){
            ServerConnectionManager.Instance.InformNewStage(map);
        }
    }
}
