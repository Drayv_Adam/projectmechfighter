using Connection;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Menu {
    public class MainMenuPanel : MonoBehaviour, IMechGameMenu {
        [SerializeField] Button localButton;
        [SerializeField] Button onlineModeButton;
        [SerializeField] Button settingsButton;
        [SerializeField] Button creditsButton;
        [SerializeField] Button quitButton;


        void Awake() {
            localButton.onClick.AddListener(OnLocal);
            onlineModeButton.onClick.AddListener(OnOnline);
            settingsButton.onClick.AddListener(OnSettings);
            creditsButton.onClick.AddListener(OnCredits);
            quitButton.onClick.AddListener(OnQuit);
        }

        void OnDestroy() {
            localButton.onClick.RemoveListener(OnLocal);
            onlineModeButton.onClick.RemoveListener(OnOnline);
            settingsButton.onClick.RemoveListener(OnSettings);
            quitButton.onClick.RemoveListener(OnQuit);
        }

        void OnLocal() {
            MenuManager.OpenMenu(MenuType.LocalMode);
        }

        void OnOnline() {
            MenuManager.OpenMenu(MenuType.OnlineMode);
        }

        void OnControlSettings() {
            MenuManager.OpenMenu(MenuType.ControlMapper);
        }

        void OnSettings() {
            MenuManager.OpenMenu(MenuType.Settings);
        }

        void OnCredits() {
            MenuManager.OpenMenu(MenuType.Credits);
        }

        void OnQuit() {
            MenuManager.Quit();
        }

        public MenuType GetMenuType() {
            return MenuType.MainMenu;
        }

        public void Open() {
            gameObject.SetActive(true);
            localButton.Select();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
