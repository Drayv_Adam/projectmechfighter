using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnnouncerManager : MonoBehaviour {
    public static AnnouncerManager Instance { get; private set; }
    [SerializeField] Image currentImage;
    [SerializeField] Sprite[] sprite;
    Coroutine hideCoroutine;

    public enum Announcement {
        Empty,
        Mission1,
        Mission2,
        Mission3,
        Mission4,
        Mission5,
        Mission6,
        FinalMission,
        Engage,
        Timeout,
        Time,
        Draw,
        TargetDown,
        Down,
        Knockdown,
        TieBreaker,
        Target,
    }

    void Awake() {
        Instance = this;
    }

    public void ChangeImageTo(Announcement announcement) {
        currentImage.sprite = sprite[(int) announcement];
    }

    public void DisplayMission(int mission) {
        switch (mission) {
            case 0:
                currentImage.sprite = sprite[(int) Announcement.Mission1];
                break;
            case 1:
                currentImage.sprite = sprite[(int) Announcement.Mission1];
                break;
            case 2:
                currentImage.sprite = sprite[(int) Announcement.Mission2];
                break;
            default:
                currentImage.sprite = sprite[(int) Announcement.FinalMission];
                break;
        }
    }

    public void DisplayEngage() {
        DisplayAnnouncerText(Announcement.Engage);
        if (gameObject.activeInHierarchy) {
            if (hideCoroutine != null)
                hideCoroutine = null;
            hideCoroutine = StartCoroutine(HideAnnouncer(1f));
        }
    }

    void OnEnable() {
        if (hideCoroutine != null)
            hideCoroutine = null;
        hideCoroutine = StartCoroutine(HideAnnouncer(1f));
    }

    IEnumerator HideAnnouncer(float hideTime) {
        yield return new WaitForSeconds(hideTime);
        currentImage.sprite = sprite[(int) Announcement.Empty];
    }


    public void DisplayAnnouncerText(Announcement announcement) {
        transform.SetSiblingIndex(1);
        ChangeImageTo(announcement);
    }
}