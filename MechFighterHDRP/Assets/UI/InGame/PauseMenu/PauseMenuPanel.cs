using System;
using System.Collections;
using Core;
using Core.Input;
using Core.Managers;
using GameViewAssets;
using SharedGame;
using UI.Popups;
using UI.Shared;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.PauseMenu {
    public class PauseMenuPanel : MonoBehaviour, IMechGameMenu {
        [SerializeField] Button resume;
        [SerializeField] Button changeControls;
        [SerializeField] Button soundSettingsButton;
        [SerializeField] Button quitMatch;
        Coroutine loadSceneCor;

        void Awake() {
            resume.onClick.AddListener(ResumeGame);
            changeControls.onClick.AddListener(OpenControlSettings);
            soundSettingsButton.onClick.AddListener(OpenSoundSettings);
            quitMatch.onClick.AddListener(QuitMatch);
        }

        void OnDestroy() {
            resume.onClick.RemoveListener(ResumeGame);
            changeControls.onClick.RemoveListener(OpenControlSettings);
            soundSettingsButton.onClick.RemoveListener(OpenSoundSettings);
            quitMatch.onClick.RemoveListener(QuitMatch);
        }

        #region MenuOverloads

        public MenuType GetMenuType() {
            return MenuType.Pause;
        }

        public void Open() {
            resume.Select();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            if (MechGame.IsGameLocal)
                Time.timeScale = 0;
            gameObject.SetActive(true);
            MechGame.IsGamePaused = true;
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        #endregion

        #region private Methods

        void ResumeGame() {
            MenuManager.CloseCurrentMenu();
            MenuManager.ResumeGame();
            Time.timeScale = 1;
        }

        void OpenSoundSettings() {
            MenuManager.OpenMenu(MenuType.Sound);
        }
        
        void QuitMatch() {
            MenuManager.CloseCurrentMenu();
            MenuManager.ReturnToMainMenu();
            GameManager.Instance.Shutdown();
            MechGameRunner.SetMenuCameraActive(true);
            HUDManager.Instance.ResetScore();
            for (int i = 0; i<2; i++) {
                MenuManager.playerVictoriesValue[i] = 0;
            }
            AnnouncerManager.Instance.DisplayAnnouncerText(AnnouncerManager.Announcement.Mission1); 
            Time.timeScale = 1;
        }

        void OpenControlSettings() {
            MenuManager.OpenMenu(MenuType.ControlMapper);
        }
        
        #endregion
    }
}