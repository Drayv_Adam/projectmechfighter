using Core;
using Core.Input;
using Core.Utility;
using GameViewAssets;
using SharedGame;
using UnityEngine;
using UnityEngine.UI;

namespace UI.InGame.HUD {
    public class MainHUD : MonoBehaviour {
        
        const float FADE_SPEED = 350;

        const float FADE_DURATION = 20;
        const float BLACK_SCREEN_DURATION = 20;
        const int TARGET_FOLLOW_SPEED = 60;
        
        static readonly Color FadeScreenNoOpacity = new Color(0, 0, 0, 0);
        static readonly Color FadeScreenFullOpacity = new Color(0, 0, 0, 1);
        
        [SerializeField] Slider[] healthBars;
        [SerializeField] Slider[] dashBars;
        [SerializeField] Slider[] superBars;
        [SerializeField] Image[] player1RoundWins;
        [SerializeField] Image[] player2RoundWins;
        [SerializeField] Image[] playerThumbnails;
        [SerializeField] Sprite[] thumbnailSprites;
        [SerializeField] LockOnTarget[] lockOnTargets;
        [SerializeField] Text timer;
        [SerializeField] Transform infinitySymbol;
        [SerializeField] Image fadeOutPanel;
        [SerializeField] Transform split;
        Image[][] roundWinsGraphics;
        
        int[] roundWins;
        bool screenFading;
        bool fadeScreenHidden;
        float fadeScreenProgress;
        
        MechView[] mechs => MechGameViewManager.mechViews;


        void Awake() {
            roundWinsGraphics = new Image[2][];
            roundWins = new int[2];
            roundWinsGraphics[0] = player1RoundWins;
            roundWinsGraphics[1] = player2RoundWins;
            GameModeManager.OnRoundEnd += HandleEndStepStarted;
            GameModeManager.OnCountDownStart += HandleCountDownStart;
            GameModeManager.OnAwaitStart += HandleAwaitStart;
            GameModeManager.OnRoundStart += AnnouncementEngage;
        }

        void OnDestroy() {
            GameModeManager.OnRoundEnd -= HandleEndStepStarted;
            GameModeManager.OnCountDownStart -= HandleCountDownStart;
            GameModeManager.OnAwaitStart -= HandleAwaitStart;
            GameModeManager.OnRoundStart -= AnnouncementEngage;
        }

        void Update() {
            UpdateHudBars();
            UpdateLockOnTargets();
            HandleFadingScreen();
            UpdateTimer();
        }

        void UpdateTimer() {
            var secondsLeft = MechGameRunner.MechGame.FramesLeft / 60;
            if (MechGameRunner.MechGame.gamePhase != Structs.GamePhase.Game)
                secondsLeft = GameModeManager.MATCH_DURATION / 60;
            timer.text = secondsLeft.ToString();
        }

        void UpdateLockOnTargets() {
            float distance = 0;
            if (mechs.Length >= 2)
                distance = Vector3.Distance(mechs[0].transform.position, mechs[1].transform.position);
            var cameras = MechGameViewManager.Instance.cameras;
            var rotationHelpers = RotationHelper.rotationHelpers;
            
            for (int i = 0; i < mechs.Length; i++) {
                if (mechs[i] == null || i >= 2 || i >= rotationHelpers.Length || rotationHelpers[i] == null) 
                    continue;
                bool lockedOn = RotationHelper.rotationHelpers[i].LockOn;
                lockOnTargets[i].gameObject.SetActive(lockedOn);
                lockOnTargets[i].fadingIn = lockedOn;
                
                if (!lockedOn || cameras.Length <= i || cameras[i] == null) 
                    continue;
                var screenPosition = cameras[i].WorldToScreenPoint(rotationHelpers[i].FollowTargetPosition);
                lockOnTargets[i].transform.position = Vector3.Lerp(
                    lockOnTargets[i].transform.position, 
                    screenPosition, 
                    TARGET_FOLLOW_SPEED * Time.deltaTime);
                        
                lockOnTargets[i].UpdateColors(TargetOutOfRange(i, distance));
            }
        }

        void UpdateHudBars() {
            for (int i = 0; i < mechs.Length; i++) {
                if (mechs[i] == null && i > 2)
                    continue;
                var currentState = mechs[i].CurrentState;
                healthBars[i].value = (float) currentState.health / currentState.parameters.health * 100f;
                dashBars[i].value = (float) currentState.dashMeter / currentState.parameters.dash * 100f;
                superBars[i].value = (float) currentState.specialMeter / currentState.parameters.super * 100f;
            }
        }
        
        public void ResetScore() {
            for (int i = 0; i < mechs.Length; i++) {
                if (mechs[i] != null && i < 2) {
                    mechs[i].CurrentState.score = 0;
                    roundWinsGraphics[i][0].enabled = false;
                    roundWinsGraphics[i][1].enabled = false;
                }
            }
        }
        
        bool TargetOutOfRange(int i, float distance) {
            if (GameManager.Instance == null || !GameManager.Instance.IsRunning || GameManager.Instance.Runner == null)
                return false;
            var game = (MechGame) GameManager.Instance.Runner.Game;
            if (!game.mechs[i].lockedOn || game.mechs[i].CurrentWeaponRange == 0)
                return false;
            return MechUtils.IntToFloat((int)game.mechs[i].CurrentWeaponRange) < distance;
        } 
        
        //warning! unused!
        //unused and ugly
        void HandleResourceRegen(int i, Mech currentState) {
            if (mechs[i].CurrentState.specialMeter < currentState.parameters.super) {
                mechs[i].CurrentState.specialMeter += 100;
            }

            if (mechs[i].CurrentState.specialMeter >= currentState.parameters.super) {
                mechs[i].CurrentState.specialMeter = (short) currentState.parameters.super;
            }

            if (mechs[i].CurrentState.health < currentState.parameters.health && mechs[i].CurrentState.inHitStun) {
                mechs[i].CurrentState.health += 4;
            }

            if (mechs[i].CurrentState.health >= currentState.parameters.health) {
                mechs[i].CurrentState.health = (short) currentState.parameters.health;
            }

            if (mechs[i].CurrentState.health <= 2000) {
                mechs[i].CurrentState.health = 2001;
            }
        }

        void UpdateScore() {
            Debug.Log("Attempting to update score");
            for (int i = 0; i < mechs.Length; i++) {
                if (mechs[i] != null && i < 2) {
                    roundWins[i] = mechs[i].CurrentState.score;
                    if (roundWins[i] <= 2 && roundWins[i] >= 1) 
                        roundWinsGraphics[i][roundWins[i] - 1].enabled = true;
                }
                if (roundWins[i] >= GameModeManager.MAX_ROUNDS) {
                    UpdateWinner(i);
                    return;
                }
            }
        }

        void UpdateWinner(int i) {
            MenuManager.playerVictoriesValue[i]++;
            MenuManager.lastWinner = i;
            for (int j = 1; j < 2; j++) {
                roundWins[j] = 0;
            }
        }

        void HandleFadingScreen() {
            if (fadeScreenHidden && !screenFading)
                return;

            if (GameManager.Instance == null || GameManager.Instance.Runner == null)
                return;
            
            var game = (MechGame) GameManager.Instance.Runner.Game;
            var remainingTime = game.FramesLeft;
            if (screenFading) {
                if (fadeScreenHidden) {
                    fadeOutPanel.gameObject.SetActive(true);
                    fadeScreenHidden = false;
                }
                if (remainingTime < FADE_DURATION + BLACK_SCREEN_DURATION) {
                    fadeScreenProgress += Time.deltaTime * 60 / FADE_DURATION;
                }
            } else {
                fadeScreenProgress -= Time.deltaTime * 60 / FADE_DURATION;
            }
            fadeOutPanel.color = Color.Lerp(FadeScreenNoOpacity, FadeScreenFullOpacity, fadeScreenProgress);
            
            if (fadeScreenProgress < 0) {
                fadeScreenHidden = true;
                fadeOutPanel.gameObject.SetActive(false);
            }
        }

        void HandleAwaitStart() {
            ForceFadeTransitionCompleted(false);
            screenFading = false;
        }
        
        void HandleEndStepStarted(Structs.RoundEndReason endReason, int winnerId) {
            UpdateScore();
            FadeScreen();
        }
        
        void HandleCountDownStart() {
            ForceFadeTransitionCompleted();
            var missions = GetRoundCount();
            AnnouncerManager.Instance.DisplayMission(missions + 1);
        }

        public void ForceFadeTransitionCompleted(bool fadeOutPanelHidden = true) {
            fadeOutPanel.gameObject.SetActive(!fadeOutPanelHidden);
            fadeOutPanel.color = fadeOutPanelHidden? FadeScreenNoOpacity : FadeScreenFullOpacity;
            fadeScreenHidden = fadeOutPanelHidden;
            fadeScreenProgress = fadeOutPanelHidden? 0 : 1;
        }
        
        void FadeScreen() {
            screenFading = true;
            fadeOutPanel.gameObject.SetActive(true);
            fadeScreenHidden = true;
        }

        void OnEnable() {
            split.gameObject.SetActive(!MenuManager.isPracticeModeSelected);
            timer.gameObject.SetActive(!MenuManager.isPracticeModeSelected);
            infinitySymbol.gameObject.SetActive(MenuManager.isPracticeModeSelected);
            ForceFadeTransitionCompleted(false);
        }

        int GetRoundCount() {
            var missions = 0;
            if (mechs != null && mechs.Length > 1) {
                for (int i = 0; i < mechs.Length; i++) {
                    if (mechs[i] != null && i < 2) {
                        missions += roundWins[i];
                    }
                }

                if (mechs[0].CurrentState.score == 0 && mechs[1].CurrentState.score == 0)
                    missions = 0;
            }

            return missions;
        }

        void AnnouncementEngage() {
            AnnouncerManager.Instance.DisplayEngage();
        }

        public void SetBlackOut() {
            fadeOutPanel.color = new Color(0, 0, 0, 1);
        }

        public void SetThumbnail(int id, int image) {
            playerThumbnails[id].sprite = thumbnailSprites[image];
        }
    }
}