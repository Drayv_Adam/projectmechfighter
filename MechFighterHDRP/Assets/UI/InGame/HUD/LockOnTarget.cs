using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI.InGame.HUD {
    public class LockOnTarget : MonoBehaviour {

        readonly static Color OutOfRangeColor = new Color(0.6f, 0.1f, 0.1f);
        
        [SerializeField] Transform bigCircle;
        [SerializeField] Transform mediumCircle;
        [SerializeField] Transform smallCircle;
        [SerializeField] Transform tinyCircle;
        [SerializeField] Image[] images;
        WaitForSeconds secondToAppear = new WaitForSeconds(0.3f);
        Color[] imageColors;
        
        Coroutine appearLockOnCoroutine;
        public bool fadingIn;

        void Awake() {
            imageColors = new Color[4];
            for (int i = 0; i < images.Length; i++) {
                imageColors[i] = images[i].color;
            }
        }

        void OnEnable() {
            for (int i = 0; i < images.Length; i++) {
                images[i].color = new Color(images[i].color.r, images[i].color.g, images[i].color.b, 0);
            }

            if (appearLockOnCoroutine != null) 
                StopCoroutine(appearLockOnCoroutine);
            appearLockOnCoroutine = StartCoroutine(AppearLockOn());
        }

        IEnumerator AppearLockOn() {
            yield return secondToAppear;
            for (int i = 0; i < images.Length; i++) {
                images[i].color = imageColors[i];
            }
        }

        void Update() {
            var bigCircleRotation = bigCircle.rotation.eulerAngles;
            bigCircleRotation += new Vector3(0, 0, 30) * (Time.deltaTime * 10);
            bigCircle.eulerAngles = bigCircleRotation;
            var mediumCircleRotation = mediumCircle.rotation.eulerAngles;
            mediumCircleRotation += new Vector3(0, 0, -40) * (Time.deltaTime * 10);
            mediumCircle.eulerAngles = mediumCircleRotation;
            var smallCircleRotation = smallCircle.rotation.eulerAngles;
            smallCircleRotation += new Vector3(0, 0, 50) * (Time.deltaTime * 10);
            smallCircle.eulerAngles = smallCircleRotation;
            var tinyCircleRotation = tinyCircle.rotation.eulerAngles;
            tinyCircleRotation += new Vector3(0, 0, 60) * (Time.deltaTime * 10);
            tinyCircle.eulerAngles = tinyCircleRotation;
        }


        public void UpdateColors(bool outOfRange) {
            if (imageColors == null || imageColors.Length == 0)
                return;
            for (int i = 0; i < images.Length; i++) {
                var alpha = images[i].color.a;
                var color = OutOfRangeColor;
                color.a = alpha;
                images[i].color = outOfRange ? color : imageColors[i];
            }
        }
        
        void OnDisable() {
            if (appearLockOnCoroutine != null) 
                StopCoroutine(appearLockOnCoroutine);
        }
        
    }
}
