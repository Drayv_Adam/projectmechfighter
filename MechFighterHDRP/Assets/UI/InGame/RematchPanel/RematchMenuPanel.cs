
using Core;
using Core.Managers;
using SharedGame;
using UI;
using UI.Shared;
using UnityEngine;
using UnityEngine.UI;

public class RematchMenuPanel : MonoBehaviour, IMechGameMenu {
    [SerializeField] Button rematchButton;
    [SerializeField] Button quitButton;
    [SerializeField] Text victoryText;
    [SerializeField] Text[] playerVictories;

    void Awake() {
        rematchButton.onClick.AddListener(Rematch);
        quitButton.onClick.AddListener(Quit);
    }

    void OnDestroy() {
        rematchButton.onClick.RemoveListener(Rematch);
        quitButton.onClick.RemoveListener(Quit);
    }
    
    public MenuType GetMenuType() {
        return MenuType.Rematch;
    }

    public void Open() {
        rematchButton.Select();
        UpdatePlayerVictories();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (MechGame.IsGameLocal)
            Time.timeScale = 0;
        gameObject.SetActive(true);
        MechGame.IsGamePaused = true;
    }

    public void Hide() {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        gameObject.SetActive(false);
        MechGame.IsGamePaused = false;
    }

    void Quit() {
        MenuManager.CloseCurrentMenu();
        MenuManager.ReturnToMainMenu();
        GameManager.Instance.Shutdown();
        MechGameRunner.SetMenuCameraActive(true);
        HUDManager.Instance.ResetScore();
        for (int i = 0; i<2; i++) {
            MenuManager.playerVictoriesValue[i] = 0;
            playerVictories[i].text = MenuManager.playerVictoriesValue[i].ToString();
        }
        AnnouncerManager.Instance.DisplayAnnouncerText(AnnouncerManager.Announcement.Mission1); 
        Time.timeScale = 1;
    }

    void Rematch() {
        MenuManager.CloseCurrentMenu();
        HUDManager.Instance.ResetScore();
        var game = (MechGame) GameManager.Instance.Runner.Game;
        game.RestartGame();
        //GameModeManager. start await phase
        AnnouncerManager.Instance.DisplayAnnouncerText(AnnouncerManager.Announcement.Mission1); 
    }

    public void UpdatePlayerVictories() {
        victoryText.text = "Player " + (MenuManager.lastWinner + 1) + " is The Winner!";
        for (int i = 0; i<2; i++) {
            playerVictories[i].text = MenuManager.playerVictoriesValue[i].ToString();
        }
    }

}
