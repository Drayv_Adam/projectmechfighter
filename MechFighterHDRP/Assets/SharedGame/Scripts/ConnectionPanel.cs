﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityGGPO;

namespace SharedGame {

    public class ConnectionPanel : MonoBehaviour {
        public Button btnLocal;
        public Button btnRemote;
        public Button btnHost;
        public TextField inpIp;
        public TextField inpPort;
        public TextField txtIp;
        public TextField txtPort;

        private GameManager gameManager => GameManager.Instance;
        private GgpoPerformancePanel perf;

        private void Awake() {
            gameManager.OnRunningChanged += OnRunningChanged;

            perf = FindObjectOfType<GgpoPerformancePanel>();
            perf.Setup();
            btnHost.clicked += OnHostClick;
            btnRemote.clicked += OnRemoteClick;
            btnLocal.clicked += OnLocalClick;
            inpIp.value = "127.0.0.1";
            inpPort.value = "7000";
            txtIp.value = "127.0.0.1";
            txtPort.value = "7001";
        }

        private void OnDestroy() {
            gameManager.OnRunningChanged -= OnRunningChanged;
            btnHost.clicked -= OnHostClick;
            btnRemote.clicked -= OnRemoteClick;
            btnLocal.clicked -= OnLocalClick;
        }

        private List<Connections> GetConnections() {
            var list = new List<Connections>();
            list.Add(new Connections() {
                ip = inpIp.text,
                port = ushort.Parse(inpPort.text),
                spectator = false
            });
            list.Add(new Connections() {
                ip = txtIp.text,
                port = ushort.Parse(txtPort.text),
                spectator = false
            });
            return list;
        }

        private void OnHostClick() {
            //gameManager.StartGGPOGame(perf, GetConnections(), 0);
        }

        private void OnRemoteClick() {
            //gameManager.StartGGPOGame(perf, GetConnections(), 1);
        }

        private void OnLocalClick() {
            gameManager.StartLocalGame(new [] {false, false});
        }

        private void OnRunningChanged(bool isRunning) {
            gameObject.SetActive(!isRunning);
        }
    }
}