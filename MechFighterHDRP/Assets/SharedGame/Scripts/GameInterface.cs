﻿using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UIElements;

namespace SharedGame {

    public class GameInterface : MonoBehaviour {
        public int maxLogLines = 20;
        public TextField txtStatus;
        public TextField txtChecksum;
        public TextField txtGameLog;
        public TextField txtPluginLog;
        public Button btnPlayer1;
        public Button btnPlayer2;
        public Button btnConnect;
        public Toggle tglPluginLog;
        public Toggle tglGameLog;
        public bool gameLog = true;
        public bool pluginLog = true;

        private GameManager gameManager => GameManager.Instance;
        private readonly List<string> gameLogs = new List<string>();
        private readonly List<string> pluginLogs = new List<string>();

        private void Awake() {
            gameManager.OnStatus += OnStatus;
            gameManager.OnChecksum += OnChecksum;
            GGPORunner.OnPluginLog += OnPluginLog;
            GGPORunner.OnGameLog += OnGameLog;
            gameManager.OnRunningChanged += OnRunningChanged;

            btnConnect.clicked += OnConnect;
            btnPlayer1.clicked += OnPlayer1;
            btnPlayer2.clicked += OnPlayer2;

            //tglPluginLog.SetEnabled(false);
            //tglGameLog.SetEnabled(false);

            //tglPluginLog.RegisterValueChangedCallback()
            //tglGameLog.onValueChanged.AddListener(OnToggleGameLog);

            SetConnectText("");
        }

        private void OnDestroy() {
            gameManager.OnStatus -= OnStatus;
            gameManager.OnChecksum -= OnChecksum;
            GGPORunner.OnPluginLog -= OnPluginLog;
            GGPORunner.OnGameLog -= OnGameLog;
            gameManager.OnRunningChanged -= OnRunningChanged;

            btnConnect.clicked -= OnConnect;
            btnPlayer1.clicked -= OnPlayer1;
            btnPlayer2.clicked -= OnPlayer2;

            //tglPluginLog.onValueChanged.RemoveListener(OnTogglePluginLog);
            //tglGameLog.onValueChanged.RemoveListener(OnToggleGameLog);
        }

        private void OnRunningChanged(bool obj) {
            SetConnectText(obj ? "Shutdown" : "--");
        }

        private void OnToggleGameLog(bool value = true) {
            if (tglGameLog.value) {
                txtGameLog.value = string.Join("\n", gameLogs);
            }
            txtGameLog.SetEnabled(tglGameLog.value);
        }

        private void OnTogglePluginLog(bool value) {
            if (tglPluginLog.value) {
                txtPluginLog.value = string.Join("\n", gameLogs);
            }
            txtPluginLog.SetEnabled(tglPluginLog.value);
        }

        private void SetConnectText(string text) {
            //btnConnect.GetComponentInChildren<Text>().text = text;
        }

        private void OnGameLog(string text) {
            if (gameLog) {
                Debug.Log("[GameLog] " + text);
            }
            gameLogs.Insert(0, text);
            while (gameLogs.Count > maxLogLines) {
                gameLogs.RemoveAt(gameLogs.Count - 1);
            }
            if (tglGameLog.value) {
                txtGameLog.value = string.Join("\n", gameLogs);
            }
        }

        private void OnPluginLog(string text) {
            if (pluginLog) {
                Debug.Log("[PluginLog] " + text);
            }
            pluginLogs.Insert(0, text);
            while (pluginLogs.Count > maxLogLines) {
                pluginLogs.RemoveAt(gameLogs.Count - 1);
            }
            if (tglPluginLog.value) {
                txtPluginLog.value = string.Join("\n", pluginLogs);
            }
        }

        private void OnPlayer1() {
            gameManager.DisconnectPlayer(0);
        }

        private void OnPlayer2() {
            gameManager.DisconnectPlayer(1);
        }

        private void OnConnect() {
            if (gameManager.IsRunning) {
                gameManager.Shutdown();
            }
        }

        private void OnChecksum(string text) {
            txtChecksum.value = text;
        }

        private void OnStatus(string text) {
            txtStatus.value = text;
        }
    }
}