﻿using System;
using Core.StageInfo;
using MechFighter;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Editor {
    
    using static MechGameConstants;
    
    [CustomEditor(typeof(StageInfoSetter))]
    public class StageInfoSetterEditor : UnityEditor.Editor {
        
        bool showInfo;
        bool showSpawnPoints;
        bool[] showSpawnPoint;

        int newSceneId = -1;
        int newBoundsRadius = -1;
        
        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            
            
            StageInfoSetter stageInfoSetter = (StageInfoSetter) target;

            EditorGUILayout.GetControlRect();
            
            if (stageInfoSetter.StageInfoObject != null) {
                EditorGUILayout.BeginVertical();
                showInfo = EditorGUILayout.BeginFoldoutHeaderGroup(showInfo, "Stage Info");
                var stageInfo = stageInfoSetter.StageInfoObject.stageInfo;


                if (newSceneId < 0)
                    newSceneId = ValidateSceneId(stageInfo.sceneID);
                if (newBoundsRadius < 0)
                    newBoundsRadius = (int)stageInfo.boundsRadius;
                if (showInfo) {
                    EditorGUI.indentLevel++;

                    newSceneId = EditorGUILayout.IntField("Scene ID:", newSceneId);
                    
                    ShowSpawnPoints(stageInfo);
                    
                    newBoundsRadius = EditorGUILayout.IntField("Bounds radius:", newBoundsRadius);
                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
                EditorGUILayout.EndVertical();
            }
            
            if (GUILayout.Button("SaveInfo")) {
                stageInfoSetter.SaveInfo(newSceneId, newBoundsRadius);    
            }
        }

        void ShowSpawnPoints(StageInfo stageInfo) {
            showSpawnPoints = EditorGUILayout.Foldout(showSpawnPoints, "Spawn Points");
            if (showSpawnPoints) {
                EditorGUI.indentLevel++;
                var spawnInfos = stageInfo.spawnInfo;

                if (showSpawnPoint == null || showSpawnPoint.Length != spawnInfos.Length) {
                    showSpawnPoint = new bool[spawnInfos.Length];
                }

                        
                for (var i = 0; i < spawnInfos.Length; i++) {
                    showSpawnPoint[i] = EditorGUILayout.Foldout(showSpawnPoint[i], $"spawn point {i}", true);
                    if (showSpawnPoint[i]) {
                        EditorGUI.indentLevel++;
                        var info = spawnInfos[i];
                        EditorGUILayout.LabelField("position: ", $"x: {info.position.x}, y: {info.position.y}, z: {info.position.z}");
                        EditorGUILayout.LabelField(" ", $"(x: {(float) info.position.x / SCALE}, y: {(float) info.position.y / SCALE}, z: {(float) info.position.z / SCALE})");
                        EditorGUILayout.LabelField("heading:", $"{info.heading} ({(float) info.heading / ANGLE_SCALE})");
                        EditorGUI.indentLevel--;
                    }
                }
                EditorGUI.indentLevel--;
            }
        }

        int ValidateSceneId(int sceneID) {
            if (sceneID <= 0 || sceneID >= SceneManager.sceneCountInBuildSettings) {
                sceneID = SceneManager.GetActiveScene().buildIndex;
                if (sceneID <= 0 || sceneID >= SceneManager.sceneCountInBuildSettings) {
                    Debug.LogError("Current Scene is not added in build settings! Make sure it is added there first then set the ID here");
                    return 0;
                }
            }
            return sceneID;
        }
    }
}