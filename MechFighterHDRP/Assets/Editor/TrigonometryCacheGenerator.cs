#if UNITY_EDITOR
using System;
using System.IO;
using System.Text;
using Core.Utility;
using MechFighter;
using Rewired.Utils.Classes.Data;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Editor {
    
    using static MechGameConstants;
    public class TrigonometryCacheGenerator : MonoBehaviour {
        const string UTILITY_FOLDER = "Utility";
        const string CORE_FOLDER = "Core";

        const string FILENAME = "TrigonometryCache.cs";

        #region Code Fragments

        const string HEAD = @"using MechFighter;

namespace Core.Utility {
    public struct TrigonometryCache {
        const ushort HALF_ANG = 180 * MechGameConstants.ANGLE_SCALE;
        const ushort FULL_ANG = 360 * MechGameConstants.ANGLE_SCALE;
        const ushort COS_OFFSET = 90 * MechGameConstants.ANGLE_SCALE;

        public static int Sin(int angle) {
            if (angle < 0)
                angle += FULL_ANG;
            if (angle >= HALF_ANG) {
                return -SinValues[angle - HALF_ANG];
            }
            return SinValues[angle];
        }
        
        public static int Cos(int angle) {
            angle = COS_OFFSET - angle;
            if (angle < 0)
                angle += FULL_ANG;
            else if (angle > FULL_ANG)
                angle -= FULL_ANG;
            return Sin(angle);
        }

";

        const string FOOT = @"
    }
}";
        
        const string SIN_COLLECTION_DECLARATION = @"
        #region Data
        
        static readonly ushort[] SinValues = new ushort[] {
";
        const string COLLECTION_CLOSE = @"        };
        
        #endregion";
        
        #endregion

        const string SEPARATOR = ", ";

        static uint entryCount = 0;

        [MenuItem("MechFighter/Trigonometry Cache/Test Trigonometry Cache")]
        static void TestTrigonometryCache() {
            
            
            TestTrigonometryCacheFor(0);
            TestTrigonometryCacheFor(90);
            TestTrigonometryCacheFor(180);
            TestTrigonometryCacheFor(270);
            TestTrigonometryCacheFor(359);
            TestTrigonometryCacheFor(Random.Range(-360, 360));
            TestTrigonometryCacheFor(Random.Range(-360, 360));
            TestTrigonometryCacheFor(Random.Range(-360, 360));
            TestTrigonometryCacheFor(Random.Range(-360, 360));


            for (int i = -360 * ANGLE_SCALE; i < 360 * ANGLE_SCALE; i++) {
                TrigonometryCache.Cos(i);
            }
            for (int i = -360 * ANGLE_SCALE; i < 360 * ANGLE_SCALE; i++) {
                TrigonometryCache.Sin(i);
            }

            /*
            var sw = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < 1000000; i++)
            {
                TrigonometryCache.Cos(GetRandomIntAngle());
            }
            sw.Stop();
            Debug.Log($"(1) {sw.Elapsed}  // original version");
            
            Math.Sin(GetRandomAngle());
            sw = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < 1000000; i++)
            {
                Math.Sin(GetRandomAngle());
            }
            sw.Stop();
            Debug.Log($"(2) {sw.Elapsed}  // Math.Sin");
            */
        }

        static float GetRandomAngle() {
            return Random.Range(0, 360);
        }
        
        static int GetRandomIntAngle() {
            int ang = 360 * ANGLE_SCALE;
            return Random.Range(0, ang);
        }
        
        static void TestTrigonometryCacheFor(float testAngle) {
            
            int roundedAngle = MechUtils.FloatToHeading(testAngle);
            Debug.Log($"Testing for angle {testAngle} (converted: {roundedAngle})");
            var radians = testAngle * Mathf.Deg2Rad;
            var correctSin = Mathf.Sin(radians);
            var correctCos = Mathf.Cos(radians);
            
            var newSin = TrigonometryCache.Sin(roundedAngle);
            var newCos = TrigonometryCache.Cos(roundedAngle);

            float convertedSin = (float)newSin / SCALE;
            float convertedCos = (float)newCos / SCALE;
        
            Debug.Log($"Test Results: sin: {convertedSin}, cos; {convertedCos}, correct: sin: {correctSin:F4}, cos: {correctCos:F4}. Delta sin = {Mathf.Abs(convertedSin - correctSin):F8}, Delta cos = {Mathf.Abs(convertedCos - correctCos):F8}");
        }

        [MenuItem("MechFighter/Trigonometry Cache/Test Vector Rotate")]
        static void TestVectorRotate() {
            int reps = 5000;

            float deltaSum = 0;

            for (int i = 0; i < reps; i++) {
                var sample = Random.insideUnitCircle;
                var angle = Random.Range(-360, 360);
                deltaSum += TestVectorRotateFor(sample, angle, i < 50);
            }

            Debug.Log($"Test result for {reps} samples: average delta: {deltaSum:F8}");
        }

        static float TestVectorRotateFor(Vector2 v, float angle, bool log = true) {
            var correct = CorrectRotateVector(v, angle);
            Vector2Int scaledIntVector = Vector2Int.RoundToInt(v * SCALE);
            var intAngle = (int)(angle * ANGLE_SCALE);
            
            var ang = angle;
            Vector2Int mine = Vector2Int.zero;
            try {
                mine = MechUtils.RotateVector2(scaledIntVector, intAngle);
            }
            catch (Exception e) {
                Debug.LogError($"Exception on angle {intAngle} ({angle}), msg: {e.Message}");
                throw;
            }

            var rescaledOutput = ((Vector2) mine) / SCALE;
            var delta = rescaledOutput - correct;

            if (log) {
                Debug.Log($"input: {v:F4}, scaled: {scaledIntVector}, angle: {angle:F4}, scaled: {intAngle}");
                Debug.Log($"output: correct: {correct:F4}, mine: {mine}, delta: {delta:F4}");
            }
            return delta.magnitude;
        }
        
        static Vector2 CorrectRotateVector(Vector2 v, float angle) {
            var cs = Mathf.Cos(-angle * Mathf.Deg2Rad);
            var sn = Mathf.Sin(-angle * Mathf.Deg2Rad);
            
            return new Vector2(
                (v.x * cs - v.y * sn),
                (v.x * sn + v.y * cs));
        }
        
        [MenuItem("MechFighter/Trigonometry Cache/Generate Trigonometry Cache")]
        static void GenerateTrigonometryCache() {
            entryCount = 0;
            string savePath = Path.Combine(Application.dataPath, CORE_FOLDER, UTILITY_FOLDER, FILENAME);
            try {
                var data = string.Concat(
                    HEAD, 
                    SIN_COLLECTION_DECLARATION,
                    SinData(),
                    COLLECTION_CLOSE,
                    FOOT);
                File.WriteAllText(savePath, data, Encoding.UTF8);
            }
            catch (Exception e) {
                Debug.LogError($"Generating Trigonometry cache failed: {e.Message}");
                return;
            }
            
            Debug.Log($"Trigonometry cache saved to: {savePath}, {entryCount} entries");
        }

        static string SinData() {
            var data = "";
            for (uint i = ushort.MinValue; i < 180 * ANGLE_SCALE; i++) {
                entryCount++;

                var a = (double)i / ANGLE_SCALE;
                var value = (ushort)Math.Round(Math.Sin(a * Mathf.Deg2Rad) * SCALE);

                if (i % 10 == 9)
                    data += string.Concat(value, SEPARATOR, Environment.NewLine);
                else
                    data += string.Concat(value, SEPARATOR);
            }
            return data;
        }
    }
}
#endif

