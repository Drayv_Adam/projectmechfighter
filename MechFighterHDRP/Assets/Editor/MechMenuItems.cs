using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

namespace Editor {
    public class MechMenuItems : MonoBehaviour {
        const string LOCAL_TESTING_FLAG = "LOCALTESTING";
        const char SEPARATOR = ';';

        static MechMenuItems instance;
        
#if LOCALTESTING
        
#endif
        static string toggleLocalTestingFlag_Text;
        
#if LOCALTESTING
        [UnitySetUp, MenuItem("MechFighter/Switch Local Testing Off")]
        static void ToggleLocalTestingFlag() {
            Debug.Log("LOCALTESTING OFF");
            var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
            var index = defines.IndexOf(LOCAL_TESTING_FLAG, StringComparison.Ordinal);
            var length = LOCAL_TESTING_FLAG.Length;
            if (index > 0) {
                index--;
                length++;
            }
            defines = defines.Remove(index, length);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, defines);
            AssetDatabase.Refresh();
            try {
                EditorUtility.RequestScriptReload();
            }
            catch (Exception e) {
                Debug.LogError(e.Message);
            }
        }
#else
        [UnitySetUp, MenuItem("MechFighter/Switch Local Testing On")]
        static void ToggleLocalTestingFlag() {
            Debug.Log("LOCALTESTING ON");
            var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
            if (!string.IsNullOrEmpty(defines))
                defines = string.Concat(defines, SEPARATOR, LOCAL_TESTING_FLAG);
            else
                defines = LOCAL_TESTING_FLAG;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, defines);
            AssetDatabase.Refresh();
            try {
                EditorUtility.RequestScriptReload();
            }
            catch (Exception e) {
                Debug.LogError(e.Message);
            }
        }
#endif
    }
}
