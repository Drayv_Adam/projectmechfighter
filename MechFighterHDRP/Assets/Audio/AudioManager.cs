using System;
using UI;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

namespace Audio {
    public class AudioManager : MonoBehaviour {
        const float MUSIC_PLAY_DELAY = 1f;
        
        const string MASTER_VOLUME_KEY = "MasterVolume";
        const string MUSIC_VOLUME_KEY = "MusicVolume";
        const string SFX_VOLUME_KEY = "SFXVolume";
        const string AMBIENT_VOLUME_KEY = "AmbientVolume";
        
        public static AudioManager Instance { get; private set; }

        [SerializeField] AudioMixer mixer;

        [SerializeField] AudioClip[] battleThemes;
        [SerializeField] AudioClip menuTheme;

        [SerializeField] AudioSource musicAudioSource;
        
        public static float masterVolume;
        public static float musicVolume;
        public static float sfxVolume;
        public static float ambientVolume;

        public enum Theme {
            Menu,
            Battle1,
            Battle2
        }
        
        void Awake() {
            Instance = this;
            TryLoadSettings();
            if (MenuManager.Instance != null && MenuManager.CurrentOpenMenuType == MenuType.MainMenu) {
                musicAudioSource.clip = menuTheme;
                musicAudioSource.loop = true;
                musicAudioSource.PlayDelayed(MUSIC_PLAY_DELAY);
            }
        }

        public static void PlayTheme(Theme theme) {
            if (Instance == null)
                return;
            switch (theme) {
                case Theme.Menu:
                    Instance.musicAudioSource.clip = Instance.menuTheme;
                    break;
                case Theme.Battle1:
                    Instance.musicAudioSource.clip = Instance.battleThemes[0];
                    break;
                case Theme.Battle2:
                    Instance.musicAudioSource.clip = Instance.battleThemes[1];
                    break;
                default:
                    Debug.LogError("Invalid Theme");
                    Instance.musicAudioSource.Stop();
                    return;
            }
            Instance.musicAudioSource.PlayDelayed(1);
        }

        public static void PlayRandomBattleTheme() {
            if (Instance == null) {
                Debug.LogError("AudioManager is not instantiated");
                return;
            }
            Instance.musicAudioSource.clip = Instance.battleThemes[Random.Range(0, Instance.battleThemes.Length)];
            Instance.musicAudioSource.PlayDelayed(1);
        }
        
        public static void SaveSettings() {
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, masterVolume);
            PlayerPrefs.SetFloat(MUSIC_VOLUME_KEY, musicVolume);
            PlayerPrefs.SetFloat(SFX_VOLUME_KEY, sfxVolume);
            PlayerPrefs.SetFloat(AMBIENT_VOLUME_KEY, ambientVolume);
        }

        public static void UpdateAllVolume() {
            Instance.mixer.SetFloat("Master", LogVolume(masterVolume));
            Instance.mixer.SetFloat("Music",LogVolume(musicVolume));
            Instance.mixer.SetFloat("SFX", LogVolume(masterVolume));
            Instance.mixer.SetFloat("Ambient", LogVolume(masterVolume));
        }

        public static void SetMasterVolume(float percent) {
            masterVolume = percent;
            Instance.mixer.SetFloat("Master", LogVolume(masterVolume));
        }
        
        public static void SetMusicVolume(float percent) {
            musicVolume = percent;
            Instance.mixer.SetFloat("Music",LogVolume(musicVolume));
        }
        
        public static void SetSFXVolume(float percent) {
            sfxVolume = percent;
            Instance.mixer.SetFloat("SFX", LogVolume(sfxVolume));
        }
        public static void SetAmbientVolume(float percent) {
            ambientVolume = percent;
            Instance.mixer.SetFloat("Ambient", LogVolume(ambientVolume));
        }
        
        void TryLoadSettings() {
            if (PlayerPrefs.HasKey(MASTER_VOLUME_KEY))
                masterVolume = PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
            else
                masterVolume = 0.5f;
            if (PlayerPrefs.HasKey(MUSIC_VOLUME_KEY))
                musicVolume = PlayerPrefs.GetFloat(MUSIC_VOLUME_KEY);
            else
                musicVolume = 0.5f;
            if (PlayerPrefs.HasKey(SFX_VOLUME_KEY))
                sfxVolume = PlayerPrefs.GetFloat(SFX_VOLUME_KEY);
            else
                sfxVolume = 0.5f;
            if (PlayerPrefs.HasKey(AMBIENT_VOLUME_KEY))
                ambientVolume = PlayerPrefs.GetFloat(AMBIENT_VOLUME_KEY);
            else
                ambientVolume = 0.5f;
            UpdateAllVolume();
        }

        static float LogVolume(float percent) => Mathf.Log10(Mathf.Clamp(percent, 0.0001f, 1)) * 20;
    }
}
