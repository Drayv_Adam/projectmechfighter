﻿using System;
using System.Collections.Generic;
using Core;
using Core.Utility;
using MechFighter;
using SharedGame;
using UnityEngine;
using Random = System.Random;

namespace MechAI {
    using static Structs;
    using static MechGameConstants;

    public class DummyCPU {
        public static List<bool> CPUFlags = new List<bool>();
        //bool inputsUpdated;
        //bool initialized;
        //int controllerId = -1;

        static MechInputs[] mechInputsArray = new MechInputs[2] {
            new MechInputs((int) Buttons.Count), new MechInputs((int) Buttons.Count)
        };
        

        public static bool IsCPU(int controllerID) {
            return CPUFlags[controllerID];
        }

        public static MechInputs GetMechInputs(int controllerID) {
            var game = (MechGame) GameManager.Instance.Runner.Game;

            var currentFrame = game.Framenumber;
            
            var me = game.mechs[controllerID];
            var player = game.mechs[1 - controllerID];
            var offset = player.position - me.position;
            var mechDistance = MechUtils.SqrMagnitude(offset);
            
            
            var botWantsToAttack = mechDistance < 400000000;

            var newHeading = MechUtils.GetVectorHeading(new Vector2Int(offset.x, offset.z));
            
            var input = mechInputsArray[controllerID];
            input.aimData = new AimData(0, newHeading);
            
            var actionCooldown = currentFrame % 40;
            if (actionCooldown == 1) {
                input = new MechInputs((int) Buttons.Count) {aimData = new AimData(0, newHeading)};
                input.movement.y = SCALE;

                var randomMovementToChoose = UnityEngine.Random.Range(1, 12);
                Debug.Log(randomMovementToChoose);
                switch (randomMovementToChoose) {
                    case 1:
                        input.Jump = false;
                        input.Dash = false;
                        input.Melee = false;
                        input.Secondary = true;
                        input.Primary = false;
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        input.Dash = true;
                        Debug.Log("Should've dashed");
                        break;
                    case 8:
                        input.Jump = true;
                        Debug.Log("Should've Jumped");
                        break;
                    case 9:
                        input.movement.y = -SCALE;
                        Debug.Log("Should've moved back.");
                        break;
                    case 10:
                        input.movement.x = SCALE;
                        Debug.Log("Should've moved right.");
                        break;
                    case 11:
                        input.movement.x = -SCALE;
                        Debug.Log("Should've moved left.");
                        break;
                }

                if (botWantsToAttack) {
                    var randomActionToChoose = UnityEngine.Random.Range(1, 5);
                    switch (randomActionToChoose) {
                        case 1:
                        case 2:
                            input.Jump = false;
                            input.Dash = false;
                            input.Melee = false;
                            input.Secondary = false;
                            input.Primary = true;
                            break;
                        case 3:
                        case 4:
                            input.Jump = false;
                            input.Dash = false;
                            input.Primary = false;
                            input.Secondary = false;
                            input.Melee = true;
                            break;
                    }
                }
                
            }

            if (actionCooldown >= 20) {
                input.Dash = false;
                input.Jump = false;
                input.Primary = false;
                input.Secondary = false;
                input.Melee = false;
            }

            mechInputsArray[controllerID] = input;
            return input;
        }
        

    }
}