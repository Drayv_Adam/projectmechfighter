using System;
using Core;
using Core.Input;
using Core.Managers;
using MechAI;
using MechFighter;
using SharedGame;
using UnityEngine;
using UnityGGPO;

namespace GameViewAssets {
    public class MechGameViewManager : MonoBehaviour {
        public static MechGameViewManager Instance { get; private set; }
        [SerializeField] MechViewPrefabMapper mechViewPrefabs;

        // other prefabs
        [SerializeField] RotationHelper rotationHelperPrefab;
        [SerializeField] Camera cameraPrefab;
        [SerializeField] Transform mechsContainer;
        [SerializeField] Transform rotationHelperContainer;

        public static MechView[] mechViews;
        public Camera[] cameras;
        bool mechViewsInstantiated;

        public static MechView[] MechViews => mechViews;
        // other lists

        GameManager GameManager => GameManager.Instance;

        void Awake() {
            Instance = this;
            if (mechsContainer == null)
                mechsContainer = transform;
            mechViews = Array.Empty<MechView>();
            if (rotationHelperContainer == null)
                rotationHelperContainer = transform;
        }

        void Update() {
            if (GameManager.IsRunning && GameManager.Runner !=null)
                UpdateGameView(GameManager.Runner);
            else if (mechViewsInstantiated)
                StopView();
        }

        void UpdateGameView(IGameRunner runner) {
            var gameState = (MechGame) runner.Game;
            var gameInfo = runner.GameInfo;

            var mechStates = gameState.mechs;
            if (mechViews.Length != mechStates.Length)
                ResetView(gameState, gameInfo);

            for (var i = 0; i < mechStates.Length; i++) {
                mechViews[i].UpdateView(mechStates[i], gameInfo.players[i], gameState.Framenumber);
            }
        }

        void ResetView(MechGame gameState, GameInfo gameInfo) {
            DestroyMechViews();
            DestroyRotationHelpers();
            DestroyCameras();
            SpawnCameras();
            InstantiateMechViews(gameState.mechs, gameInfo.players);
        }

        void StopView() {
            DestroyMechViews();
            DestroyRotationHelpers();
            DestroyCameras();
        }

        public void SpawnCameras() {
            cameras = new Camera[MechGameRunner.LocalNonCPU];
            if (MechGameRunner.LocalNonCPU == 0) {
                return;
            }
            MechGameRunner.SetMenuCameraActive(false);
            for (int i = 0; i < MechGameRunner.LocalNonCPU; i++) {
                cameras[i] = Instantiate(cameraPrefab, rotationHelperContainer);
                cameras[i].cullingMask += (1 << MechGameConstants.V_CAM_LAYER[i]); //layerMask is a 32 bit array as an Int, so we change 0 to 1 at index we want by using bitshift
                if (MechGameRunner.LocalNonCPU == 2) {
                    //todo: manage camera rects
                    cameras[i].rect = new Rect(i * 0.5f, 0, 0.5f, 1);
                }
            }
        }

        public void DestroyCameras() {
            if (cameras == null || cameras.Length == 0)
                return;
            for (var i = 0; i < cameras.Length; i++) {
                Destroy(cameras[i].gameObject);
            }
            cameras = new Camera[0];
        }

        public void DestroyMechViews() {
            if (mechViews == null) return;
            for (var i = 0; i < mechViews.Length; i++) {
                Destroy(mechViews[i].gameObject);
            }
            mechViews = new MechView[0];
            mechViewsInstantiated = false;
        }

        public void DestroyRotationHelpers() {
            if (RotationHelper.rotationHelpers == null) return;
            for (var i = 0; i < RotationHelper.rotationHelpers.Length; i++) {
                if (RotationHelper.rotationHelpers[i] == null)
                    continue;
                Destroy(RotationHelper.rotationHelpers[i].gameObject);
                RotationHelper.rotationHelpers[i] = null;
            }
        }

        void InstantiateMechViews(Mech[] mechs, PlayerConnectionInfo[] playerInfos) {
            mechViews = new MechView[mechs.Length];
            mechViewsInstantiated = true;
            for (var i = 0; i < mechs.Length; i++) {
                var mechPrefab = GetMechPrefab(i);
                mechViews[i] = Instantiate(mechPrefab, mechsContainer);
                if (i < playerInfos.Length && playerInfos[i].type == GGPOPlayerType.GGPO_PLAYERTYPE_LOCAL &&
                    !DummyCPU.CPUFlags[playerInfos[i].controllerId]) {
                    //todo: create a cameraHelper for CPU? 
                    SetupCameraHelper(playerInfos[i].controllerId, mechViews[i], mechs[i]);
                }
            }
        }

        MechView GetMechPrefab(int playerId) {
            if (mechViewPrefabs == null) {
                Debug.LogError("MechPrefabsMapper object is null");
                return null;
            }

            if (playerId > MechGameRunner.MechGame.playerInfos.Length) {
                Debug.LogError(
                    "PlayerIndex out of range. Are you sure this is a player? Are playerInfos loadedCorrectly");
                return DefaultMechView;
            }

            var mechId = MechGameRunner.MechGame.playerInfos[playerId].playerLoadoutData.mechId;
            if (mechId >= mechViewPrefabs.MechViews.Length || mechViewPrefabs.MechViews[mechId] == null) {
                Debug.LogError("MechId not found");
                return DefaultMechView;
            }

            return mechViewPrefabs.MechViews[mechId];
        }

        MechView DefaultMechView => mechViewPrefabs ? mechViewPrefabs.MechViews[0] : null;

        void SetupCameraHelper(int controllerId, MechView followTarget, Mech initialState) {
            RotationHelper.rotationHelpers[controllerId] = Instantiate(rotationHelperPrefab, rotationHelperContainer);
            RotationHelper.rotationHelpers[controllerId].Initialize(controllerId, followTarget.transform, initialState.aimData);
            followTarget.rotationHelper = RotationHelper.rotationHelpers[controllerId];
        }
    }
}