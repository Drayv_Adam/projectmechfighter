using System.Collections.Generic;
using UnityEngine;

namespace GameViewAssets.Code {
    public abstract class MechAnimations : MonoBehaviour {

        public abstract Dictionary<int, AnimationStateInfo> MechAnimationDictionary { get; }
    
        public static readonly int IsMoving = Animator.StringToHash("isMoving"); //could get ridoff and base off of WalkXY
        public static readonly int IsRunning = Animator.StringToHash("isRunning"); //could get rid off and base off of MechState
        public static readonly int IsGrounded = Animator.StringToHash("isGrounded"); //could get rid off and base off of MechState
        public static readonly int IsCrouching = Animator.StringToHash("isCrouching"); //needed for blocking and reaction variants
        public static readonly int WalkY = Animator.StringToHash("WalkY");
        public static readonly int WalkX = Animator.StringToHash("WalkX");
        public static readonly int IsInHitStun = Animator.StringToHash("isInHitStun"); //not sure bout this one
        public static readonly int IsBlocking = Animator.StringToHash("isBlocking"); //not sure bout this one
        public static readonly int IsDashing = Animator.StringToHash("isDashing"); //not sure bout this one
    
        public enum AnimatorLayers{
            IdleAndWalk,
            Jumps,
            Dashes,
            Blocking,
            Attacks,
            ReceivedHits
        }
    
    }
}