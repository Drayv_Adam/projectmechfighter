﻿using System;
using Connection;
using Core;
using Core.Input;
using Core.MechComponents;
using Core.Utility;
using GameViewAssets.Code;
using MechFighter;
using SharedGame;
using UI.Popups;
using UnityEditor;
using UnityEngine;
using UnityGGPO;

namespace GameViewAssets {
    public class MechView : MonoBehaviour {
        //This makes sure the mech represents itself in the ggpo's GameState
        [SerializeField] InterpolationComponent interpolationComponent;
        [SerializeField] MechAnimationManager mechAnimationManager;
        [SerializeField] ShieldController shieldController;

        [SerializeField] Mech currentState;

        [SerializeField] ParticleSystem hitSpark;
        [SerializeField] ParticleSystem blockSpark;
        [SerializeField] AudioSource hitSound;
        [SerializeField] AudioSource blockSound;

        public RotationHelper rotationHelper { get; set; }

        public Mech CurrentState => currentState;

#if UNITY_EDITOR
        public bool debugInfoEnabled = true;
        public bool autoDisableIfNotPlayer1 = true;
        
        public Color debugTextColor = Color.red;
        public int debugTextFontSize = 21;

        
        public bool debugBasicInfo = true;
        public bool debugConnectionState = false;
        public bool debugCharacterState = true;
        public bool drawHitboxes;
        public bool drawHurtboxes;
        public bool drawVelocity = false;
        public bool drawPosition = true;

        Vector3 debugDrawPos;
        PlayerConnectState debugPlayerConnectState;
        string debugCurrentStatusText;
        int debugCurrentProgress;
        bool debugStarted = false;
#endif

        void Awake() {
            MechHitController.OnHit += PlayHitSpark;
            MechHitController.OnBlock += PlayBlockSpark;
        }

        void OnDestroy() {
            MechHitController.OnHit -= PlayHitSpark;
            MechHitController.OnBlock -= PlayBlockSpark;
        }

        void PlayHitSpark(int mechId) {
            if (mechId != CurrentState.handle)
                return;
            hitSpark.Play();
            hitSound.Play();
        }
        
        void PlayBlockSpark(int mechId) {
            if (mechId != CurrentState.handle)
                return;
            blockSpark.Play();
            blockSound.Play();
        }
        
        public void UpdateView(Mech mechState, PlayerConnectionInfo connectionInfo, int frameNumber) {
            currentState = mechState;
            CheckConnectionState(connectionInfo);
            bool running = connectionInfo.state == PlayerConnectState.Running;
            //maybe we want to control a disconnected player in a certain way?
            //by default, it is controlled by a rudimentary AI, so nothing *really* needs to be done here, besides maybe UI
            //maybe we want to show something on UI 


            mechAnimationManager.UpdateView(mechState, frameNumber);
            if (interpolationComponent != null)
                interpolationComponent.InterpolateTransform(mechState.position, mechState.aimData.Heading);

            if (rotationHelper != null)
                rotationHelper.UpdateLockOn(currentState.lockedOn, currentState.lockOnTarget);

            if (shieldController != null)
                shieldController.UpdateShield(mechState);

            //TODO: Update Effects
            //Update UI?
        }

        void CheckConnectionState(PlayerConnectionInfo connectionInfo) {
            string status = "";
            int progress = -1;
            switch (connectionInfo.state) {
                case PlayerConnectState.Connecting:
                    status = (connectionInfo.type == GGPOPlayerType.GGPO_PLAYERTYPE_LOCAL)
                        ? "Local Player"
                        : "Connecting...";
                    break;

                case PlayerConnectState.Synchronizing:
                    progress = connectionInfo.connect_progress;
                    status = (connectionInfo.type == GGPOPlayerType.GGPO_PLAYERTYPE_LOCAL)
                        ? "Local Player"
                        : "Synchronizing...";
                    break;

                case PlayerConnectState.Disconnected:
                    status = "Disconnected";
                    //Destroy GameObject or Deactivate it?
                    //Destroy(gameObject);
                    PopupManager.ShowError($"player {connectionInfo.handle} disconnected");
                    ServerConnectionManager.Instance.FallBackToOnlineMode();
                    break;

                case PlayerConnectState.Disconnecting:
                    status = "Waiting for player...";
                    progress = (Utils.TimeGetTime() - connectionInfo.disconnect_start) * 100 /
                               connectionInfo.disconnect_timeout;
                    break;
                case PlayerConnectState.Running:
                    status = "Running";
                    progress = 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            //TODO: UpdateUI
            //statusText = status;
            //progresBar = progress;

#if UNITY_EDITOR
            TempDebugConnectionStateInfo(status, progress, connectionInfo.state);
#endif
        }
#if UNITY_EDITOR
        void TempDebugConnectionStateInfo(string status, int progress, PlayerConnectState connectState) {
            debugCurrentProgress = progress;
            debugCurrentStatusText = status;
            debugPlayerConnectState = connectState;
            if (debugCurrentStatusText != status) {
                Debug.Log($"[MechView] mechStatus: {status}");
                debugCurrentStatusText = status;
            }
        }

        void OnDrawGizmos() {
            if (!debugStarted)
                AutoDisableIfNotPlayer1();
            debugStarted = true;
            if (!debugInfoEnabled || GameManager.Instance == null || !GameManager.Instance.IsRunning)
                return;
            GUIStyle textStyle = new GUIStyle();
            textStyle.normal.textColor = debugTextColor;
            textStyle.fontSize = debugTextFontSize;
            debugDrawPos = transform.position + Vector3.up * 2;
            
            var text = "";

            if (debugBasicInfo)
                text += DebugBasicInfo();
            if (debugConnectionState)
                text += DebugConnectionState();
            if (debugCharacterState)
                text += DebugCharacterState();
            if (drawPosition)
                text += DrawPosition();
            if (drawVelocity)
                text += DrawVelocity();

            Handles.Label(debugDrawPos, text, textStyle);
        }

        void AutoDisableIfNotPlayer1() {
            debugInfoEnabled &= !(autoDisableIfNotPlayer1 && currentState.handle != 0);
        }
        
        string DebugBasicInfo() {
            if (MechGameRunner.Instance == null || currentState.handle > MechGameRunner.MechGame.playerInfos.Length)
                return "failed to get player info\n";
            var info = MechGameRunner.MechGame.playerInfos[currentState.handle];

            var text = $"{info.playerName} \nLoadout: mechId {info.playerLoadoutData.mechId} \n";
            var start = MechUtils.IntToVector3(currentState.position);

            Gizmos.color = currentState.grounded ? Color.green : Color.grey;
            Gizmos.DrawSphere(start, 0.1f);

            return text;
        }

        string DebugCharacterState() {
            int frame = GameManager.Instance.Runner.Game.Framenumber - currentState.currentAnimStateStartFrame;
            return $"current animation: {currentState.currentAnimStateId}: {frame}\n" +
                   $"(int)character State: {currentState.characterState} \n";;
        }
        
        string DebugConnectionState() {
            var text = $"{debugCurrentStatusText}";
            if (debugCurrentProgress > 0  && debugCurrentProgress != 1)
                text += $" - progress: {debugCurrentProgress}";
            return text + "\n";
        }

        string DrawPosition() {
            return $"(int)position: {currentState.position} \n";
        }

        string DrawVelocity() {
            var start = MechUtils.IntToVector3(currentState.position);
            var velocity = MechUtils.IntToVector3(currentState.velocity);
            var maxSpeed = MechGameConstants.MAX_SPEED;
            Gizmos.color = Color.Lerp(new Color(1, 1, 1, 0.5f), new Color(1, 0, 0, 1),
                velocity.magnitude / maxSpeed);
            Gizmos.DrawLine(start, start + velocity);
            return $"(int)velocity: {currentState.velocity} \n";
        }
#endif
    }
}