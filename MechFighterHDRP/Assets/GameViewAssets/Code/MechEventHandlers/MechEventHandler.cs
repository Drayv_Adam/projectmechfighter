using System;
using Core.MechComponents;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameViewAssets.Code.MechEventHandlers {
    public class MechEventHandler : MonoBehaviour {
        
        [SerializeField] protected Animator mechAnimator;
        [SerializeField] protected MechView mech;

        [SerializeField] protected UniversalMechVFX universalParticles;
        [SerializeField] protected UniversalMechSFX universalSounds;

        [Serializable]
        public struct UniversalMechVFX {
            [Header("Left Dash")] public ParticleSystem leftDash;
            public ParticleSystem leftDashInitialize;
            [Header("Right Dash")] public ParticleSystem rightDash;
            public ParticleSystem rightDashInitialize;

            [Header("Sprint")] public ParticleSystem sprint;

            [Header("Jumps")] public ParticleSystem jump;
            public ParticleSystem jumpLand;
            public ParticleSystem doubleJump;
            
            [Header("HitSparks")]
            public ParticleSystem hitSpark;
            public ParticleSystem blockSpark;

#if UNITY_EDITOR
            public bool includeInAutoAssign;
            public void TryAssign(Transform transform) {
                if (!includeInAutoAssign)
                    return;
            }
#endif
        }

        [Serializable]
        public struct UniversalMechSFX {
            public AudioSource lightWhoosh;
            public AudioSource heavyWhoosh;
            public AudioSource jumpSound;
            public AudioSource land;
            public AudioSource dash;
            public AudioSource[] footstep;
            public AudioSource[] shot;
            [Header("HitSparks")]
            public AudioSource hitSound;
            public AudioSource blockSound;
            
#if UNITY_EDITOR
            public bool includeInAutoAssign;
            public void TryAssign(Transform transform) {
                if (!includeInAutoAssign)
                    return;
                var all = transform.GetComponentsInChildren<AudioSource>();
                footstep = new AudioSource[4];//new AudioSource[5];
                for (int i = 0; i < all.Length; i++) {
                    switch (all[i].name) {
                        case "HITSPARK_hit":
                            hitSound = all[i];
                            break;
                        case "HITSPARK_block":
                            hitSound = all[i];
                            break;
                        case "Melee_Light Whoosh":
                            lightWhoosh = all[i];
                            break;
                        case "Kick_Heavy Whoosh":
                            heavyWhoosh = all[i];
                            break;
                        case "NewJumpingV1":
                            jumpSound = all[i];
                            break;
                        case "NewLandingV1":
                            land = all[i];
                            break;
                        case "NewDash":
                            dash = all[i];
                            break;
                        case "Footsteps":
                            footstep[0] = all[i];
                            break;
                        case "Footsteps (1)":
                            footstep[1] = all[i];
                            break;
                        case "Footsteps (2)":
                            footstep[2] = all[i];
                            break;
                        case "Footsteps (3)":
                            footstep[3] = all[i];
                            break;
                        case "Footsteps (4)":
                            footstep[4] = all[i];
                            break;
                        case "Footsteps (5)":
                            //footstep[5] = all[i];
                            break;
                    }
                }
            }
#endif
        }
        
        void Awake() {
            MechHitController.OnHit += PlayHitSpark;
            MechHitController.OnBlock += PlayBlockSpark;
        }

        void OnDestroy() {
            MechHitController.OnHit -= PlayHitSpark;
            MechHitController.OnBlock -= PlayBlockSpark;
        }

        void PlayHitSpark(int mechId) {
            if (mechId != mech.CurrentState.handle)
                return;
            universalParticles.hitSpark.Play();
            universalSounds.hitSound.Play();
        }
        
        void PlayBlockSpark(int mechId) {
            if (mechId != mech.CurrentState.handle)
                return;
            universalParticles.blockSpark.Play();
            universalSounds.blockSound.Play();
        }

        public static int GetFreeAudioSourceIndex(AudioSource[] audioSources) {
            for (int i = 0; i < audioSources.Length; i++) {
                if (!audioSources[i].isPlaying)
                    return i;
            }
            Debug.LogWarning("Failed to find a free audioSource");
            return 0;
        }
        
#if UNITY_EDITOR
        public bool autoAssign;
        protected virtual void OnValidate() {
            if (!autoAssign)
                return;
            autoAssign = false;
            mechAnimator = GetComponent<Animator>();
            mech = GetComponent<MechView>();
            universalParticles.TryAssign(transform);
            universalSounds.TryAssign(transform);
        }
#endif
        
    }

    public static class AudioSourceExtension {
        public static void PlayWithRandomPitch(this AudioSource source, float pitchVariance = 0.5f, float original = 1) {
            //var original = source.pitch;
            source.pitch = Random.Range(original - pitchVariance, original + pitchVariance);
            source.Play();
            //source.pitch = original;
        }
    }
}