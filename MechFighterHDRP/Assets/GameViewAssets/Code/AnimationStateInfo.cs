using UnityEngine;

namespace GameViewAssets.Code{
    public struct AnimationStateInfo
    {
        public int length;
        public int layer;
        public int animationID;
            
        public AnimationStateInfo (int length, MechGunAnimationStates.AnimatorLayers layer, string animationID){
            this.length = length;
            this.layer = (int) layer;
            this.animationID = Animator.StringToHash(animationID);
        }
    }
}

