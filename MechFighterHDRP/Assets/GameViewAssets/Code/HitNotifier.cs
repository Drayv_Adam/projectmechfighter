using UnityEngine;

namespace GameViewAssets.Code {
    public class HitNotifier : MonoBehaviour {
        public static void BroadcastHitboxHit(HitboxHitInfo info) {
            
        }

        public static void BroadcastRaycastHit(RaycastHitInfo info) {
        }



        public struct HitboxHitInfo {
            public int attackerId;
            
            
        }

        public struct RaycastHitInfo {
            public int attackerId;
            public Vector3 hitPoint;
        }
    }
}
