using Core;
using UnityEngine;

namespace GameViewAssets.Code {
    public class ShieldController : MonoBehaviour {
        [SerializeField] Transform shield;

        [SerializeField] float scaleFull = 2.3f;

        [SerializeField] float shieldScaleSpeed = 100;

        float currentScale;

        bool fullyOpen;
        bool fullyClosed;
        
        void Awake() {
            if (shield != null) {
                shield.gameObject.SetActive(false);
                shield.localScale = Vector3.zero;
            }
            currentScale = 0;
        }

        public void UpdateShield(Mech mechState) {
            if (shield == null || mechState.isBlocking && fullyOpen || !mechState.isBlocking && fullyClosed)
                return;
            
            var targetScale = mechState.isBlocking ? scaleFull : 0;
            fullyClosed = false;
            fullyOpen = false;
            if (mechState.isBlocking)
                shield.gameObject.SetActive(true);
            
            if (targetScale > currentScale)
                currentScale += shieldScaleSpeed * Time.deltaTime;
            else
                currentScale -= shieldScaleSpeed * Time.deltaTime;

            if (currentScale < 0) {
                currentScale = 0;
                shield.gameObject.SetActive(false);
                fullyClosed = true;
            } else if (currentScale > scaleFull) {
                currentScale = scaleFull;
                fullyOpen = true;
            }

            shield.localScale = Vector3.one * currentScale;
        }
    }
}
