using System.Collections.Generic;

namespace GameViewAssets.Code{
    public class MechGunAnimationStates : MechAnimations {
        
        public override Dictionary<int, AnimationStateInfo> MechAnimationDictionary => GunAnimationDictionary;
        public enum AnimationState {
            //Layer: Idle and Walk 
            //Animations start with 10000
            Idle = 10000,
            Idle_Crouch = 10001,
            Walking = 10002,
            Aim_Walking = 10003,
            
            //Layer: Jumps
            //Animations start with 20000
            Falling = 20000,
            Jump_Land = 20001,
            Neutral_Jump = 20002,

            //Layer: Dashes
            //Animations start with 30000
            Dashing = 30000,

            //Layer: Blocking
            //Animations start with 40000
            Blocking = 40000,
            Blocking_Crouch = 40001,
            Block_Crouching_Struck = 40002,
            Block_Struck = 40003,

            //Layer: Attacks
            //Animations start with 50000
            
            //Layer: Attacks, Sub_State:Secondary_Crouching
            //Animations start with 51000
            Aim_Crouch = 51000,
            Aim_Crouch_Return = 51001,
            Crouch_Ranged_Shotgun = 51002,
            Crouch_Ranged_SingleShot = 51003,
            Crouch_Ranged_Burst = 51004,

            //Layer: Attacks, Sub_State:Secondary_Standing
            //Animations start with 52000
            Aim = 52000,
            Aim_Return = 52001,
            Ranged_Shotgun_Lvl1 = 52002,
            Ranged_Shotgun_Lvl2 = 52003,
            Ranged_Shotgun_Lvl3 = 52004,
            Ranged_Single_Lvl1 = 52005,
            Ranged_Single_Lvl2 = 52006,
            Ranged_Single_Lvl3 = 52007,
            Ranged_Burst_Lvl1 = 52008,
            Ranged_Burst_Lvl2 = 52009,
            Ranged_Burst_Lvl3 = 52010,
            Ranged_Burst_Quick = 52011,
            Special_QuickReload = 52012,

            //Layer: Attacks, Sub_State:Secondary_Air
            //Animations start with 53000
            Aim_Air = 53000,
            Aim_Air_Return = 53001,
            Ranged_Air_Shotgun = 53002,
            Ranged_Air_Single = 53003,
            Ranged_Air_Burst = 53004,
            Jump_Land_After_Shoot = 53005,

            //Layer: Attacks, Sub_State:Melee_Air
            //Animations start with 54000
            Melee_Air_Jab_Whole = 54000,
            Melee_Air_OverheadKick = 54001,
            Melee_Air_Jab_Charge = 54002,
            Melee_Air_Jab_Strike = 54003,

            //Layer: Attacks, Sub_State:Melee_Standing
            //Animations start with 55000
            Melee_OverheadPunch_Charge = 55000,
            Melee_OverheadPunch_Strike = 55001,
            Melee_OverheadPunch_Whole = 55002,
            Melee_SideKickLeft = 55003,
            Melee_SideKickRight = 55004,
            Melee_StraightPunch = 55005,
            Melee_Jab = 55006,

            //Layer: Attacks, Sub_State:Melee_Crouching
            //Animations start with 56000
            Melee_Crouching_LegPoke = 56000,
            Melee_Crouching_SweepRight = 56001,
            Melee_Crouching_SweepLeft = 56002,
            Melee_Crouching_Jab = 56003,
            Melee_Crouching_AntiAir = 56004,
            
            //Layer: Attacks, Sub_State:Primary_Crouching
            //Animations start with 57000
            Crouching_Neutral_Primary = 57000,
            Crouching_Backward_Primary = 57001,
            
            //Layer: Attacks, Sub_State:Primary_Standing
            //Animations start with 58000
            Standing_Neutral_Primary = 58000,
            Standing_Forward_Primary = 58001,
            Standing_Backward_Primary = 58002,
            
            
            //Layer: Attacks, Sub_State:Primary_Air
            //Animations start with 59000
            Air_Forward_Primary = 59000,
            Air_Neutral_Primary = 59001,
            Air_Forward_PrimaryALT = 59002,

            //Layer: Received Hits
            //Animations start with 60000
            GetUp_Spring = 60000,
            Hit_Crouching_Overhead = 60001,
            Hit_Mid = 60002,
            Hit_From_Left_1 = 60003,
            Hit_From_Left_2 = 60004,
            Hit_From_Right = 60005,
            Down_FaceUp = 60006,
            Landing_On_Back = 60007,
            Hit_Midair = 60008,
            Knockdown = 60009,
            Dead = 60010,
            Death_On_Knees = 60011,
        };

                
        static readonly  Dictionary<int, AnimationStateInfo> GunAnimationDictionary =
            new Dictionary<int, AnimationStateInfo>() {
                //Layer: Idle and Walk 
                {(int) AnimationState.Idle, new AnimationStateInfo(200, AnimatorLayers.IdleAndWalk, "Idle")},
                {(int) AnimationState.Idle_Crouch, new AnimationStateInfo(100, AnimatorLayers.IdleAndWalk, "Idle_Crouch")},
                {(int) AnimationState.Walking, new AnimationStateInfo(48, AnimatorLayers.IdleAndWalk, "Walking")},
                {(int) AnimationState.Aim_Walking, new AnimationStateInfo(40, AnimatorLayers.IdleAndWalk, "Aim_Walking")},
                
                //Layer: Jumps
                {(int) AnimationState.Neutral_Jump, new AnimationStateInfo(60, AnimatorLayers.Jumps, "Neutral_Jump")},
                {(int) AnimationState.Jump_Land, new AnimationStateInfo(68, AnimatorLayers.Jumps, "Jump_Land")},
                {(int) AnimationState.Falling, new AnimationStateInfo(100, AnimatorLayers.Jumps, "Falling")},
                
                //Layer: Dashes
                {(int) AnimationState.Dashing, new AnimationStateInfo(60, AnimatorLayers.Dashes, "Dashing")},
                
                //Layer: Blocking
                {(int) AnimationState.Blocking, new AnimationStateInfo(100, AnimatorLayers.Blocking, "Blocking")},
                {(int) AnimationState.Blocking_Crouch, new AnimationStateInfo(120, AnimatorLayers.Blocking, "Blocking_Crouch")},
                {(int) AnimationState.Block_Crouching_Struck, new AnimationStateInfo(20, AnimatorLayers.Blocking, "Blocking_Crouching_Struck")},
                {(int) AnimationState.Block_Struck, new AnimationStateInfo(100, AnimatorLayers.Blocking, "Block_Struck")},

                //Layer: Attacks
                
                //Layer: Attacks, Sub_State:Secondary_Crouching
                {(int) AnimationState.Aim_Crouch, new AnimationStateInfo(45, AnimatorLayers.Attacks, "Aim_Crouch")},
                {(int) AnimationState.Aim_Crouch_Return, new AnimationStateInfo(45, AnimatorLayers.Attacks, "Aim_Crouch_Return")},
                {(int) AnimationState.Crouch_Ranged_Shotgun, new AnimationStateInfo(20, AnimatorLayers.Attacks, "Crouch_Ranged_Shotgun")},
                {(int) AnimationState.Crouch_Ranged_SingleShot, new AnimationStateInfo(15, AnimatorLayers.Attacks, "Crouch_Ranged_SingleShot")},
                {(int) AnimationState.Crouch_Ranged_Burst, new AnimationStateInfo(33, AnimatorLayers.Attacks, "Crouch_Ranged_Burst")},

                //Layer: Attacks, Sub_State:Secondary_Standing
                {(int) AnimationState.Aim, new AnimationStateInfo(25, AnimatorLayers.Attacks, "Aim")},
                {(int) AnimationState.Aim_Return, new AnimationStateInfo(25, AnimatorLayers.Attacks, "Aim_Return")},
                {(int) AnimationState.Ranged_Shotgun_Lvl1, new AnimationStateInfo(26, AnimatorLayers.Attacks, "Ranged_Shotgun_Lvl1")},
                {(int) AnimationState.Ranged_Shotgun_Lvl2, new AnimationStateInfo(26, AnimatorLayers.Attacks, "Ranged_Shotgun_Lvl2")},
                {(int) AnimationState.Ranged_Shotgun_Lvl3, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Ranged_Shotgun_Lvl3")},
                {(int) AnimationState.Ranged_Single_Lvl1, new AnimationStateInfo(15, AnimatorLayers.Attacks, "Ranged_Single_Lvl1")},
                {(int) AnimationState.Ranged_Single_Lvl2, new AnimationStateInfo(19, AnimatorLayers.Attacks, "Ranged_Single_Lvl2")},
                {(int) AnimationState.Ranged_Single_Lvl3, new AnimationStateInfo(21, AnimatorLayers.Attacks, "Ranged_Single_Lvl3")},
                {(int) AnimationState.Ranged_Burst_Lvl1, new AnimationStateInfo(32, AnimatorLayers.Attacks, "Ranged_Burst_Lvl1")},
                {(int) AnimationState.Ranged_Burst_Lvl2, new AnimationStateInfo(40, AnimatorLayers.Attacks, "Ranged_Burst_Lvl2")},
                {(int) AnimationState.Ranged_Burst_Lvl3, new AnimationStateInfo(81, AnimatorLayers.Attacks, "Ranged_Burst_Lvl3")},
                {(int) AnimationState.Ranged_Burst_Quick, new AnimationStateInfo(40, AnimatorLayers.Attacks, "Ranged_Burst_Quick")},
                {(int) AnimationState.Special_QuickReload, new AnimationStateInfo(29, AnimatorLayers.Attacks, "Special_QuickReload")},

                //Layer: Attacks, Sub_State:Secondary_Air
                {(int) AnimationState.Aim_Air, new AnimationStateInfo(10, AnimatorLayers.Attacks, "Aim_Air")},
                {(int) AnimationState.Aim_Air_Return, new AnimationStateInfo(10, AnimatorLayers.Attacks, "Aim_Air_Return")},
                {(int) AnimationState.Ranged_Air_Shotgun, new AnimationStateInfo(31, AnimatorLayers.Attacks, "Ranged_Air_Shotgun")},
                {(int) AnimationState.Ranged_Air_Single, new AnimationStateInfo(13, AnimatorLayers.Attacks, "Ranged_Air_Single")},
                {(int) AnimationState.Ranged_Air_Burst, new AnimationStateInfo(40, AnimatorLayers.Attacks, "Ranged_Air_Burst")},
                {(int) AnimationState.Jump_Land_After_Shoot, new AnimationStateInfo(72, AnimatorLayers.Attacks, "Jump_Land_After_Shoot")},

                //Layer: Attacks, Sub_State:Melee_Air
                {(int) AnimationState.Melee_Air_Jab_Whole, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_Air_Jab_Whole")},
                {(int) AnimationState.Melee_Air_OverheadKick, new AnimationStateInfo(73, AnimatorLayers.Attacks, "Melee_Air_OverheadKick")},
                {(int) AnimationState.Melee_Air_Jab_Charge, new AnimationStateInfo(6, AnimatorLayers.Attacks, "Melee_Air_Jab_Charge")},
                {(int) AnimationState.Melee_Air_Jab_Strike, new AnimationStateInfo(42, AnimatorLayers.Attacks, "Melee_Air_Jab_Strike")},

                //Layer: Attacks, Sub_State:Melee_Standing
                {(int) AnimationState.Melee_OverheadPunch_Charge, new AnimationStateInfo(14, AnimatorLayers.Attacks, "Melee_OverheadPunch_Charge")},
                {(int) AnimationState.Melee_OverheadPunch_Strike, new AnimationStateInfo(55, AnimatorLayers.Attacks, "Melee_OverheadPunch_Strike")},
                {(int) AnimationState.Melee_OverheadPunch_Whole, new AnimationStateInfo(55, AnimatorLayers.Attacks, "Melee_OverheadPunch_Whole")},
                {(int) AnimationState.Melee_SideKickLeft, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_SideKickLeft")},
                {(int) AnimationState.Melee_SideKickRight, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_SideKickRight")},
                {(int) AnimationState.Melee_StraightPunch, new AnimationStateInfo(46, AnimatorLayers.Attacks, "Melee_StraightPunch")},
                {(int) AnimationState.Melee_Jab, new AnimationStateInfo(36, AnimatorLayers.Attacks, "Melee_Jab")},

                //Layer: Attacks, Sub_State:Melee_Crouching
                {(int) AnimationState.Melee_Crouching_LegPoke, new AnimationStateInfo(41, AnimatorLayers.Attacks, "Melee_Crouching_LegPoke")},
                {(int) AnimationState.Melee_Crouching_SweepRight, new AnimationStateInfo(59, AnimatorLayers.Attacks, "Melee_Crouching_SweepRight")},
                {(int) AnimationState.Melee_Crouching_SweepLeft, new AnimationStateInfo(59, AnimatorLayers.Attacks, "Melee_Crouching_SweepLeft")},
                {(int) AnimationState.Melee_Crouching_Jab, new AnimationStateInfo(27, AnimatorLayers.Attacks, "Melee_Crouching_Jab")},
                {(int) AnimationState.Melee_Crouching_AntiAir, new AnimationStateInfo(48, AnimatorLayers.Attacks, "Melee_Crouching_AntiAir")},
                
                //Layer: Attacks, Sub_State:Primary_Crouching
                //Animations start with 57000
                {(int) AnimationState.Crouching_Neutral_Primary, new AnimationStateInfo(30, AnimatorLayers.Attacks, "Crouching_Neutral_Primary")},
                {(int) AnimationState.Crouching_Backward_Primary, new AnimationStateInfo(45, AnimatorLayers.Attacks, "Crouching_Backward_Primary")},

                //Layer: Attacks, Sub_State:Primary_Standing
                //Animations start with 58000
                {(int) AnimationState.Standing_Neutral_Primary, new AnimationStateInfo(40, AnimatorLayers.Attacks, "Standing_Neutral_Primary")},
                {(int) AnimationState.Standing_Forward_Primary, new AnimationStateInfo(45, AnimatorLayers.Attacks, "Standing_Forward_Primary")},
                {(int) AnimationState.Standing_Backward_Primary, new AnimationStateInfo(45, AnimatorLayers.Attacks, "Standing_Backward_Primary")},

                //Layer: Attacks, Sub_State:Primary_Air
                //Animations start with 59000
                {(int) AnimationState.Air_Forward_Primary, new AnimationStateInfo(50, AnimatorLayers.Attacks, "Air_Forward_Primary")},
                {(int) AnimationState.Air_Neutral_Primary, new AnimationStateInfo(35, AnimatorLayers.Attacks, "Air_Neutral_Primary")},
                {(int) AnimationState.Air_Forward_PrimaryALT, new AnimationStateInfo(40, AnimatorLayers.Attacks, "Air_Forward_PrimaryALT")},

                //Layer: Received Hits
                {(int) AnimationState.GetUp_Spring, new AnimationStateInfo(58, AnimatorLayers.ReceivedHits, "GetUp_Spring")},
                {(int) AnimationState.Hit_Crouching_Overhead, new AnimationStateInfo(76, AnimatorLayers.ReceivedHits, "Hit_Crouching_Overhead")},
                {(int) AnimationState.Hit_Mid, new AnimationStateInfo(64, AnimatorLayers.ReceivedHits, "Hit_Mid")},
                {(int) AnimationState.Hit_From_Left_1, new AnimationStateInfo(25, AnimatorLayers.ReceivedHits, "Hit_From_Left_1")},
                {(int) AnimationState.Hit_From_Left_2, new AnimationStateInfo(32, AnimatorLayers.ReceivedHits, "Hit_From_Left_2")},
                {(int) AnimationState.Hit_From_Right, new AnimationStateInfo(25, AnimatorLayers.ReceivedHits, "Hit_From_Right")},
                {(int) AnimationState.Down_FaceUp, new AnimationStateInfo(60, AnimatorLayers.ReceivedHits, "Down_FaceUp")},
                {(int) AnimationState.Landing_On_Back, new AnimationStateInfo(100, AnimatorLayers.ReceivedHits, "Landing_On_Back")},
                {(int) AnimationState.Hit_Midair, new AnimationStateInfo(55, AnimatorLayers.ReceivedHits, "Hit_Midair")},
                {(int) AnimationState.Knockdown, new AnimationStateInfo(49, AnimatorLayers.ReceivedHits, "Knockdown")},
                {(int) AnimationState.Dead, new AnimationStateInfo(60, AnimatorLayers.ReceivedHits, "Dead")},
                {(int) AnimationState.Death_On_Knees, new AnimationStateInfo(109, AnimatorLayers.ReceivedHits, "Death_On_Knees")},
            };

        
    }
}
