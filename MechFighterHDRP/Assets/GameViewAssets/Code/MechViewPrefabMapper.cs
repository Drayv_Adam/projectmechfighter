using UnityEngine;

namespace GameViewAssets {
    [CreateAssetMenu(fileName = "MechPrefabMapper", menuName = "ScriptableObjects/Mappers/Create MechPrefabMapper", order = 1)]
    public class MechViewPrefabMapper : ScriptableObject {
        [SerializeField] MechView[] mechViews;

        public MechView[] MechViews => mechViews;
    }
}