﻿using Core.Utility;
using MechFighter;
using UnityEngine;

namespace GameViewAssets {
    public class InterpolationComponent : MonoBehaviour{
        const float POSITION_DIFF_THRESHOLD = 0.001f;
        const float ROTATION_DIFF_THRESHOLD = 0.1f;
        //const int INTERPOLATION_FRAMES = 1;
        //const float INTERPOLATION_TIME = MechGameConstants.DELTA_TIME * INTERPOLATION_FRAMES;
        const float INTERPOLATION_TIME = MechGameConstants.DELTA_TIME * 3;
        
        Vector3 currentSpeed = Vector3.zero;
        float angularSpeed = 0;

        public void InterpolateTransform(Vector3Int position, ushort heading) {
            var targetPosition = MechUtils.IntToVector3(position);
            var targetHeading = MechUtils.HeadingToFloat(heading);

            var viewTransform = transform;
            
            var dist = Vector3.SqrMagnitude(viewTransform.position - targetPosition);
            var rotDiff = Mathf.DeltaAngle(viewTransform.eulerAngles.y, targetHeading);

            if (dist < POSITION_DIFF_THRESHOLD && rotDiff < ROTATION_DIFF_THRESHOLD) {
                transform.position = targetPosition;
                transform.rotation = Quaternion.Euler(0, targetHeading, 0);
                return;
            }

            transform.position = Vector3.SmoothDamp(viewTransform.position, targetPosition, ref currentSpeed,
                INTERPOLATION_TIME);

            transform.rotation = Quaternion.Euler(0,
                Mathf.SmoothDampAngle(viewTransform.eulerAngles.y, targetHeading, ref angularSpeed, INTERPOLATION_TIME),
                0);
        }
    }
}