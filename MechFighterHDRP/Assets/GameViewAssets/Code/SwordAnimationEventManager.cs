using GameViewAssets.Code;
using GameViewAssets.Code.MechEventHandlers;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameViewAssets.Mechs.Animation.Swords {
    public class SwordAnimationEventManager : MonoBehaviour {
        [SerializeField] Animator mechAnimator;
        [SerializeField] Transform mech;

        [Header("Attacks")] [SerializeField] ParticleSystem Air_Neutral_PrimaryV2_P1_Particle;
        [SerializeField] ParticleSystem Air_Neutral_PrimaryV2_P2_Particle;
        [SerializeField] ParticleSystem Melee_Combo_P1_Particle;
        [SerializeField] ParticleSystem Melee_Combo_P2_Particle;
        [SerializeField] ParticleSystem Melee_PummelAttack_Particle;
        [SerializeField] ParticleSystem Melee_SidekickLeft_Unsworded_Particle;
        [SerializeField] ParticleSystem Melee_SidekickRight_Unsworded_Particle;
        [SerializeField] ParticleSystem DashSlash_Particle;
        [SerializeField] ParticleSystem DashSlashLights_Particle;
        [SerializeField] ParticleSystem Melee_Crouch_Slash_P1_Particle;
        [SerializeField] ParticleSystem Melee_Crouch_Slash_P2_Particle;
        [SerializeField] ParticleSystem Running_Forward_Melee_Particle;
        [SerializeField] ParticleSystem Melee_StraightPunch_Particle;
        [SerializeField] ParticleSystem Crouching_Neutral_Melee_Particle;

        [Header("States")] [SerializeField] ParticleSystem leftDash;
        [SerializeField] ParticleSystem rightDash;
        [SerializeField] ParticleSystem sprint;

        [Header("Movement")] [SerializeField] ParticleSystem jump;
        [SerializeField] ParticleSystem jumpLand;
        [SerializeField] ParticleSystem doubleJump;

        [Header("Step")] [SerializeField] ParticleSystem leftStep;
        [SerializeField] ParticleSystem rightStep;

        [SerializeField] AudioSource heavyWhoosh;

        [SerializeField] AudioSource heavyWhoosh2;

        [SerializeField] AudioSource lightWhoosh;

        [SerializeField] AudioSource lightWhoosh2;

        [SerializeField] AudioSource jumpSound;

        [SerializeField] AudioSource land;

        [SerializeField] AudioSource dash;

        [SerializeField] AudioSource[] footstep;
        // Update is called once per frame

        #region DashFX

        void HandleLeftBooster(bool isRunning) {
            if (isRunning) {
                //leftDash.gameObject.SetActive(true);
                if (!leftDash.isEmitting) {
                    dash.Play();
                    leftDash.Play();
                }
            }
            else {
                if (leftDash.isEmitting) {
                    leftDash.Stop();
                    //dash.Stop();
                }

                //leftDash.gameObject.SetActive(false);
            }
        }

        void HandleRightBooster(bool isDashing) {
            if (isDashing) {
                //rightDash.gameObject.SetActive(true);
                if (!rightDash.isEmitting)
                    rightDash.Play();
            }
            else {
                if (rightDash.isEmitting)
                    rightDash.Stop();
                //rightDash.gameObject.SetActive(false);
            }
        }

        void HandleRunning(bool isDashing) {
            if (isDashing) {
                sprint.gameObject.SetActive(true);
                if (!sprint.isPlaying)
                    sprint.Play();
            }
            else {
                if (sprint.isPlaying)
                    sprint.Stop();
                sprint.gameObject.SetActive(false);
            }
        }

        #endregion

        void Update() {
            HandleRunning(mechAnimator.GetBool(MechAnimations.IsDashing));
            var isDashing = mechAnimator.GetBool(MechAnimations.IsDashing);
            HandleLeftBooster(isDashing);
            HandleRightBooster(isDashing);
        }

        [UsedImplicitly]
        public void Air_Neutral_PrimaryV2_P1() {
            Air_Neutral_PrimaryV2_P1_Particle.Play();
            heavyWhoosh.PlayWithRandomPitch();
        }

        [UsedImplicitly]
        public void Air_Neutral_PrimaryV2_P2() {
            Air_Neutral_PrimaryV2_P2_Particle.Play();
            heavyWhoosh2.PlayWithRandomPitch();
        }

        [UsedImplicitly]
        public void Melee_Combo_P1() {
            lightWhoosh.PlayWithRandomPitch();
            Melee_Combo_P1_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_Combo_P2() {
            lightWhoosh2.PlayWithRandomPitch();
            Melee_Combo_P2_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_PummelAttack() {
            lightWhoosh.PlayWithRandomPitch();
            Melee_PummelAttack_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_SidekickLeft_Unsworded() {
            heavyWhoosh.PlayWithRandomPitch();
            Melee_SidekickLeft_Unsworded_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_SidekickRight_Unsworded() {
            heavyWhoosh.PlayWithRandomPitch();
            Melee_SidekickRight_Unsworded_Particle.Play();
        }

        [UsedImplicitly]
        public void DashSlash() {
            heavyWhoosh.Play();
            DashSlash_Particle.Play();
        }

        [UsedImplicitly]
        public void DashSlashLights() {
            DashSlashLights_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_Crouch_Slash_P1() {
            lightWhoosh.PlayWithRandomPitch();
            Melee_Crouch_Slash_P1_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_Crouch_Slash_P2() {
            lightWhoosh2.PlayWithRandomPitch();
            Melee_Crouch_Slash_P2_Particle.Play();
        }

        [UsedImplicitly]
        public void Running_Forward_Melee() {
            land.Play();
            Running_Forward_Melee_Particle.Play();
        }

        [UsedImplicitly]
        public void Melee_StraightPunch() {
            heavyWhoosh.Play();
            Melee_StraightPunch_Particle.Play();
        }

        [UsedImplicitly]
        public void Crouching_Neutral_Melee() {
            lightWhoosh.Play();
            Crouching_Neutral_Melee_Particle.Play();
        }

        [UsedImplicitly]
        public void Jump_Land() {
            land.Play();
            jumpLand.Play();
        }

        [UsedImplicitly]
        public void Jump() {
            jumpSound.Play();
            if (mech.position.y < 0.2f)
                jump.Play();
            else {
                doubleJump.Play();
            }
        }

        [UsedImplicitly]
        public void Left() {
            footstep[MechEventHandler.GetFreeAudioSourceIndex(footstep)].PlayWithRandomPitch();
            /*
            if (mech.position.y < 0.2f)
                leftStep.Play();
            */
            //too glitchy and I have no time to remove events.
        }

        [UsedImplicitly]
        public void Right() {
            footstep[MechEventHandler.GetFreeAudioSourceIndex(footstep)].PlayWithRandomPitch();
            /*
            if (mech.position.y < 0.2f)
                rightStep.Play();
            */
            //too glitchy and I have no time to remove events.
        }
    }
}