using Core;
using Core.Utility;
using UnityEngine;
using static Core.Utility.MechUtils;

namespace GameViewAssets.Code {
    public class MechAnimationManager : MonoBehaviour {
        const float MIN_WALK_SPEED = 0.0000001f;

        const int IDLE_ANIMATIONS = 20000;

        static readonly int Empty = Animator.StringToHash("Empty");
        
        [SerializeField] Animator mechAnimator;
        [SerializeField] MechAnimations mechAnimations;
        
        int currentAnimationId;
        int currentAnimationStartFrame;

        Mech mech;
        
#if UNITY_EDITOR
        [SerializeField] MechType mechType;
        [SerializeField] MechGunAnimationStates.AnimationState gunAnimationToDebugPlay;
        [SerializeField] MechSwordAnimationStates.AnimationState swordAnimationToDebugPlay;
        [SerializeField] float playOnFrame;
        [SerializeField] bool play;
#endif

        public void UpdateView(Mech mech, int frameNumber) {
            this.mech = mech;
            UpdateAnimatorParameters();
            UpdateAnimation(frameNumber);
        }

        void UpdateAnimation(int frameNumber) {
            if (currentAnimationId == mech.currentAnimStateId && currentAnimationStartFrame == mech.currentAnimStateStartFrame) 
                return;

            currentAnimationId = mech.currentAnimStateId;
            currentAnimationStartFrame = mech.currentAnimStateStartFrame;

            if (currentAnimationId == 0) {
                mechAnimator.Rebind();
                return;
            }
            if (currentAnimationId <= IDLE_ANIMATIONS)
                return;
            
            PlayAnimation(currentAnimationId, frameNumber - mech.currentAnimStateStartFrame);
        }

        void UpdateAnimatorParameters() {
            SetHorizontalMovement(CalculateHorizonalMovement(mech.parameters.walkSpeed));

            SetCharacterStateFlags(mech.characterState);
            
            mechAnimator.SetBool(MechAnimations.IsGrounded, mech.grounded);
            mechAnimator.SetBool(MechAnimations.IsInHitStun, mech.inHitStun);
            mechAnimator.SetBool(MechAnimations.IsBlocking, mech.isBlocking);
        }

        void SetCharacterStateFlags(Structs.MechCharacterState state) {
            mechAnimator.SetBool(MechAnimations.IsCrouching, false);
            mechAnimator.SetBool(MechAnimations.IsRunning, false);
            mechAnimator.SetBool(MechAnimations.IsDashing, false);
            switch (state) {
                case Structs.MechCharacterState.Crouching:
                    mechAnimator.SetBool(MechAnimations.IsCrouching, true);
                    break;
                case Structs.MechCharacterState.Dashing:
                    mechAnimator.SetBool(MechAnimations.IsDashing, true);
                    mechAnimator.SetBool(MechAnimations.IsRunning, true);
                    break;
                case Structs.MechCharacterState.Running:
                    mechAnimator.SetBool(MechAnimations.IsRunning, true);
                    break;
            }
        }
        
        Vector2 CalculateHorizonalMovement(int maxSpeed = 0) {
            var horizontalMovement = new Vector2Int(mech.velocity.x, mech.velocity.z); //If we go back to float, change this
            horizontalMovement = RotateVector2(horizontalMovement, -mech.aimData.Heading);
            var v = new Vector2(horizontalMovement.x, horizontalMovement.y);
            if (maxSpeed > 0)
                v /= maxSpeed;
            return v;
        }

        void SetHorizontalMovement(Vector2 movement) {
            bool isMoving = movement.sqrMagnitude > MIN_WALK_SPEED;
            mechAnimator.SetBool(MechAnimations.IsMoving, isMoving);
            mechAnimator.SetFloat(MechAnimations.WalkX, movement.x);
            mechAnimator.SetFloat(MechAnimations.WalkY, movement.y);
        }


        public void PlayAnimation(int animationState, float playOnFrame) {
            if (mechAnimator == null)
                return;
            ResetAnimationLayers(); //if mech is Tposing too much, use this after the return;
            var animationDictionary = mechAnimations.MechAnimationDictionary;
            if (!animationDictionary.ContainsKey(animationState)) {
                mechAnimator.Play(Empty);
                Debug.LogError($"Animation {animationState} state not found.");
                return;
            }

            var state = animationDictionary[animationState].animationID;
            var layer = animationDictionary[animationState].layer;
            float frameToPlayOn = playOnFrame / animationDictionary[animationState].length;
            mechAnimator.Play(state, layer, frameToPlayOn);
        }

        public void ResetAnimationLayers(bool includingIdles = false) {
            var start = includingIdles ? 0 : (int)MechAnimations.AnimatorLayers.Dashes;

            for (int i = start; i <= (int)MechAnimations.AnimatorLayers.ReceivedHits; i++) {
                mechAnimator.Play(Empty, i, 0);
            }
        }
        
        public enum MechType {
            Sword,
            Gun
        }

#if UNITY_EDITOR
        void OnValidate() {
            if (!play)
                return;
            mechAnimator.Rebind();
            
            if (mechType == MechType.Gun)
                PlayAnimation((int) gunAnimationToDebugPlay, playOnFrame);
            else if (mechType == MechType.Sword)
                PlayAnimation((int) swordAnimationToDebugPlay, playOnFrame);
            
            mechAnimator.Update(1f/60);
        }
#endif
    }
}