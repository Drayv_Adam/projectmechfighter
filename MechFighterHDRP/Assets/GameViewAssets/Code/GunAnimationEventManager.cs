using Core.Utility;
using GameViewAssets.Code;
using GameViewAssets.Code.MechEventHandlers;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameViewAssets.Mechs.Animation.Gun {
    public class GunAnimationEventManager : MonoBehaviour {

        [SerializeField] Transform mech;
        [SerializeField] Animator mechAnimator;
        [SerializeField] MechView mechView;

        [SerializeField] Vector3 shootOffset;
    
        [SerializeField] ParticleSystem AerialPrimaryN_Particle;
        [SerializeField] ParticleSystem CrouchingPrimary1_Particle;
        [SerializeField] ParticleSystem CrouchingPrimary2_Particle;
        [SerializeField] ParticleSystem CrouchingPrimary3_Particle;
        [SerializeField] ParticleSystem CrouchingPrimaryN_Particle;
        [SerializeField] ParticleSystem StandingPrimaryB1_Particle;
        [SerializeField] ParticleSystem StandingPrimaryB2_Particle;
        [SerializeField] ParticleSystem StandingPrimaryB3_Particle;
        [SerializeField] ParticleSystem StandingPrimaryB4_Particle;
        [SerializeField] ParticleSystem StandingPrimaryB5_Particle;
        [SerializeField] ParticleSystem StandingPrimaryB6_Particle;
        [SerializeField] ParticleSystem StandingForwardP_Particle;
        [SerializeField] ParticleSystem StandingNeutralPrimary_Particle;

        [SerializeField] ParticleSystem AerialMeleeB_Particle;
        [SerializeField] ParticleSystem AerialMeleeN_Particle;
        [SerializeField] ParticleSystem CrouchingMeleeN_Particle;
        [SerializeField] ParticleSystem MeleeCrouchingL_Particle;
        [SerializeField] ParticleSystem MeleeF_Particle;
        [SerializeField] ParticleSystem MeleeN_Particle;
        [SerializeField] ParticleSystem MeleeR_Particle;
        [SerializeField] ParticleSystem MeleeL_Particle;
        [SerializeField] ParticleSystem CrouchingBMelee_Particle;

        [SerializeField] ParticleSystem projectile;
    
        [Header("States")] 
        [SerializeField] ParticleSystem leftDash;
        [SerializeField] ParticleSystem leftDashInitialize;
        [SerializeField] TrailRenderer leftDashTrail;
        [SerializeField] ParticleSystem rightDash;
        [SerializeField] ParticleSystem rightDashInitialize;
        [SerializeField] TrailRenderer rightDashTrail;
        [SerializeField] ParticleSystem sprint;

        [Header("Movement")] 
        [SerializeField] ParticleSystem jump;
        [SerializeField] ParticleSystem jumpLand;
        [SerializeField] ParticleSystem doubleJump;
    
        [SerializeField] AudioSource heavyWhoosh;
        [SerializeField] AudioSource heavyWhoosh2;
        [SerializeField] AudioSource lightWhoosh;
        [SerializeField] AudioSource lightWhoosh2;
        [SerializeField] AudioSource jumpSound;
        [SerializeField] AudioSource land;
        [SerializeField] AudioSource dash;
        [SerializeField] AudioSource[] footstep;
        [SerializeField] AudioSource[] shot;


        void Update() {
            HandleDashFX();
        }

        #region DashFX
        
        void HandleDashFX() {
            HandleRun(mechAnimator.GetBool(MechAnimations.IsDashing));

            var isDashing = mechAnimator.GetBool(MechAnimations.IsDashing);
            
            HandleLeftBooster(isDashing);
            HandleRightBooster(isDashing);
            
        }
        void HandleRightBooster(bool isDashing) {
            if (isDashing) {
                //rightDashTrail.gameObject.SetActive(true);
                //rightDashTrail.time = 0.4f;
                if (rightDash.isEmitting)
                    return;
                rightDash.Play();
                rightDashInitialize.Play();
                return;
            }
            if (rightDash.isEmitting)
                rightDash.Stop();
            rightDashInitialize.Stop();
            //rightDashTrail.time = 0f;
            //rightDashTrail.gameObject.SetActive(false);
        }

        void HandleLeftBooster(bool isDashing) {
            if (isDashing) {
                //leftDashTrail.gameObject.SetActive(true);
                //leftDashTrail.time = 0.4f;
                if (leftDash.isEmitting) 
                    return;
                dash.Play();
                leftDash.Play();
                leftDashInitialize.Play();
                return;
            }
            if (leftDash.isEmitting)
                leftDash.Stop();
            leftDashInitialize.Stop();
            //leftDashTrail.time = 0f;
            //leftDashTrail.gameObject.SetActive(false);
        }

        void HandleRun(bool isRunning) {
            sprint.gameObject.SetActive(isRunning);
            if (isRunning)
                sprint.Play();
            else
                sprint.Stop();
        }

        #endregion

        #region PrimaryAttacks

        [UsedImplicitly]
        public void AerialPrimaryN() {
            var randomShot = UnityEngine.Random.Range(0, shot.Length);
            shot[randomShot].Play();
            AerialPrimaryN_Particle.Play();
        }

        [UsedImplicitly]
        public void CrouchingPrimary1() {
            shot[1].PlayWithRandomPitch();
            CrouchingPrimary1_Particle.Play();
        }
    
        [UsedImplicitly]
        public void CrouchingPrimary2() {
            CrouchingPrimary2_Particle.Play();
        }
    
        [UsedImplicitly]
        public void CrouchingPrimary3() {
            shot[3].PlayWithRandomPitch();
            CrouchingPrimary3_Particle.Play();
        }

        [UsedImplicitly]
        public void CrouchingPrimaryN() {
            var randomShot = Random.Range(1, shot.Length);
            shot[randomShot].Play();
            CrouchingPrimaryN_Particle.Play();
        }

        [UsedImplicitly]
        public void StandingPrimaryB1() {
            StandingPrimaryB1_Particle.Play();
        }
    
        [UsedImplicitly]
        public void StandingPrimaryB2() {
            var randomShot = Random.Range(0, shot.Length);
            shot[2].PlayWithRandomPitch(); //why not random?
            StandingPrimaryB2_Particle.Play();
        }
    
        [UsedImplicitly]
        public void StandingPrimaryB3() {
            StandingPrimaryB3_Particle.Play();
        }
    
        [UsedImplicitly]
        public void StandingPrimaryB4() {
            StandingPrimaryB4_Particle.Play();
        }
    
        [UsedImplicitly]
        public void StandingPrimaryB5() {
            var randomShot = Random.Range(0, shot.Length);
            shot[5].Play(); //not random?
            StandingPrimaryB5_Particle.Play();
        }
    
        [UsedImplicitly]
        public void StandingPrimaryB6() {
            StandingPrimaryB6_Particle.Play();
        }

        [UsedImplicitly]
        public void StandingForwardPrimary() {
            var randomShot = Random.Range(0, shot.Length);
            shot[randomShot].Play();
            StandingForwardP_Particle.Play();
        }

        [UsedImplicitly]
        public void StandingNeutralPrimary() {
            var randomShot = Random.Range(0, shot.Length);
            shot[randomShot].Play();
            StandingNeutralPrimary_Particle.Play();
        }
        
        #endregion
        
        #region Melee

        [UsedImplicitly]
        public void AerialMeleeB() {
            heavyWhoosh.PlayWithRandomPitch();
            AerialMeleeB_Particle.Play();
        }

        [UsedImplicitly]
        public void AerialMeleeN() {
            lightWhoosh.PlayWithRandomPitch();
            AerialMeleeN_Particle.Play();
        }

        [UsedImplicitly]
        public void CrouchingMeleeN() {
            lightWhoosh.PlayWithRandomPitch();
            CrouchingMeleeN_Particle.Play();
        }

        [UsedImplicitly]
        public void MeleeCrouchingL() {
            heavyWhoosh.PlayWithRandomPitch();
            MeleeCrouchingL_Particle.Play();
        }

        [UsedImplicitly]
        public void MeleeF() {
            heavyWhoosh.PlayWithRandomPitch();
            MeleeF_Particle.Play();
        }

        [UsedImplicitly]
        public void MeleeN() {
            lightWhoosh.PlayWithRandomPitch();
            MeleeN_Particle.Play();
        }

        [UsedImplicitly]
        public void MeleeR() {
            heavyWhoosh.PlayWithRandomPitch();
            MeleeR_Particle.Play();
        }
    
        [UsedImplicitly]
        public void MeleeL() {
            heavyWhoosh.PlayWithRandomPitch();
            MeleeL_Particle.Play();
        }

        [UsedImplicitly]
        public void CrouchingBMelee() {
            heavyWhoosh.PlayWithRandomPitch();
            CrouchingBMelee_Particle.Play();
        }

        #endregion
        
        [UsedImplicitly]
        public void Shoot() {
            projectile.transform.eulerAngles = new Vector3(MechUtils.PitchToFloat(mechView.CurrentState.aimData.pitch),
                MechUtils.HeadingToFloat(mechView.CurrentState.aimData.Heading), 0) + shootOffset;
        
            var randomShot = UnityEngine.Random.Range(1, 6);
            shot[randomShot].Play();

            projectile.Play();
        }

        #region Movement

        [UsedImplicitly]
        public void Jump_Land() {
            jumpLand.Play();
            land.PlayWithRandomPitch();
        }

        [UsedImplicitly]
        public void Jump() {
            jumpSound.Play();
            if (mech.position.y < 0.4f)
                jump.Play();
        
            else {
                doubleJump.Play();
            }
        }
    
        [UsedImplicitly]
        public void Left() {
            footstep[MechEventHandler.GetFreeAudioSourceIndex(footstep)].PlayWithRandomPitch();
            /*
        if (mech.position.y < 0.2f)
            leftStep.Play();
        */
            //too glitchy and I have no time to remove events.
        }

        [UsedImplicitly]
        public void Right() {
            footstep[MechEventHandler.GetFreeAudioSourceIndex(footstep)].PlayWithRandomPitch();
            /*
        if (mech.position.y < 0.2f)
            rightStep.Play();
        */
            //too glitchy and I have no time to remove events.
        }

        #endregion
    }
}
