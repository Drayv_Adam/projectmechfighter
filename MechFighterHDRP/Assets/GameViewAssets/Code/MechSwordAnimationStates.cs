using System.Collections.Generic;

namespace GameViewAssets.Code{
    public class MechSwordAnimationStates : MechAnimations {
        
        public override Dictionary<int, AnimationStateInfo> MechAnimationDictionary => SwordAnimationDictionary;
        public enum AnimationState {
            //Layer: Idle and Walk 
            //Animations start with 10000
            Idle = 10000,
            Idle_Crouch = 10001,
            Walking = 10002,

            //Layer: Jumps
            //Animations start with 20000
            Falling = 20000,
            Jump_Land = 20001,
            Neutral_Jump = 20002,

            //Layer: Dashes
            //Animations start with 30000
            Dashing = 30000,

            //Layer: Blocking
            //Animations start with 40000
            Blocking = 40000,
            Blocking_Crouch = 40001,
            Block_Crouching_Struck = 40002,
            Block_Struck = 40003,

            //Layer: Attacks
            //Animations start with 50000

            //Layer: Attacks, Sub_State:Secondary_Crouching
            //Animations start with 51000

            //Layer: Attacks, Sub_State:Secondary_Standing
            //Animations start with 52000

            //Layer: Attacks, Sub_State:Secondary_Air
            //Animations start with 53000

            //Layer: Attacks, Sub_State:Melee_Air
            //Animations start with 54000
            Melee_Air_Jab_Whole = 54000,
            Melee_Air_Jab_Charge = 54001,
            Melee_Air_Jab_Strike = 54002,
            Melee_Air_Horizontal = 54003,
            Air_Neutral_Melee = 54004,
            
            //Layer: Attacks, Sub_State:Melee_Standing
            //Animations start with 55000
            Melee_OverheadPunch_Charge = 55000,
            Melee_OverheadPunch_Strike = 55001,
            Melee_OverheadPunch_Whole = 55002,
            Melee_SideKickLeft = 55003,
            Melee_SideKickRight = 55004,
            Melee_SideKickLeft_Sworded = 55005,
            Melee_SideKickRight_Sworded = 55006,
            Melee_StraightPunch = 55007,
            Melee_Jab = 55008,
            Melee_Combo_P1 = 55009,
            Melee_Combo_P2 = 55010,
            Melee_DashingSlash = 55011,
            Melee_Running_Thrust = 55012,
            Melee_PummelAttack = 55013,
            Running_Forward_Melee = 55014,
            Running_Forward_MeleeALT = 55015,
            Standing_Forward_Melee = 55016,
            
            //Layer: Attacks, Sub_State:Melee_Crouching
            //Animations start with 56000
            Melee_Crouching_LegPoke = 56000,
            Melee_Crouching_Jab = 56001,
            Melee_Crouching_AntiAir = 56002,
            Melee_Crouch_Slash_P1 = 56003,
            Melee_Crouch_Slash_P2 = 56004,
            Crouching_Neutral_Melee = 56005,
            
            //Layer: Attacks, Sub_State:Primary_Crouching
            //Animations start with 57000
            Crouch_Backward_Melee = 57000,
            
            //Layer: Attacks, Sub_State:Primary_Standing
            //Animations start with 58000
            Standing_Forward_Primary_Followup = 58000,
            Standing_Forward_Primary = 58001,
            
            //Layer: Attacks, Sub_State:Primary_Air
            //Animations start with 59000
            Air_Backward_Primary = 59000,
            Air_Neutral_Primary = 59001,
            Air_Neutral_PrimaryAlt = 59002,
            Air_Backward_PrimaryV2 = 59003,
            Air_Neutral_PrimaryV2 = 59004,
            
            //Layer: Received Hits
            //Animations start with 60000
            GetUp_Spring = 60000,
            Hit_Crouching_Overhead = 60001,
            Hit_Mid = 60002,
            Hit_From_Left_1 = 60003,
            Hit_From_Left_2 = 60004,
            Hit_From_Right = 60005,
            Down_FaceUp = 60006,
            Landing_On_Back = 60007,
            Hit_Midair = 60008,
            Knockdown = 60009,
            Dead = 60010,
            Death_On_Knees = 60011,
        };

                
        static readonly  Dictionary<int, AnimationStateInfo> SwordAnimationDictionary =
            new Dictionary<int, AnimationStateInfo>() {
                //Layer: Idle and Walk 
                {(int) AnimationState.Idle, new AnimationStateInfo(200, AnimatorLayers.IdleAndWalk, "Idle")},
                {(int) AnimationState.Idle_Crouch, new AnimationStateInfo(100, AnimatorLayers.IdleAndWalk, "Idle_Crouch")},
                {(int) AnimationState.Walking, new AnimationStateInfo(48, AnimatorLayers.IdleAndWalk, "Walking")},

                //Layer: Jumps
                {(int) AnimationState.Neutral_Jump, new AnimationStateInfo(60, AnimatorLayers.Jumps, "Neutral_Jump")},
                {(int) AnimationState.Jump_Land, new AnimationStateInfo(68, AnimatorLayers.Jumps, "Jump_Land")},
                {(int) AnimationState.Falling, new AnimationStateInfo(100, AnimatorLayers.Jumps, "Falling")},
                
                //Layer: Dashes
                {(int) AnimationState.Dashing, new AnimationStateInfo(60, AnimatorLayers.Dashes, "Dashing")},
                
                //Layer: Blocking
                {(int) AnimationState.Blocking, new AnimationStateInfo(100, AnimatorLayers.Blocking, "Blocking")},
                {(int) AnimationState.Blocking_Crouch, new AnimationStateInfo(120, AnimatorLayers.Blocking, "Blocking_Crouch")},
                {(int) AnimationState.Block_Crouching_Struck, new AnimationStateInfo(20, AnimatorLayers.Blocking, "Blocking_Crouching_Struck")},
                {(int) AnimationState.Block_Struck, new AnimationStateInfo(100, AnimatorLayers.Blocking, "Block_Struck")},

                //Layer: Attacks

                //Layer: Attacks, Sub_State:Secondary_Crouching

                //Layer: Attacks, Sub_State:Secondary_Standing

                //Layer: Attacks, Sub_State:Secondary_Jumping

                //Layer: Attacks, Sub_State:Melee_Air
                {(int) AnimationState.Melee_Air_Jab_Whole, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_Air_Jab_Whole")},
                {(int) AnimationState.Melee_Air_Jab_Charge, new AnimationStateInfo(6, AnimatorLayers.Attacks, "Melee_Air_Jab_Charge")},
                {(int) AnimationState.Melee_Air_Jab_Strike, new AnimationStateInfo(42, AnimatorLayers.Attacks, "Melee_Air_Jab_Strike")},
                {(int) AnimationState.Melee_Air_Horizontal, new AnimationStateInfo(25, AnimatorLayers.Attacks, "Melee_Air_Horizontal")},
                {(int) AnimationState.Air_Neutral_Melee, new AnimationStateInfo(16, AnimatorLayers.Attacks, "Air_Neutral_Melee")},

                //Layer: Attacks, Sub_State:Melee_Standing
                {(int) AnimationState.Melee_OverheadPunch_Charge, new AnimationStateInfo(14, AnimatorLayers.Attacks, "Melee_OverheadPunch_Charge")},
                {(int) AnimationState.Melee_OverheadPunch_Strike, new AnimationStateInfo(55, AnimatorLayers.Attacks, "Melee_OverheadPunch_Strike")},
                {(int) AnimationState.Melee_OverheadPunch_Whole, new AnimationStateInfo(55, AnimatorLayers.Attacks, "Melee_OverheadPunch_Whole")},
                {(int) AnimationState.Melee_SideKickLeft, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_SideKickLeft")},
                {(int) AnimationState.Melee_SideKickRight, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_SideKickRight")},
                {(int) AnimationState.Melee_SideKickLeft_Sworded, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_SideKickLeft_Sworded")},
                {(int) AnimationState.Melee_SideKickRight_Sworded, new AnimationStateInfo(49, AnimatorLayers.Attacks, "Melee_SideKickRight_Sworded")},
                {(int) AnimationState.Melee_StraightPunch, new AnimationStateInfo(46, AnimatorLayers.Attacks, "Melee_StraightPunch")},
                {(int) AnimationState.Melee_Jab, new AnimationStateInfo(36, AnimatorLayers.Attacks, "Melee_Jab")},
                {(int) AnimationState.Melee_Combo_P1, new AnimationStateInfo(25, AnimatorLayers.Attacks, "Melee_Combo_P1")},
                {(int) AnimationState.Melee_Combo_P2, new AnimationStateInfo(25, AnimatorLayers.Attacks, "Melee_Combo_P2")},
                {(int) AnimationState.Melee_DashingSlash, new AnimationStateInfo(50, AnimatorLayers.Attacks, "Melee_DashingSlash")},
                {(int) AnimationState.Melee_Running_Thrust, new AnimationStateInfo(30, AnimatorLayers.Attacks, "Melee_Running_Thrust")},
                {(int) AnimationState.Melee_PummelAttack, new AnimationStateInfo(13, AnimatorLayers.Attacks, "Melee_PummelAttack")},
                {(int) AnimationState.Running_Forward_Melee, new AnimationStateInfo(80, AnimatorLayers.Attacks, "Running_Forward_Melee")},
                {(int) AnimationState.Running_Forward_MeleeALT, new AnimationStateInfo(100, AnimatorLayers.Attacks, "Running_Forward_MeleeALT")},
                {(int) AnimationState.Standing_Forward_Melee, new AnimationStateInfo(30, AnimatorLayers.Attacks, "Standing_Forward_Melee")},
                
                //Layer: Attacks, Sub_State:Melee_Crouching
                {(int) AnimationState.Melee_Crouching_LegPoke, new AnimationStateInfo(41, AnimatorLayers.Attacks, "Melee_Crouching_LegPoke")},
                {(int) AnimationState.Melee_Crouching_Jab, new AnimationStateInfo(27, AnimatorLayers.Attacks, "Melee_Crouching_Jab")},
                {(int) AnimationState.Melee_Crouching_AntiAir, new AnimationStateInfo(48, AnimatorLayers.Attacks, "Melee_Crouching_AntiAir")},
                {(int) AnimationState.Melee_Crouch_Slash_P1, new AnimationStateInfo(30, AnimatorLayers.Attacks, "Melee_Crouch_Slash_P1")},
                {(int) AnimationState.Melee_Crouch_Slash_P2, new AnimationStateInfo(30, AnimatorLayers.Attacks, "Melee_Crouch_Slash_P2")},
                {(int) AnimationState.Crouching_Neutral_Melee, new AnimationStateInfo(16, AnimatorLayers.Attacks, "Crouching_Neutral_Melee")},

                //Layer: Attacks, Sub_State:Primary_Crouching
                {(int) AnimationState.Crouch_Backward_Melee, new AnimationStateInfo(50, AnimatorLayers.Attacks, "Crouch_Backward_Primary")},

                //Layer: Attacks, Sub_State:Primary_Standing
                //Animations start with 58000
                {(int) AnimationState.Standing_Forward_Primary_Followup, new AnimationStateInfo(35, AnimatorLayers.Attacks, "Standing_Forward_Primary_Followup")},
                {(int) AnimationState.Standing_Forward_Primary, new AnimationStateInfo(35, AnimatorLayers.Attacks, "Standing_Forward_Primary")},
                
                //Layer: Attacks, Sub_State:Primary_Air
                //Animations start with 59000
                {(int) AnimationState.Air_Backward_Primary, new AnimationStateInfo(60, AnimatorLayers.Attacks, "Air_Backward_Primary")},
                {(int) AnimationState.Air_Neutral_Primary, new AnimationStateInfo(60, AnimatorLayers.Attacks, "Air_Neutral_Primary")},
                {(int) AnimationState.Air_Neutral_PrimaryAlt, new AnimationStateInfo(50, AnimatorLayers.Attacks, "Air_Neutral_PrimaryAlt")},
                {(int) AnimationState.Air_Backward_PrimaryV2, new AnimationStateInfo(60, AnimatorLayers.Attacks, "Air_Backward_PrimaryV2")},
                {(int) AnimationState.Air_Neutral_PrimaryV2, new AnimationStateInfo(50, AnimatorLayers.Attacks, "Air_Neutral_PrimaryV2")},

                //Layer: Received Hits
                {(int) AnimationState.GetUp_Spring, new AnimationStateInfo(58, AnimatorLayers.ReceivedHits, "GetUp_Spring")},
                {(int) AnimationState.Hit_Crouching_Overhead, new AnimationStateInfo(76, AnimatorLayers.ReceivedHits, "Hit_Crouching_Overhead")},
                {(int) AnimationState.Hit_Mid, new AnimationStateInfo(64, AnimatorLayers.ReceivedHits, "Hit_Mid")},
                {(int) AnimationState.Hit_From_Left_1, new AnimationStateInfo(25, AnimatorLayers.ReceivedHits, "Hit_From_Left_1")},
                {(int) AnimationState.Hit_From_Left_2, new AnimationStateInfo(32, AnimatorLayers.ReceivedHits, "Hit_From_Left_2")},
                {(int) AnimationState.Hit_From_Right, new AnimationStateInfo(25, AnimatorLayers.ReceivedHits, "Hit_From_Right")},
                {(int) AnimationState.Down_FaceUp, new AnimationStateInfo(60, AnimatorLayers.ReceivedHits, "Down_FaceUp")},
                {(int) AnimationState.Landing_On_Back, new AnimationStateInfo(100, AnimatorLayers.ReceivedHits, "Landing_On_Back")},
                {(int) AnimationState.Hit_Midair, new AnimationStateInfo(55, AnimatorLayers.ReceivedHits, "Hit_Midair")},
                {(int) AnimationState.Knockdown, new AnimationStateInfo(49, AnimatorLayers.ReceivedHits, "Knockdown")},
                {(int) AnimationState.Dead, new AnimationStateInfo(60, AnimatorLayers.ReceivedHits, "Dead")},
                {(int) AnimationState.Death_On_Knees, new AnimationStateInfo(109, AnimatorLayers.ReceivedHits, "Death_On_Knees")},
            };

    }
}
